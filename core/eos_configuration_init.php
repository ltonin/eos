<?php

define("EOS_CONFIG_BASEPATH", 		"/var/www/html/eOS/core/config/eos_config.ini");

$eos_cfg_general = parse_ini_file(EOS_CONFIG_BASEPATH, true);
$eos_cfg_session = $eos_cfg_general['session'];
$eos_cfg_system  = $eos_cfg_general['system'];

// General
define("EOS_VERSION",    		$eos_cfg_system['version']);
define("EOS_BASEPATH",   		$eos_cfg_system['siteroot'].$eos_cfg_system['version']."/");
//define("EOS_BASEURL", 			$eos_cfg_system['siteurl']);
define("EOS_BASEURL", 			"/eOS/");
define("EOS_SESSION_TIMEOUT", 		$eos_cfg_session['timeout']);
define("EOS_SYSTEM_IMAGES", 		EOS_BASEURL."core/img/");

// Resources
define("EOS_EXTRA_BASEURL", 		EOS_BASEURL."extra/");
define("EOS_FONTS_BASEURL", 		EOS_BASEURL."core/fonts/"); // TO BE CHANGED TO EXTRAURL."fonts/"
define("EOS_CSS_BASEURL",			EOS_BASEURL."core/styles/");
define("EOS_FILES_BASEURL",  		EOS_BASEURL."resources/");
define("EOS_FILES_BASEPATH", 		EOS_BASEPATH."resources/");
define("EOS_IMAGES_BASEURL", 		EOS_FILES_BASEURL."images/");
define("EOS_IMAGES_BASEPATH", 		EOS_FILES_BASEPATH."images/");
define("EOS_BOOKSTORE_BASEPATH",	EOS_FILES_BASEPATH."bookstore/");
define("EOS_BOOKSTORE_BASEURL",   	EOS_FILES_BASEURL."bookstore/");
define("EOS_VIDEO_BASEPATH",  		EOS_FILES_BASEPATH."video/");

define("EOS_DOI_BASEPATH",			EOS_BASEPATH."core/doi/");
define("EOS_PRINCE_BASEPATH",		EOS_BASEPATH."extra/prince/");

// Plugins
define("EOS_PLUGINS_BASEURL", 		EOS_BASEURL."plugins/");
define("EOS_ANALYTICS_BASEURL", 	EOS_PLUGINS_BASEURL."analytics/");
define("EOS_CKEDITOR_BASEURL", 		EOS_PLUGINS_BASEURL."ckeditor/");
define("EOS_CKFINDER_BASEURL",		EOS_PLUGINS_BASEURL."ckfinder/");

// Unset all configuration variables
unset($eos_cfg_general);
unset($eos_cfg_session);
unset($eos_cfg_system);

?>
