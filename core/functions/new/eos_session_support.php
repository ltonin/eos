<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_database.php");

function eos_session_start($timeout = EOS_SESSION_TIMEOUT) {

	// Create or resume session	
	session_start();

	// Check for session expiration
	eos_session_expired($timeout);

	// If is not already set, initialize userid
	if(isset($_SESSION['userid']) == false) 
		$_SESSION['userid'] = -1;

	// If is not already set, initialize usertype
	if(isset($_SESSION['usertype']) == false) 
		$_SESSION['usertype'] = -1;

	// If is not already set, initialize current issue
	if(isset($_SESSION['issuecurrent']) == false) 
		$_SESSION['issuecurrent'] = eos_session_get_issue();
	
	// If is not already set, initialize shown issue with the current
	if(isset($_SESSION['issueshown']) == false) 
		$_SESSION['issueshown']=$_SESSION['issuecurrent'];
		
}

function eos_session_expired($timeout) {


	// If in session is stored last_activity (resume)
	if(isset($_SESSION['lastactivity'])) {

		// Request current time
		$time = $_SERVER['REQUEST_TIME'];

		// Get time of last activity and save current time
		$last = $_SESSION['lastactivity'];

		// Compute elapsed time
		$elapsed = $time - $last;
		
		// If the elapsed time is less than timeout, 
		// set expired to false
		if ($elapsed > $timeout) {
			session_unset();
			session_destroy();
			session_start();
		}
	}
	$_SESSION['lastactivity'] = $_SERVER['REQUEST_TIME'];
}

function eos_session_get_issue() {

	$db = DataBase::Connect();
	$query="SELECT tb_numero.id, tb_numero.numero, tb_numero.special,
	   		tb_numero.img_url, tb_numero.col, tb_numero.altcol, tb_numero.shade
			FROM tb_numero INNER JOIN issue_status ON 
			tb_numero.stato=issue_status.id WHERE issue_status.label='Pubblicato'";
	$stmt = $db->prepare($query);

	if($stmt->execute() == false) {
		trigger_error('[mysql::eos_session_start()] '.$merror);
	}
	$result = $stmt->get_result();
	$stmt->close();
	$db->close();

	$issue = array();
	while($row = $result->fetch_array()) {
		$issue['id']       = $row['id'];
		$issue['number']   = $row['numero'];
		$issue['special']  = $row['special'];
		$issue['image']    = $row['img_url'];
		$issue['color']    = $row['col'];
		$issue['altcolor'] = $row['altcol'];
		$issue['shade']    = $row['shade'];
	}
	
	return $issue;
}



?>
