<?php
require_once(__DIR__ . "/../../eos_configuration_init.php");

error_reporting(E_ALL);
ini_set("display_errors", 1);

function eos_frontend_current_title_image() {
	$issue = $_SESSION['issueshown'];

	switch($issue['shade']) {
		case 4:
			$filename = "engrammaMarchio-w.png";
			break;
		default:
			$filename = "engrammaMarchio-b.png";
	}
	
	return EOS_BASEURL .'core/img/' . $filename;
}


?>
