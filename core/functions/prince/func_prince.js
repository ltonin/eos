//// simpletoc

function simpleToc() {

    var selector;
    var tocselector = "#toc";

    switch (arguments.length) {
        case 1:
            selector = arguments[0];
            break;
        case 2:
            selector = arguments[0];
            tocselector = arguments[1];
            break;
        default:
            console.log("toc: wrong number of arguments")
    }

    //   console.log("SIMPLETOC These elements will appear in toc:",selector);
    //   console.log("SIMPLETOC ToC will appear inside this element:",tocselector);

    var elems = document.querySelectorAll(selector);
    var ToCelems = document.querySelectorAll(tocselector);

    if (ToCelems.length > 0) {
        var ToCelem = ToCelems[0];
    } else {
        console.log("No ToC element found");
        exit;
    }

    for (var i = 0; i < elems.length; i++) {

        if (elems[i].hasAttribute("sommario")) {
            var str = elems[i].getAttribute("sommario").trim();
        }
        else {
            var str = elems[i].textContent.trim();
        }
        var text = document.createTextNode(str);
        //      console.log("TEXT: ", str);

        //      var text = document.createTextNode(getText(elems[i]));
        //      var text = document.createTextNode(window.getComputedStyle(elems[i], ':before').content);

        elems[i].setAttribute("id", "ch" + i);
        // var link = document.createElement("a"); 
        // link.setAttribute("href", "#ch" + i);
        //       var linkText = document.createElement("pre");
        //       linkText.appendChild(text);
        //       link.appendChild(linkText);
        //      link.appendChild(bt);

        // link.appendChild(text);
        // table
        var table = document.createElement("table");
        // row
        var row_link = document.createElement("a");
        row_link.setAttribute("href", "#ch" + i);
        var row = document.createElement("tr");
        row_link.appendChild(row);
        // page
        var page = document.createElement("td");
        page.className = "toc_page";
        page.setAttribute("href", "#ch" + i);
        row.appendChild(page);
        // title
        var title = document.createElement("td");
        title.className = "toc_title";
        title.appendChild(text);
        row.appendChild(title);
        // Append row to the table
        table.appendChild(row_link);
        // List element
        var li = document.createElement("li");
        li.className += elems[i].localName;
        li.appendChild(table);
        // li.appendChild(link);

        ToCelem.appendChild(li);
    }
}




//// multipass.js

//Prince.trackBoxes = true;

/*
var funcsarr = [];
var argsarr = [];

function printObject(o) {
    var out = '';
    for (var p in o) {
        out += p + ': ' + o[p] + '\n';
    }
    console.log(out);
}

function postlayout() {
    //  console.log("postlayout starting",funcsarr.length);
    if (funcsarr.length > 0) {
        var func = funcsarr.shift();
        var arg = argsarr.shift();
        console.log("  postlayout:", func.name);
        func(arg);
        Prince.registerPostLayoutFunc(postlayout);
    }
}

function schedule(callback, args) {
    funcsarr.push(callback);
    argsarr.push(args);
    if (args) {
        console.log("  scheduling:", callback.name, args);
    } else {
        console.log("  scheduling:", callback.name);
    }
    Prince.registerPostLayoutFunc(postlayout);
}


//// baseline alignment

// snap.js provides simple baseline aligment in Prince by sizing elements 
// so that they are a multiple of the line-height. Also, the script can push elements downwards so that
// the distance to the top of the page box is a multiple of the line height.


// <script type="text/javascript" src="snap.js"></script>
// <body onload="snap(16, 'h2', 'margin', 'img', 'height', 'h1', 'padding')">

// the arguments to the snap function is a the snap size in points, and a list of selectors
// snap() should only be called once per document
// the snap size should be equal to the line-height of the body text 
// selected elements will be adjusted to that their size is divisible by the snap size
// selected elements will be increased in size, either through increasing 'padding', 'margin' or 'height'
// the selected elements should all have "break-inside: avoid" set


function snap(size) {
    var snapSize = 1;
    var selectors = [];
    var methods = [];

    //  console.log("snap",size);

    for (var i = 0; i < arguments.length; i++) {
        if (i == 0) {
            snapSize = parseInt(arguments[0]);
            //      console.log("Setting snap size to",size);
        } else {
            if (i % 2) {  // odd number, i.e. a selector
                selectors.push(arguments[i]);
                //	   console.log("Registering selector",arguments[i]);
            } else { // even number 
                if ((arguments[i] == "margin") || (arguments[i] == "margin-top") || (arguments[i] == "margin-bottom") || (arguments[i] == "padding") || (arguments[i] == "padding-top") || (arguments[i] == "padding-bottom") || (arguments[i] == "padding-even") || (arguments[i] == "height") || (arguments[i] == "push")) {
                    methods.push(arguments[i]);
                    //              console.log("   method:",arguments[i]);
                } else {
                    //              console.log("   method",arguments[i],"not recognized, defaulting to padding");
                    methods.push("padding");
                }
            }
        }
    }
    //  console.log("registering baselineAlign");
    schedule(snap_postlayout, [snapSize, selectors, methods]);
}

function snap_postlayout(snap_args) {

    snapSize = snap_args[0];
    selectors = snap_args[1];
    methods = snap_args[2];

    //   console.log("snap_postlayout",snapSize,selectors,methods);

    var dbox = document.body.getPrinceBoxes();
    var y1 = dbox[0].y;                           // using first page
    var y2 = dbox[0].y - dbox[0].h;

    for (var i = 0; i < selectors.length; i++) {
        adjustSize(selectors[i], methods[i], y1, y2);
    }
}

function adjustSize(selector, method, y1, y2) {
    var elements = document.querySelectorAll(selector);
    //   console.log("    adjustSize ",selector, method, y1, y2);

    for (var i = 0; i < elements.length; i++) {
        var fboxes = elements[i].getPrinceBoxes();
        console.log("    adjustSize -- selector: \"", selector, "\" -- snapSize:", snapSize, "-- method:", method, "-- y1:", y1, "-- y2:", y2);
        switch (fboxes.length) {
            case 0:
                break;
            case 1:
                var paddingTop = parseFloat(fboxes[0].style.paddingTop);
                var paddingBottom = parseFloat(fboxes[0].style.paddingBottom);
                var marginTop = parseFloat(fboxes[0].style.marginTop);
                var marginBottom = parseFloat(fboxes[0].style.marginBottom);
                var height = fboxes[0].h;    // height includes padding/border!
                var gap = snapSize - (height % snapSize);  // gap is size that must be added

                var ha = y1 - fboxes[0].y;                       // ha is height above element 
                var hb = (fboxes[0].y - fboxes[0].h) - y2;       // hb is height below element 

                console.log("      element", i + 1, "gap", gap, "height", height, "paddingBottom", paddingBottom, "paddingTop", paddingTop, "marginBottom", marginBottom, "marginTop", marginTop);

                var has = ha / (ha + hb);   // fraction above element
                var hbs = hb / (ha + hb);   // fraction below element

                if (method == "height") {
                    height = height + gap - paddingTop - paddingBottom;
                    elements[i].style.height = height + "pt";
                    console.log("      adjusting height -- gap", gap, "new height", height);
                } else if (method == "padding") {
                    paddingTop += has * gap;
                    paddingBottom += hbs * gap;
                    elements[i].style.paddingTop = paddingTop + "pt";
                    elements[i].style.paddingBottom = paddingBottom + "pt";
                } else if (method == "margin") {
                    marginTop += has * gap;
                    marginBottom += hbs * gap;
                    elements[i].style.marginTop = marginTop + "pt";
                    elements[i].style.marginBottom = marginBottom + "pt";
                } else if (method == "margin-top") {
                    marginTop = gap;
                    elements[i].style.marginTop = marginTop + "pt";
                } else if (method == "margin-bottom") {
                    marginBottom = gap;
                    elements[i].style.marginBottom = marginBottom + "pt";
                } else if (method == "padding-top") {
                    paddingTop += gap;
                    elements[i].style.paddingTop = paddingTop + "pt";
                } else if (method == "padding-bottom") {

                    if (paddingBottom > snapSize) {
                        paddingBottom += (gap - snapSize);
                    }
                    else {
                        paddingBottom += gap;
                    }
                    elements[i].style.paddingBottom = paddingBottom + "pt";
                } else if (method == "padding-even") {
                    paddingTop += gap / 2;
                    paddingBottom += gap / 2;
                    elements[i].style.paddingTop = paddingTop + "pt";
                    elements[i].style.paddingBottom = paddingBottom + "pt";

                } else if (method == "push") {
                    paddingTop += snapSize - (ha % snapSize);
                    elements[i].style.paddingTop = paddingTop + "pt";
                }
                break;
            default:
                console.log("FRAGMENTATION ALERT! Element spans several pages ");
        }
    }
}

//// textfit will change the font size of all elements on a line so that they fill the full width

// textfit takes between one a three arguments:
//   selector 
//   size: represents the percentage of the line width that should be filled, 100 being default
//   even: a booleal


function textfit() {
    //  console.log("textfit",selector);
    var selector;
    var size = 100;
    var even = false;

    switch (arguments.length) {
        case 1:
            selector = arguments[0];
            break;
        case 2:
            selector = arguments[0];
            size = parseFloat(arguments[1]);
            break;
        case 3:
            selector = arguments[0];
            size = parseFloat(arguments[1]);
            //        console.log("odd or even?");
            if (arguments[2]) {
                var even = true;
                console.log("even!!");
            }
            break;
        default:
            console.log("textfit: wrong number of arguments")
    }

    var elems = document.querySelectorAll(selector);
    for (var i = 0; i < elems.length; i++) {
        addSpan(elems[i]);                        // add span elements around all words in all elements
    }
    schedule(textfit_postlayout, [selector, size, even])
}


function textfit_postlayout(textfit_args) {
    var selector = textfit_args[0];
    var size = textfit_args[1];
    var even = textfit_args[2];

    //    console.log("Textfit_postlayout",selector, size, even);
    var elems = document.querySelectorAll(selector);
    //    console.log("number of elements",elems.length);
    for (var i = 0; i < elems.length; i++) {                           // go through all elems that match selector
        var elem = elems[i];
        var minfs = 0;

        spans = elem.querySelectorAll("span.textfit");
        //        console.log("number of span.textfit",spans.length);
        for (var j = 0; j < spans.length; j++) {                       // go through all elems that were created by script
            var boxes = spans[j].getPrinceBoxes();
            var box = boxes[0];
            var fs = parseFloat(box.style.fontSize); // current font size of span
            var line = findLine(box);
            fullw = line.parent.w * (size / 100);
            fs = fs / (line.w / fullw);
            if ((fs < minfs) || (minfs == 0)) {
                minfs = fs;
            }
            box.element.style.fontSize = fs + "pt";        // set on span element
            elem.style.fontSize = fs + "pt";               // also set on parent element, so that the em unit works
        }
        if (even) {
            for (var j = 0; j < spans.length; j++) {                       // go through all elems that were created by script
                var boxes = spans[j].getPrinceBoxes();
                var box = boxes[0];
                box.element.style.fontSize = minfs + "pt";        // set on span element
                elem.style.fontSize = minfs + "pt";               // also set on parent element, so that the em unit works
            }
        }
    }
}

function findLine(box) {
    if (box.type == "LINE") {
        return box;
    } else {
        return findLine(box.parent);
    }
}

function addSpan(elem) {
    var node = elem.firstChild;
    while (node) {
        if (node.nodeType === Node.ELEMENT_NODE) {
            addSpan(node);
            node = node.nextSibling;
        } else if (node.nodeType === Node.TEXT_NODE) {
            //            var words = node.data.split(/(\s+)/);
            var words = node.data.split(/( +)/);
            for (var i = 0; i < words.length; ++i) {
                var newNode;
                if (words[i].trim() === "") {
                    newNode = document.createTextNode(words[i]);
                } else {
                    newNode = document.createElement("span");
                    newNode.textContent = words[i] + " ";      // space must be inside span 
                    newNode.className = "textfit";
                    //                    console.log("adding span",
                }
                node.parentNode.insertBefore(newNode, node);
            }
            var nextNode = node.nextSibling;
            node.parentNode.removeChild(node);
            node = nextNode;
        } else {
            node = node.nextSibling;
        }
    }
}

//// textfitpage? 

function textfitpage(selector) {
    //  console.log("textfitpage",selector);
    schedule(textfitpage_postlayout, selector)
}


function textfitpage_postlayout(selector) {
    //  console.log("textfitpage_postlayout",selector);
    var elems = document.querySelectorAll(selector);
    for (var i = 0; i < elems.length; i++) {
        var boxes = elems[i].getPrinceBoxes();
        if (boxes.length > 1) {
            console.log("WARNING: multi-page side boxes");
            next;
        }

        var box = boxes[0];
        var bbox = findbody(box);

        console.log("box is width, height, width * height", box.w, box.h, box.w * box.h);
        console.log("bbox is width, height, width * height", bbox.w, bbox.h, bbox.w * bbox.h);
        console.log("this is fullpage, missing height is: ", bbox.h - box.h);

        var firstimg = elems[i].querySelector("img");
        var firstimgb = firstimg.getPrinceBoxes();
        var firstimgh = firstimgb[0].h;
        console.log("image has height: ", firstimgh);

        var newheight = firstimgh + (bbox.h - box.h) - 20; // 20 is arbitrary
        firstimg.style.height = newheight + "pt";
        firstimg.style.objectFit = "cover";
        console.log("setting new height: ", newheight);
        printObject(firstimg.style);

        //    var totalw = boxes[0].w;
        //    elems[i].style.fontSize = newfs + "pt";
    }
}

function findbody(box) {
    //  console.log(box.type);

    if (box.type == 'BODY') {
        return box;
    }
    return findbody(box.parent);
}



//// toc


function toc() {
    var toc_args = [];
    for (var i = 0; i < arguments.length; i++) {
        toc_args.push(arguments[i]);
        console.log(" registering argument", arguments[i]);
    }
    schedule(toc_postlayout, toc_args);
}

function toc_postlayout(toc_args) {
    var tocselector = "#toc";
    switch (toc_args.length) {
        case 1:
            selector = toc_args[0];
            console.log("toc_postlayout: ", selector);
            break;
        case 2:
            selector = toc_args[0];
            tocselector = toc_args[1];
            break;
        default:
            console.log("toc: wrong number of arguments")
    }

    //   console.log("POSTLAYOUT These elements will appear in toc:",selector);
    //   console.log("POSTLAYOUT ToC will appear inside this element:",tocselector);

    var elems = document.querySelectorAll(selector);
    var ToCelems = document.querySelectorAll(tocselector);

    if (ToCelems.length > 0) {
        var ToCelem = ToCelems[0];
    } else {
        console.log("No ToC element found");
        exit;
    }

    for (var i = 0; i < elems.length; i++) {
        //      var before = pseudotext(elems[i]);
        var str = addpseudotext(elems[i], " ", " ");
        var text = document.createTextNode(str);
        //      console.log("TEXT: ", str);
        //      var text = document.createTextNode(getText(elems[i]));
        //      var text = document.createTextNode(window.getComputedStyle(elems[i], ':before').content);
        elems[i].setAttribute("id", "ch" + i);
        var link = document.createElement("a");
        link.setAttribute("href", "#ch" + i);
        var linkText = document.createElement("pre");
        linkText.appendChild(text);
        link.appendChild(linkText);
        //      link.appendChild(bt);
        var li = document.createElement("li");
        li.className += elems[i].localName;
        li.appendChild(link);
        ToCelem.appendChild(li);
    }
}


function addpseudotext(el, bstr, astr) {
    var boxes = el.getPrinceBoxes();    // get the boxes associated with the paragraph, there can be more than one if the paragaph spans several pages
    var str = ""

    if (el.textContent) {
        str = el.textContent;
    }

    for (var j = 0; j < boxes.length; j++) {   // go through list of boxes
        box = boxes[j];
        switch (box.pseudo) {
            case "before":
                //                printObject(box.element);
                //	        console.log("  pseudotext returning: ",box.children[0].text);
                str = box.children[0].text + bstr + str;
                break;
            case "after":
                str = str + astr + box.children[0].text;
                break;
        }
    }
    //     console.log("  pseudotext returning: ",astr+el.textContent+bstr);
    return (str);
}

//// indexes

function index() {
    schedule(index_postlayout);
}

function index_postlayout() {
    var ida = new Array();
    var enta = new Array();
    var str = "";

    // find all elements that contain index entries, these are marked with "class=ix" or "class=ixb" (for bold)

    var ix = document.querySelectorAll('.ix, .ixb');  // get all elements that are tagged for inclusion in the index

    // go through these elements, give them an unique id attribut so they can be linke to

    for (var i = 0; i < ix.length; i++) {

        ix[i].setAttribute("id", "ix" + i);      // set id attribute on elemenst tagged for inclusiont

        // if title attribute is specified, use it in the index. Else use the textual content

        var ent = ix[i].getAttribute("title");
        if (!ent) {
            ent = ix[i].textContent;
        }

        // add index entry to an array for later sorting

        if ((enta.join("")).indexOf(ent) < 0) {
            enta.push(ent);
        }

        // create an array to hold references to this entry (e.g. "Bach")

        if (!ida[ent]) {
            ida[ent] = new Array();
        }

        // add reference to the array

        ida[ent].push(i);                     // what is added to the array is a reference so that we can find the element later
        //        console.log(" adding ref",ent,i);
    }

    // sort the index entries alphabetically, case-insensitively 

    enta.sort(
        function (a, b) {
            if (a.toLowerCase() < b.toLowerCase()) return -1;
            if (a.toLowerCase() > b.toLowerCase()) return 1;
            return 0;
        });


    var el;

    // loop through entries and weed out references so that there is only one reference per entry per page. bold entries win over normal ones

    var prevmain = undefined;
    var main = undefined;
    var sub = undefined;

    for (var i = 0; i < enta.length; i++) {

        var prevpage = undefined;
        var normal = undefined;
        var bold = undefined;

        ent = enta[i];
        var a = ida[ent];  // array which now contains a list of indexes that belong to an entry

        //       console.log("Index entry",ent,a.length);

        for (var j = a.length - 1; j >= 0; j--) {       // reverse order is important as we will cull the array along the way
            el = ix[a[j]];
            var page = getPage(el);
            el.page = page;                                    // record the page number of the element which caused the reference
            //          console.log("  ref",j,el.id,el.className);

            if (page == prevpage) {    // if we are still on the same page, something has to be deleted
                if (bold) {             // if bold ref has been set for this page already, we should remove current ref
                    a.splice(j, 1);      // remove ref
                    //                 console.log("  removing in bold ",j);
                    continue;
                }

                if (el.className == 'ixb') {   // is this a bold page reference?
                    if (normal) {              // if normal ref has been set for this page, remove it
                        a.splice(normal, 1)
                        //                     console.log("  removing in normal",j);
                    }
                    bold = j;                  // remember the bold reference
                    continue;
                }

                if (normal) {            // if this is a normal reference, remove the previous normal reference
                    a.splice(normal, 1)
                    //                  console.log("  REMOVING IN NORMAL",j);
                    normal = j;
                }
            } else {
                prevpage = page;
                if (el.className == 'ixb') {
                    bold = j;
                    //                console.log("  recording bold",j);
                } else {
                    normal = j;
                    //                console.log("  recording normal",j);
                }
            }
        }

        // we have now culled double entries, we now will go through the remaining entries and create the index. 
        // the hard part in this section is to combine page numbers e.g. "1, 2, 3, 5" should become "1-3, 5"

        //       re = /([^;]*);([^;]*)/;

        if (ent.search(/;/) > -1) {    // is there a semi-colon in entry?
            var found = ent.match(/(.*)\s*;\s*(.*)/)
            var main = found[1];
            var sub = found[2];

            if (main == prevmain) {
                str = str + "\n<li class=subentry>" + sub + "&nbsp;";
            } else {
                prevmain = main;
                str = str + "\n<li class=entry>" + main + "\n<li class=subentry>" + sub + "&nbsp;";
            }
        } else {
            str = str + "\n<li class=entry>" + ent + "&nbsp;";
            prevmain = ent;
        }

        var el = undefined;       // this reference
        var pel = undefined;      // the previous reference
        var series = undefined;   // are we in a series that should be combined?

        for (var j = 0; j < a.length; j++) {

            el = ix[a[j]];   // find element pointed to by ref

            if (el.className) {                            // make string from element's classname
                var classname = " class=" + el.className;
            } else {
                classname = "";
            }

            if (!pel) {  // pel is undefined, so this is the first entry. 
                str = str + "<a href=#ix" + a[j] + classname + ">" + el.page + "</a>";
                pel = el;
                continue;
            }

            if (pel.className) {                            // make string from previous element's classname
                var pclassname = " class=" + pel.className;
            } else {
                pclassname = "";
            }

            if (el.page == pel.page + 1) {                 // if we are close
                if (el.className == pel.className) {
                    series = "-";                            // we are in a series which should be combined
                    pel = el;
                    continue;
                } else {                                    // different classname, we must terminate previous element (pel)
                    if (series) {
                        str = str + "&ndash;<a href=#ix" + a[j - 1] + pclassname + ">" + pel.page + "</a>";
                        series = undefined;                  // series has been terminated
                    }
                    str = str + ", <a href=#ix" + a[j] + classname + ">" + el.page + "</a>";
                    pel = el;
                }
            } else {                                        // we are not close, so we must terminate previous element (pel)
                if (series) {
                    str = str + "&ndash;<a href=#ix" + a[j - 1] + pclassname + ">" + pel.page + "</a>";
                    series = undefined;                      // series has been terminated
                }
                str = str + ", <a href=#ix" + a[j] + classname + ">" + el.page + "</a>";
                pel = el;
            }
        }

        if (series) {
            str = str + "&ndash;<a href=#ix" + a[j - 1] + classname + ">" + el.page + "</a>";
        }
    }
    //    console.log(str);
    document.getElementById("index").innerHTML = str;
}


function getPage(el) {

    var elboxes = el.getPrinceBoxes();
    if (elboxes.length > 0) {
        return elboxes[0].pageNum;         // page number where ref appears
    } else {
        console.log("GetPage, can't find page");
        return udefined;
    }
}
*/

