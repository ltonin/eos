$(document).ready(function() {
    // Set default italian language
    $("html").attr("lang", "it")

    // Converti didascalie
    $("p.caption").each(function (e, el) {
        $(el).replaceWith($('<figcaption>' + this.innerHTML + '</figcaption>'));
    });
    // Converti immagini in div4
    $("div.grid_4").each(function (e, el) {
        $(this).replaceWith($("<figure class='fig-small'>" + this.innerHTML + "</figure>"));
    });
    $("figure.fig-small>p>img").each(function (index) {
        $(this).unwrap();
    });


    // Converti immagini in div10 (e tutte quelle che non sono in una div10)
    $("p").has("img").each(function (index) {
        $(this).next("figcaption").addBack().wrapAll("<figure class='fig-big'></figure>");
    });
    $("figure.fig-big>p>img").each(function (index) {
        $(this).unwrap();
    });

    // Rimuovi tutte div 6 e div 10
    $("div.grid_6").each(function (index) {
        $(this).replaceWith($(this.innerHTML));
    });
    $("div.grid_10").each(function (index) {
        $(this).replaceWith($(this.innerHTML));
    });

  });