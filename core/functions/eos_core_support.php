<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/*
 * General related functions
 */
function eos_redirect($url) {

	$string = '<script type="text/javascript">';
	$string .= 'window.location = "' . $url . '"';
   	$string .= '</script>';

	echo $string;
}


/*
 * Backend related functions
 */
function eos_article_setstatus($article, $status) {

	// First - Fix article position list (by excluding this one)
	//// Get all the articles with the same attribute of this one
	$table = new Table('tb_articolo');
	$table->SetCondition('tipo',      $article->Get('type::id'), ElementType::AsInteger);
	$table->SetCondition('stato',     $article->Get('status::id'), ElementType::AsInteger);
	$table->SetCondition('id_numero', $article->Get('issue::id'), ElementType::AsInteger);
	$table->SetCondition('categoria', $article->Get('section::id'), ElementType::AsInteger);
	$table->SetCondition('id',        $article->Get('id'), ElementType::AsInteger, QueryCondition::IsDifferent);
	$table->SetOrder('pos');
	$table->Select('id');
	$table->Get('id', $listarticles);

	//// Fix the position of the articles
	$correctpos = 1;
	foreach($listarticles as $carticleid) {
		$carticle = new Article();
		$carticle->Retrieve($carticleid);
		if($carticle->Get('position') != $correctpos) {
			$carticle->Set('position', $correctpos);
			if($carticle->Store() == false) {
				$message->AddMessage("Cannot fix position for article $carticleid", MessageType::AsDebug);
				$message->Show();
				return false;
			}
		}
		$correctpos++;
	}

	// Second - Set the new status (and position)
	//// Save the current status of the article
	$cstatus = $article->Get('status');

	//// Get the maximum position of the articles in the new status
	$table->SetTable('tb_articolo');
	$table->SetCondition('stato',     $status->Get('id'), ElementType::AsInteger);
	$table->SetCondition('tipo',      $article->Get('type::id'), ElementType::AsInteger);
	$table->SetCondition('id_numero', $article->Get('issue::id'), ElementType::AsInteger);
	$table->SetCondition('categoria', $article->Get('section::id'), ElementType::AsInteger);
	$table->Select('MAX(pos) AS maxpos');
	$table->Get('maxpos', $maxposition, 0);

	// Setting the new status for the article
	$article->Set('status', $status);

	// Setting the correct position for the article (at the end) or: 
	// - 0 if the new status is 'cestino' 
	// - 0 if the current status is 'cestino' and the type of the article is
	//   'Copertina' (Restore)
	if($status->Get('label') === 'cestino') {
		$article->Set('position', 0);
	} elseif($article->Get('type::label') === 'Copertina') {
		$article->Set('position', 0);
	} else {
		$article->Set('position', $maxposition+1);
	}

	// Store article in the database
	if($article->Store() == false) {
		$message->AddMessage("Cannot store position for this article $article->Get('id')", MessageType::AsDebug);
		$message->Show();
		return false;
	}

	return true;	
}

function eos_setposition_article($article, $step) {

	$typeid       = $article->Get('type::id');
	$sectionid    = $article->Get('section::id');
	$currpos      = $article->Get('position');

	// Get all article with the same type, status, issue and section
	$table = new Table('tb_articolo');
	$table->SetCondition('tipo',      $typeid, ElementType::AsInteger);
	$table->SetCondition('categoria', $sectionid, ElementType::AsInteger);
	$table->SetOrder('pos');

	$table->Select('id', 'pos');
	$result = $table->GetResult();
	while($row = $result->fetch_array() ) {
		$index[] 	 = $row['id'];
	}

	$newpos = $currpos + $step;

	if($newpos < 0 || $newpos >= count($index) )
		return true;

	//echo "Before swapping: <br>";
	//foreach($index as $key => $value)
	//	echo $key.": ".$value."<br>";
	
	// Swapping
	$oldvalue = $index[$newpos];
	$index[$newpos] = $index[$currpos];
	$index[$currpos] = $oldvalue;

	//echo "After swapping: <br>";
	//foreach($index as $key => $value)
	//	echo $key.": ".$value."<br>";


	$dberror = false;
	$db = Database::Connect();
	foreach($index as $key => $value) {
		$stmt = $db->prepare("UPDATE `tb_articolo` SET `pos`=? WHERE `id`=?");
		$stmt->bind_param('ii', $key, $value);
		if($stmt->execute() == false) {
			trigger_error('[mysql::'.$this->_name.'] '.$merror);
			$dberror = true;
		}
	}

	$stmt->close();
	$db->close();

	return !$dberror;
}

function eos_article_setposition($article, $step) {
	
	$message = new Message();

	$currpos = $article->Get('position');
	$newpos  = $currpos + $step;
	
	// Initialize conditions for table
	$table = new Table('tb_articolo');
	$table->SetCondition('tipo',      $article->Get('type::id'), ElementType::AsInteger);
	$table->SetCondition('stato',     $article->Get('status::id'), ElementType::AsInteger);
	$table->SetCondition('id_numero', $article->Get('issue::id'), ElementType::AsInteger);
	$table->SetCondition('categoria', $article->Get('section::id'), ElementType::AsInteger);

	// Select max value for the position
	$table->Select('MAX(pos) AS maxpos');
	$table->Get('maxpos', $maxpos, 0);
	
	if( ($newpos < 1) || ($newpos > $maxpos) ) {
		return false;
	}

	// Select other article with the new position and the same tipo and
	$table->SetCondition('pos', $newpos, ElementType::AsInteger);
	$table->Select('id');
	if($table->Get('id', $otherid, 0) > 0) {
		$other = new Article();
		$other->Retrieve($otherid);
		$other->Set('position', $currpos);
		if($other->Store() == false) {
			$message->AddMessage("Cannot store position for other article $otherid", MessageType::AsDebug);
			$message->Show();
			return false;
		}
	}

	// Update current article with the new position		
	$article->Set('position', $newpos);

	if($article->Store() == false) {
		$message->AddMessage("Cannot store position for this article $article->Get('id')", MessageType::AsDebug);
		$message->Show();
		return false;
	}

	return true;	
}

function eos_section_setposition($section, $step) {

	$currpos = $section->Get('position');
	$newpos  = $currpos + $step;

	// Initialize conditions for table
	$table = new Table('tb_categoria');
	
	// Select max value for the position
	$table->Select('MAX(pos) AS maxpos');
	$table->Get('maxpos', $maxpos, 0);
	
	if( ($newpos < 1) || ($newpos > $maxpos) ) {
		return false;
	}

	// Select other section with the new position
	$table->SetCondition('pos', $newpos, ElementType::AsInteger);
	$table->Select('id');
	
	if($table->Get('id', $otherid, 0) > 0) {
		$other = new ArticleSection();
		$other->Retrieve($otherid);
		$other->Set('position', $currpos);
		if($other->Store() == false) {
			$message->AddMessage("Cannot store position for other section: $otherid", MessageType::AsDebug);
			$message->Show();
			return false;
		}
	}
	
	// Update current section with the new position		
	$section->Set('position', $newpos);
	if($section->Store() == false) {
		$message->AddMessage("Cannot store position for this section $section->Get('label')", MessageType::AsDebug);
		$message->Show();
		return false;
	}

	return true;
}

function eos_series_setposition($series, $step) {

	$currpos = $series->Get('position');
	$newpos  = $currpos + $step;

	// Initialize conditions for table
	$table = new Table('issue_series');
	
	// Select max value for the position
	$table->Select('MAX(position) AS maxpos');
	$table->Get('maxpos', $maxpos);
	
	if( ($newpos < 1) || ($newpos > $maxpos) ) {
		return false;
	}

	// Select other section with the new position
	$table->SetCondition('position', $newpos, ElementType::AsInteger);
	$table->Select('id');
	
	if($table->Get('id', $otherid, 0) > 0) {
		$other = new IssueSeries();
		$other->Retrieve($otherid);
		$other->Set('position', $currpos);
		if($other->Store() == false) {
			$message->AddMessage("Cannot store position for other series: $otherid", MessageType::AsDebug);
			$message->Show();
			return false;
		}
	}
	
	// Update current section with the new position		
	$series->Set('position', $newpos);
	if($series->Store() == false) {
		$message->AddMessage("Cannot store position for this series $series->Get('label')", MessageType::AsDebug);
		$message->Show();
		return false;
	}

	return true;
}


/*
 * Session related functions
 */
function eos_session_start($timeout = EOS_SESSION_TIMEOUT) {

	// Create or resume session	
	session_start();

	// Check for session expiration
	eos_session_expired($timeout);

	// If is not already set, initialize userid
	if(isset($_SESSION['userid']) == false) 
		$_SESSION['userid']=0;

	// If is not already set, initialize usertype
	if(isset($_SESSION['usertype']) == false) 
		$_SESSION['usertype']=0;

	// Initialize current_issue
	$issuestatus = new IssueStatus();
	$issuestatus->RetrieveBy('label', ElementType::AsString, 'Pubblicato');
	$issue = new Issue();
	$issue->RetrieveBy('stato', ElementType::AsInteger, $issuestatus->Get('id'));
	$_SESSION['issuecurrent']= $issue->Get('number');

	//// If is not already set, initialize usertype
	if(isset($_SESSION['issueshown']) == false) 
		$_SESSION['issueshown']=$_SESSION['issuecurrent'];
}

function eos_session_expired($timeout) {


	// If in session is stored last_activity (resume)
	if(isset($_SESSION['lastactivity'])) {

		// Request current time
		$time = $_SERVER['REQUEST_TIME'];

		// Get time of last activity and save current time
		$last = $_SESSION['lastactivity'];

		// Compute elapsed time
		$elapsed = $time - $last;
		
		// If the elapsed time is less than timeout, 
		// set expired to false
		if ($elapsed > $timeout) {
			session_unset();
			session_destroy();
			session_start();
		}
	}
	$_SESSION['lastactivity'] = $_SERVER['REQUEST_TIME'];

}

function eos_session_delete() {
	session_unset();
	session_destroy();
}

function eos_session_set($usertype, $showissue, $userid) {
	
	$_SESSION['userid']	= $userid;
	$_SESSION['usertype'] 	= $usertype;
	$_SESSION['issueshown'] = $showissue;	
}

?>
