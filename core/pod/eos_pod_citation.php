<?php


class Citation  {
	function __construct() {
		$entry = NULL;
	}

	
	public function RetrieveByArticle($article_id) {

		if(empty($article_id))
			return false;

		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `article_doi` WHERE `article_id`=?");
		$stmt->bind_param('i', $article_id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::citation] '.$merror); 
			return false;
		}

		$result  = $stmt->get_result();
		$doi = $result->fetch_array();
		
		if ( empty($doi) == true ) {
			return false;
		}

		// Retrieve issueid
		$stmt=$db->prepare("SELECT tb_numero.numero, tb_numero.special FROM tb_numero INNER JOIN tb_articolo on tb_numero.id = tb_articolo.id_numero AND tb_articolo.id=?");
		$stmt->bind_param('i', $article_id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::citation] '.$merror); 
			return false;
		}
		
		$result  = $stmt->get_result();
		$issue = $result->fetch_array();

		$stmt->close();
		$db->close();


		$this->entry = $doi;
		$this->entry["issue"] = $issue["numero"];
		$this->entry["issue_special"] = $issue["special"];
		
		return true;

	}

	public function meta_doi() {
		return "<meta name=\"citation_doi\" content=\"" . $this->entry["doi"] . "\">";
	}

	public function meta_authors() {

		$content = "";
		$input   = htmlspecialchars($this->entry["creator"]);
		$authors = explode(";", $input);
		
		foreach($authors as $value) {
			$content = $content . "<meta name=\"citation_author\" content=\"" . $value . "\">";
		}
		return $content;
	}

	public function meta_title() {
		$title = htmlspecialchars($this->entry["title"]);
		return "<meta name=\"citation_title\" content=\"" . $title . "\">";
	}

	public function meta_journal_title() {
		$jtitle = htmlspecialchars($this->entry["publisher"]);
		return "<meta name=\"citation_journal_title\" content=\"" . $jtitle . "\">";

	}
	
	public function meta_volume() {
		$volume = htmlspecialchars($this->entry["year"]);
		return "<meta name=\"citation_volume\" content=\"" . $volume . "\">";
	}

	public function meta_issue() {
		$issue = htmlspecialchars($this->entry["issue"]);
		return "<meta name=\"citation_issue\" content=\"" . $issue . "\">";
	}
	
	public function meta_year() {
		$year = htmlspecialchars($this->entry["year"]);
		return "<meta name=\"citation_year\" content=\"" . $year . "\">";
	}
	
	public function meta_publication_date() {
		$special = htmlspecialchars($this->entry["issue_special"]);
		$day     = sprintf("%02d", 22);
		
		$month   = $this->guessing_publish_month($special);
		if($month == -1) {
			return "";
		}

		$month   = sprintf("%02d", $this->guessing_publish_month($special));
		$year    = htmlspecialchars($this->entry["year"]);
		$date    = $year . "/" . $month . "/" . $day;
		return "<meta name=\"citation_publication_date\" content=\"" . $date . "\">";
	}

	public function meta_abstract() {
		$abstract = htmlspecialchars($this->entry["abstract"]);
		return "<meta name=\"citation_abstract\" content=\"" . $abstract . "\">";
	}

	public function guessing_publish_month($value) {

		$value = strtolower($value);
		$months = array("gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre");

		$monthnum = -1;
		foreach($months as $k => $m) {
			if(str_contains($value, $m) == true) {
				$monthnum = $k + 1;
				break;
			}
		}
		return $monthnum;

	}
	
		
}

?>
