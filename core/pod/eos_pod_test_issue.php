<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
include_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
?>

<html>
<body>
<?php

$newlabel   = "Rimosso";
$newnumber  = 30101982;
$newspecial = "compleanno";

echo "Create new issuestatus with label '".$newlabel."'<br>";
$issuestatus = new IssueStatus();
$issuestatus->Set("label", $newlabel);
if($issuestatus->Store()) {
	$issuestatusid = $issuestatus->Get("id");
	echo "&emsp;&emsp;|- New issuestatus stored with id: ".$issuestatusid."<br>";
} else {
	$issuestatusid = '';
	echo "&emsp;&emsp;|- Cannot store issuestatus<br>";
}
unset($issuestatus);

echo "Retrieve issuestatus with id: ".$issuestatusid."<br>";
$issuestatus = new IssueStatus();
if($issuestatus->Retrieve($issuestatusid)) {
	echo "&emsp;&emsp;|- Done<br>";
} else {
	echo "&emsp;&emsp;|- Cannot store issuestatus<br>";
}


echo "Create new issue with numeber '".$newnumber."' and status '".$newlabel."'<br>";
$issue = new Issue();
$issue->Set("number", $newnumber);
$issue->Set("status", $issuestatus);
$issue->Set("special", $newspecial);

if($issue->Store()) {
	$issueid = $issue->Get("id");
	echo "&emsp;&emsp;|+ New issue stored with id: ".$issueid."<br>";
	echo "&emsp;&emsp; |- 'number' : ".$issue->Get("number")."<br>";
	echo "&emsp;&emsp; |- 'status' : ".$issue->Get("status::label")."|".$issue->Get("status::id")."<br>";
	echo "&emsp;&emsp; |- 'special': ".$issue->Get("special")."<br>";

} else {
	$issueid = '';
	echo "&emsp;&emsp;|- Cannot store issue<br>";
}

echo "Delete issue and issue<br>";
$issuestatus->Drop();
$issue->Drop();


$number = 137;
$tbname = "tb_numero";
$query = "SELECT * FROM ".$tbname." WHERE numero=?";

$db   = Database::Connect();
//$stmt=$db->prepare("SELECT * FROM `tb_numero` WHERE `numero`=?");
$stmt=$db->prepare($query);
$stmt->bind_param('i', $number);
$code = $stmt->execute();
$result = $stmt->get_result();
$issue = $result->fetch_array();
$stmt->close();
$db->close();

echo "Id for number ".$number.": ".$issue['id'];

?>

</body>
</html>
