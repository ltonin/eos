<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_element.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_database.php");

abstract class ElementDB extends Element {
	
	protected $_tbname = '';
	
	function __construct($elname, $tbname) {

		parent::__construct($elname);
		$this->_tbname = $tbname;
	}

	abstract public function Retrieve($id);
	abstract public function Store();

	public function RetrieveBy($fname, $ftype, $fvalue) {

		$db = Database::Connect();
		$query = "SELECT `id` FROM `".$this->_tbname."` WHERE `".$fname."`=?";
		$stmt=$db->prepare($query);
		$stmt->bind_param($ftype, $fvalue);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result  = $stmt->get_result();
		$article = $result->fetch_array();
		$stmt->close();
		$db->close();

		if(isset($article))
			return $this->Retrieve($article['id']);
		else
			return false;

	}
  
  public function IsEmpty($key = "id") {
    try {
      $value = $this->Get($key);

      if (is_null($value)) 
        return true;
      else
        return false;
    } catch (Exception $e) {
      return true;
    }
  }

	public function Drop() {
		$db = Database::Connect();
		$code = false;
		$id   = $this->Get("id");	
		if(is_null($id) == false) {
			$stmt = $db->prepare("DELETE FROM `".$this->_tbname."` WHERE `id`=?");
			$stmt->bind_param('i', $id);
			if($stmt->execute() == false) {
				$merror = $stmt->error;
				trigger_error('[mysql::'.$this->_name.'] '.$merror); 
				return false;
			}

			$stmt->close();
			$db->close();
		}
		return true;	
	}

	public function GetTableName() {
		return $this->_tbname;
	}

	public function SetTableName($tbname) {
		$this->_tbname = $tbname;
	}
}

?>
