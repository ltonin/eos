<?php
require_once(__DIR__."/eos_pod_element_database.php");

class IssueSeries extends ElementDB {

	function __construct($elname = "series", $tbname = "issue_series") {
		
		parent::__construct($elname, $tbname);
		
		$this->Set("id", 	NULL);
		$this->Set("label", 	NULL);
		$this->Set("position", 	NULL);
		$this->Set("special", 	NULL);
		$this->Set("imgurl", 	NULL);
	}

	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		$result = $stmt->get_result();
		$series = $result->fetch_array();
		$stmt->close();
		$db->close();

		if(isset($series) == false)
			return false;

		$this->Set('id', 	$series['id']);
		$this->Set('label', 	$series['label']);
		$this->Set('position', 	$series['position']);
		$this->Set('special', 	$series['special']);
		$this->Set('imgurl', 	$series['imgurl']);
		
		return true;
	}

	public function Store() {
		$db = Database::Connect();

		$id    		= $this->Get("id");	
		$label    	= $this->Get("label");	
		$position    	= $this->Get("position");	
		$special 	= $this->Get("special");	
		$imgurl 	= $this->Get("imgurl");	
		
		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`label`, `position`, `special`, `imgurl`) VALUES (?, ?, ?, ?)");
			$stmt->bind_param('siss', $label, $position, $special, $imgurl);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `label`= ?, `position`=?, `special`=?, `imgurl`=? WHERE `id`=?");
			$stmt->bind_param('sissi', $label, $position, $special, $imgurl, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		if(is_null($id))
			$this->Set('id', $stmt->insert_id);
		
		$stmt->close();
		$db->close();

		return true;
	}
}

class IssueStatus extends ElementDB {

	function __construct($elname = "status", $tbname = "issue_status") {
		
		parent::__construct($elname, $tbname);
		
		$this->Set("id", 	NULL);
		$this->Set("label", 	NULL);
	}

	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id`, `label` FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		$stmt->bind_result($id, $label);
		$stmt->fetch();
		$stmt->close();
		$db->close();
		
		$this->Set("id", 	$id);
		$this->Set("label", 	$label);
		
		return true;
	}

	public function Store() {
		$db = Database::Connect();
		$id    = $this->Get("id");	
		$label = $this->Get("label");	

		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`label`) VALUES (?)");
			$stmt->bind_param('s', $label);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `label`= ? WHERE `id`=?");
			$stmt->bind_param('si', $label, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		if(is_null($id))
			$this->Set('id', $stmt->insert_id);
		
		$stmt->close();
		$db->close();

		return true;
	}
}

class IssueIsbn extends ElementDB {

	function __construct($elname = "isbn_base", $tbname = "isbn_series") {
		
		parent::__construct($elname, $tbname);
		
		$this->Set("id",		NULL);
		$this->Set("base",		NULL);
		$this->Set("current", 	NULL);
	}

	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id`, `base`, `current` FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		$stmt->bind_result($id, $base, $current);
		$stmt->fetch();
		$stmt->close();
		$db->close();
		
		$this->Set("id", 	$id);
		$this->Set("base", 	$base);
		$this->Set("current", 	$current);
		
		return true;
	}

	public function Store() {
		$db = Database::Connect();
		$id      = $this->Get("id");	
		$base    = $this->Get("base");	
		$current = $this->Get("current");	

		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`base`, `current`) VALUES (?)");
			$stmt->bind_param('si', $base, $current);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `base`= ?, `current`= ? WHERE `id`=?");
			$stmt->bind_param('sii', $base, $current, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		if(is_null($id))
			$this->Set('id', $stmt->insert_id);
		
		$stmt->close();
		$db->close();

		return true;
	}
}


class Issue extends ElementDB {
	
	public function __construct($elname = "issue", $tbname = "tb_numero") {
		
		parent::__construct($elname, $tbname);
		
		$this->Set('id', 		NULL);
		$this->Set('number', 		NULL);
		$this->Set('status', 		new IssueStatus());
		$this->Set('series', 		new IssueSeries());
		$this->Set('created', 		NULL);
		$this->Set('special', 		NULL);
		$this->Set('datelabel', 	NULL);
		$this->Set('description', 	NULL);
		$this->Set('authors', 		NULL);
		$this->Set('extra', 		NULL);
		$this->Set('isbn',			NULL);
		$this->Set('isbn_base', 	new IssueIsbn());
		$this->Set('img',			NULL);
		$this->Set('color', 		NULL);
		$this->Set('altcolor', 		NULL);
		$this->Set('shade', 		NULL);
	}

	public function Retrieve($id) {
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result = $stmt->get_result();
		$issue = $result->fetch_array();
		$stmt->close();
		$db->close();

		if(isset($issue) == false)
			return false;

		$this->Set('id', 		$issue['id']);
		$this->Set('number', 		$issue['numero']);
		$this->Set('special', 		$issue['special']);
		$this->Set('datelabel', 	$issue['data_comp']);
		$this->Set('description', 	$issue['descrizione']);
		$this->Set('authors', 		$issue['autori']);
		$this->Set('extra', 		$issue['extra']);
		$this->Set('isbn',			$issue['isbn']);
		$this->Set('img', 			$issue['img_url']);
		$this->Set('color', 		$issue['col']);
		$this->Set('altcolor', 		$issue['altcol']);
		$this->Set('shade', 		$issue['shade']);
		$this->Set('created', 		$issue['data']);
		
		$this->Get('status')->Retrieve($issue['stato']);
		$this->Get('series')->Retrieve($issue['seriesid']);
		$this->Get('isbn_base')->Retrieve($issue['isbn_base']);

		return true;
	}

	public function Exist() {
		$number = $this->Get("number");
		$checknumber = new Issue();
		$checknumber->RetrieveBy('numero', ElementType::AsInteger, $number);	
		if(is_null($checknumber->Get("id"))) {
			return false;
		} 			
		return true;
	}


	public function Store() {

		$db = Database::Connect();
		$id       	= $this->Get('id');
		$number 	= $this->Get('number');
		$statusid  	= $this->Get('status::id');
		$seriesid  	= $this->Get('series::id');
		$special 	= $this->Get('special');
		$datelabel 	= $this->Get('datelabel');
		$description 	= $this->Get('description');
		$authors 	= $this->Get('authors');
		$extra 		= $this->Get('extra');
		$isbn 		= $this->Get('isbn');
		$isbnbaseid = $this->Get('isbn_base::id');
		$img 		= $this->Get('img');
		$color 		= $this->Get('color');
		$altcolor 	= $this->Get('altcolor');
		$shade 		= $this->Get('shade');

		if(is_null($id) == true) {
			$created  = date("Y-m-d H:i:s");
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`numero`, `stato`, `seriesid`, `special`, `data`, `data_comp`, `descrizione`, `autori`, `extra`, `isbn`, `isbn_base`, `img_url`, `col`, `altcol`, `shade`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('iiisssssssisssi', $number, $statusid, $seriesid, $special, $created, $datelabel, $description, $authors, $extra, $isbn, $isbnbaseid, $img, $color, $altcolor, $shade);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `numero`= ?, `stato`=?, `seriesid`=?, `special`=?, `data_comp`=?, `descrizione`=?, `autori`=?, `extra`=?, `isbn`=?, `isbn_base`=?, `img_url`=?, `col`=?, `altcol`=?, `shade`=? WHERE `id`=?");
			$stmt->bind_param('iiissssssisssii', $number, $statusid, $seriesid, $special, $datelabel, $description, $authors, $extra, $isbn, $isbnbaseid, $img, $color, $altcolor, $shade, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);

		$stmt->close();
		$db->close();

		return true;
	}

	public function GetIsbn() {

		$isbn_base = $this->Get('isbn_base::base');
		$isbn      = $this->Get('isbn');

		if(is_null($isbn_base) || $isbn_base === "")
			return "";

		if(is_null($isbn))
			return "";

		$isbn_s = $isbn_base . $isbn;

		$number = 3*( intval(substr($isbn_s, 1, 1))
					   +  intval(substr($isbn_s, 3, 1))
					   +  intval(substr($isbn_s, 5, 1))
					   +  intval(substr($isbn_s, 7, 1))
					   +  intval(substr($isbn_s, 9, 1))
					   +  intval(substr($isbn_s, 11, 1))) +
					  (  intval(substr($isbn_s, 0, 1))
					   + intval(substr($isbn_s, 2, 1))
					   + intval(substr($isbn_s, 4, 1))
					   + intval(substr($isbn_s, 6, 1))
					   + intval(substr($isbn_s, 8, 1))
					   + intval(substr($isbn_s, 10, 1)));

		$integrity = ceil($number/10)*10 - $number;

		return $isbn_s . strval($integrity);


	}
}

?>
