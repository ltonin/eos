<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once(__DIR__."/../eos_configuration_init.php");

include_once(__DIR__."/eos_pod_issue.php");
include_once(__DIR__."/eos_pod_user.php");
include_once(__DIR__."/eos_pod_article.php");
?>


<html>
<body>
<?php

$articleid = 2912;


echo "Retrieve article with id: ".$articleid."<br>";
$article = new Article();
if($article->Retrieve($articleid)) {
	echo "&emsp;&emsp;|- Done<br>";
} else {
	echo "&emsp;&emsp;|- Cannot retrieve article<br>";
	die();
}

echo "<br><br>";

echo "+ Article<br>";
echo "|- id:		".$article->Get("id")."<br>";
echo "|- issue:		".$article->Get("issue::number")."<br>";
echo "|- user:		".$article->Get("user::nickname")."<br>";
echo "|- type:		".$article->Get("articletype::label")."<br>";
echo "|- section:	".$article->Get("articlesection::label")."<br>";
echo "|- status:	".$article->Get("articlestatus::label")."<br>";
echo "|- title:		".$article->Get("title")."<br>";
echo "|- created:	".$article->Get("created")."<br>";
echo "|- modified:	".$article->Get("modified")."<br>";
echo "|- modifiedby:	".$article->Get("modifiedby")."<br>";
echo "|- position:	".$article->Get("position")."<br>";

echo "<br><br>";

$narticle = new Article();
$ntype = new ArticleType();
$ntype->Retrieve(3); // Copertina
$narticle->Set("issue", 		$article->Get("issue")); 
$narticle->Set("user", 			$article->Get("user")); 
$narticle->Set("articletype", 		$ntype); 
$narticle->Set("articlesection",	$article->Get("articlesection")); 
$narticle->Set("articlestatus", 	$article->Get("articlestatus")); 
$narticle->Set("title", 		"Titolo per nuovo articolo di prova");

if($narticle->Store() == true) {
	$narticleid = $narticle->Get('id');
	echo "&emsp;&emsp;|- New article stored with id: ".$narticleid."<br>";
} else {
	echo "&emsp;&emsp;|- Article not stored<br>";
	die();
}


?>
</body>
</html>
