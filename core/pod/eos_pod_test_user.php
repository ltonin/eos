<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once(__DIR__."/../eos_configuration_init.php");
include_once(__DIR__."/eos_pod_user.php");
?>

<html>
<body>
<?php

// Create an empty user type
$utype = new UserType();

// Retrieve user with id 1 (Magister Ludi)
try {
	$utype->Retrieve(1);
} catch (Exception $e) {
	echo $e->getMessage()."\n";
}

echo 'User type retrieved from db: id|'.$utype->Get('id').' label|'.$utype->Get('label')."<br>";

// Store new user type
echo 'Creating a new usertype:<br>';
$new_utype = new UserType();
$new_utype->Set('label', 'Visitatore');
if($new_utype->Store() == true) {
	$new_utypeid = $new_utype->Get('id');
	echo "&emsp;&emsp;|- New usertype stored with id: ".$new_utypeid."<br>";
} else {
	echo "&emsp;&emsp;|- Usertype not stored<br>";
}

$new_utype->Retrieve($new_utypeid);
echo 'New user type retrieved: id|'.$new_utype->Get('id').' label|'.$new_utype->Get('label')."<br>";

echo 'Update usertype:<br>';
$new_utype->Set('label', 'Hacker');
if($new_utype->Store() == true) {
	$new_utypeid = $new_utype->Get('id');
	echo "&emsp;&emsp;|- Usertype with id: ".$new_utypeid." updated<br>";
} else {
	echo "&emsp;&emsp;|- Usertype not updated<br>";
}

echo 'User type updated: id|'.$new_utype->Get('id').' label|'.$new_utype->Get('label')."<br>";
$hackertype = $new_utype;

if($new_utype->Drop() == true) {
	echo "&emsp;&emsp;|- Usertype with id: ".$new_utypeid." deleted<br>";
} else {
	echo "&emsp;&emsp;|- Usertype not deleted<br>";
}

echo "<br><br><br>";
// Create an empty user
$user = new User();

// Retrieve user with id 88 (ltonin)
try {
	$user->Retrieve(88);
} catch (Exception $e) {
	echo $e->getMessage(), "\n";
}

echo 'User retrieved from db: id|'.$user->Get('id').' nickname|'.$user->Get('nickname').' type|'.$user->Get('usertype::label')."<br>";

// Store new user
echo "<br><br>New user:<br>";
$new_user = new User();
$new_user->Set('usertype', $hackertype);

$new_user->Set('nickname', "cicciopasticcio");
if($new_user->Store() == true) {
	$new_userid = $new_user->Get('id');
	echo "&emsp;&emsp;|- New user stored with id: ".$new_userid."<br>";
} else {
	echo "&emsp;&emsp;|- User not stored<br>";
}

$new_user->Retrieve($new_userid);
echo 'New user retrieved: id|'.$new_user->Get('id').' nickname|'.$new_user->Get('nickname')." usertype|".$new_user->Get('usertype::label')."<br>";

$new_user->Set('nickname', "quiquoqua");
if($new_user->Store() == true) {
	$new_userid = $new_user->Get('id');
	echo "&emsp;&emsp;|- User with id: ".$new_userid." updated<br>";
} else {
	echo "&emsp;&emsp;|- User not updated<br>";
}

echo 'Updated user: id|'.$new_user->Get('id').' nickname|'.$new_user->Get('nickname')." usertype|".$new_user->Get('usertype::label')."<br>";


if($new_user->Drop() == true) {
	echo "&emsp;&emsp;|- User with id: ".$new_userid." deleted<br>";
} else {
	echo "&emsp;&emsp;|- User not deleted<br>";
}

?>

</body>
</html>
