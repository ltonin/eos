<?php
require_once(__DIR__."/eos_pod_element_database.php");
include_once(__DIR__."/eos_pod_issue.php");
include_once(__DIR__."/eos_pod_user.php");


class DOI extends ElementDB {
	function __construct($elname = "doi", $tbname = "article_doi") {
		parent::__construct($elname, $tbname);
		$this->Set('id',			NULL);
		$this->Set('article_id', 	NULL);
		$this->Set('doi',			NULL);
		$this->Set('creator',		NULL);
		$this->Set('status',		NULL);
		$this->Set('url',			NULL);
		$this->Set('title',			NULL);
		$this->Set('language',		NULL);
		$this->Set('publisher',		NULL);
		$this->Set('year',			NULL);
		$this->Set('resource',		NULL);
		$this->Set('abstract',		NULL);
		$this->Set('keywords',		NULL);
		$this->Set('created_by',	NULL);
		$this->Set('modified_by',	NULL);
		$this->Set('modified_on',	NULL);
	}

	public function Retrieve($id) {

		if(empty($id))
			return false;

		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result  = $stmt->get_result();
		$doi = $result->fetch_array();
		$stmt->close();
		$db->close();
		
		$this->Set('id',			$doi['id']);
		$this->Set('article_id', 	$doi['article_id']);
		$this->Set('doi',			$doi['doi']);
		$this->Set('creator', 	 	$doi['creator']);
		$this->Set('status', 	 	$doi['status']);
		$this->Set('url',			$doi['url']);
		$this->Set('title',			$doi['title']);
		$this->Set('language',		$doi['language']);
		$this->Set('publisher',		$doi['publisher']);
		$this->Set('year',			$doi['year']);
		$this->Set('resource',		$doi['resource']);
		$this->Set('abstract',		$doi['abstract']);
		$this->Set('keywords',		$doi['keywords']);
		$this->Set('modified_by',	$doi['modified_by']);
		$this->Set('modified_on',	$doi['modified_on']);

		
		return true;

	}
	
	public function RetrieveByArticle($article_id) {

		if(empty($article_id))
			return false;

		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `article_id`=?");
		$stmt->bind_param('i', $article_id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result  = $stmt->get_result();
		$doi = $result->fetch_array();
		$stmt->close();
		$db->close();

		if ( empty($doi) == true ) {
			return false;
		}

		$this->Set('id',			$doi['id']);
		$this->Set('article_id', 	$doi['article_id']);
		$this->Set('doi',			$doi['doi']);
		$this->Set('creator', 	 	$doi['creator']);
		$this->Set('status', 	 	$doi['status']);
		$this->Set('url',			$doi['url']);
		$this->Set('title',			$doi['title']);
		$this->Set('language',		$doi['language']);
		$this->Set('publisher',		$doi['publisher']);
		$this->Set('year',			$doi['year']);
		$this->Set('resource',		$doi['resource']);
		$this->Set('abstract',		$doi['abstract']);
		$this->Set('keywords',		$doi['keywords']);
		$this->Set('modified_by',	$doi['modified_by']);
		$this->Set('modified_on',	$doi['modified_on']);

		
		return true;

	}

	public function ArticleDoiIndex($issue_id, $article_id) {
		
		if(empty($article_id) || empty($issue_id)) 
			return false;

		$issue = new Issue();
		$issue->Retrieve($issue_id);

		$doi = $this->Get('doi');
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id` FROM `tb_articolo` WHERE `id_numero`=?");
		$stmt->bind_param('i', $issue_id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		$result  = $stmt->get_result();
		$indexes = $result->fetch_all(MYSQLI_ASSOC);
		$stmt->close();
		$db->close();


		$doi_index = array_search($article_id, array_column($indexes, 'id'));

		$doi_index_str = sprintf("%04d", $doi_index);
		return $doi_index_str;
	}

	public function Store() {

		$db = Database::Connect();

		
		$id				= $this->Get('id');			
		$article_id		= $this->Get('article_id');
		$doi			= $this->Get('doi');			
		$creator		= $this->Get('creator');
		$status			= $this->Get('status');
		$url			= $this->Get('url');
		$title			= $this->Get('title');
		$language		= $this->Get('language');
		$publisher		= $this->Get('publisher');
		$year			= $this->Get('year');
		$resource		= $this->Get('resource');
		$abstract		= $this->Get('abstract');
		$keywords		= $this->Get('keywords');
		$modified_by	= $this->Get('modified_by');
		$modified_on	= $this->Get('modified_on');


		
		if(empty($id)) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`article_id`, `doi`, `creator`, `status`, `url`, `title`, `language`, `publisher`, `year`, `resource`, `abstract`, `keywords`, `modified_by`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('isssssssisssi', $article_id,  $doi, $creator, $status, $url, $title, $language, $publisher, $year, $resource, $abstract, $keywords, $modified_by);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `article_id` = ?, `doi` = ?, `creator` = ?, `status` = ?, `url` = ?, `title` = ?, `language` = ?, `publisher` = ?, `year` = ?, `resource` = ?, `abstract` = ?, `keywords` = ?, `modified_by` = ? WHERE `id`=?");
			$stmt->bind_param('isssssssisssii', $article_id,  $doi, $creator, $status, $url, $title, $language, $publisher, $year, $resource, $abstract, $keywords, $modified_by, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);
		
		
		$stmt->close();
		$db->close();

		return true;
	}
}



?>

