<?php
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."/core/pod/eos_pod_table.php");

$table = new Table("tb_tipo");


$table->ClearConditions();
$table->SetCondition('nome', 'Copertina', ElementType::AsInteger);
$table->Select("id", "nome");

if($table->Get("id", $ids) > 0)
	print_r($ids);
if($table->Get("nome", $names) > 0)
	print_r($names);
?>
