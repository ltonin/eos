<?php
require_once(__DIR__."/eos_pod_element.php");
require_once(__DIR__."/eos_pod_element_database.php");

class QueryCondition extends Element {
	
	const IsEqual        = '=';
	const IsDifferent    = '<>';
	const IsGreater      = '>';
	const IsLesser       = '<';
	const IsGreaterEqual = '>=';
	const IsLesserEqual  = '<=';	

	function __construct($elname = "condition") {
		parent::__construct($elname);
	}
}

class QueryOrder extends Element {
	const AsAscending  = 'ASC';
	const AsDescending = 'DESC';	
	function __construct($elname = "order") {
		parent::__construct($elname);
	}
}

class Table {

	private $_name     = NULL;
	private $_result     = NULL;
	private $_orders     = array();
	private $_conditions = array();

	function __construct($tbname) {
		$this->_name = $tbname;
	}

	public function SetTable($name) {
		$this->_name 	   = $name;
		$this->_conditions = array();
		$this->_orders     = array();
		$this->_result     = NULL;
	}

	public function GetTable() {
		return $this->_name;
	}

	public function Select() {
		
		$selections = $this->prepare_query_selections(func_get_args());
		$conditions = $this->prepare_query_conditions();
		$orders     = $this->prepare_query_orders();
		
		$db = Database::Connect();
		$query  = "SELECT ".$selections." FROM `".$this->_name."`".$conditions.$orders;
		$stmt   = $db->prepare($query);
		if($stmt == false) {
				trigger_error("[eos::table] - Error while
					preparing statement. Probably requested
					fields have not been found in the table");
				die();
		}
		if(empty($this->_conditions) == false)
			$this->dynamic_bind_parameters($stmt);
		$stmt->execute();
		$this->_result = $stmt->get_result();
		$stmt->close();
		$db->close();

		// return something
	}

	public function GetResult() {
		return $this->_result;
	}

	public function Get($field, &$values, $index = NULL) {
		
		$nrows  = $this->_result->num_rows;
		$values = array();

		while($row = $this->_result->fetch_array())
			$values[] = $row[$field];
		$this->_result->data_seek(0);
		
		if(is_null($index) == false)
			$values = $values[$index];

		return $nrows;
	}

	public function Size($field) {
		$iarray = array();
		return $this->Get($field, $iarray);
	}

	public function SetCondition($field, $value, $type, $logic = QueryCondition::IsEqual) {

		$condition = new QueryCondition();
		$condition->Set("field", $field);
		$condition->Set("value", $value);
		$condition->Set("type",  $type);
		$condition->Set("logic", $logic);
		array_push($this->_conditions, $condition);
	}

	public function ClearConditions() {
		$this->_conditions = array();
	}

	public function SetOrder($field, $sort = QueryOrder::AsAscending) {
		
		$order = new QueryOrder();
		$order->Set('field', $field);
		$order->Set('sort', $sort);
		array_push($this->_orders, $order);
	}
	
	public function ClearOrders() {
		$this->_orders = array();
	}

	private function prepare_query_selections($list) {
		
		$selection = "";
		$count = 0;
		foreach($list as $csel) {
			if($count > 0) 
				$selection .=",";
			$selection .= " ".$csel."";
			$count++;
		}

		if(empty($selection)) 
			$selection = "*";

		return $selection;
	}

	private function prepare_query_conditions() {
		$conditions = "";
		if(empty($this->_conditions) == false) {
			$count = 0;
			$conditions = " WHERE";
			foreach($this->_conditions as $cond) {
				if ($count > 0)
					$conditions .= " AND ";
				if(is_null($cond->Get('value'))) 
					$conditions .= " (`".$cond->Get('field')."` IS NULL) ";	
				else
					$conditions .= " `".$cond->Get('field')."`".$cond->Get('logic')."? ";	
				$count++;
			}
		}

		return $conditions;
	}

	private function prepare_query_orders() {
		$orders = "";
		if(empty($this->_orders) == false) {
			$count = 0;
			$orders = " ORDER BY";
			foreach($this->_orders as $ord) {
				if($count > 0)
					$orders .= ", ";
				$orders .= " `".$ord->Get('field')."` ".$ord->Get('sort');
				$count++;
			}
		}
		return $orders;
	}

	private function dynamic_bind_parameters(&$stmt) {

		$pbinded = array();
		$pvalue  = array();
		$nparams = count($this->_conditions);
		$ptype = '';
		for($i = 0; $i < $nparams; $i++) {
			if(is_null($this->_conditions[$i]->Get('value')) == false)
				$ptype .= $this->_conditions[$i]->Get('type');
		}
		$pbinded[] = & $ptype;
				
		for($i = 0; $i < $nparams; $i++)
			$pvalue[] = $this->_conditions[$i]->Get('value');
		
		for($i = 0; $i < $nparams; $i++) {
			if(is_null($this->_conditions[$i]->Get('value')) == false)
				$pbinded[] =  & $pvalue[$i];
		}
		call_user_func_array(array($stmt, 'bind_param'), $pbinded);
	}

}
?>
