<?php
require_once(__DIR__."/eos_pod_element_database.php");


class Book extends ElementDB {
	
	public function __construct($elname = "book", $tbname = "issue_bookshop") {
		
		parent::__construct($elname, $tbname);
		
		$this->Set('id', 		NULL);
		$this->Set('issue_id', 		NULL);
		$this->Set('thumbnail', 	NULL);
		$this->Set('pdf_path', 		NULL);
		$this->Set('ebook_path',	NULL);
	}

	public function Retrieve($id) {
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result = $stmt->get_result();
		$book = $result->fetch_array();
		$stmt->close();
		$db->close();
		
		$this->Set('id', 		    $book['id']);
		$this->Set('issue_id', 		$book['issue_id']);
		$this->Set('thumbnail', 	$book['thumbnail']);
		$this->Set('pdf_path', 		$book['pdf_path']);
		$this->Set('ebook_path', 	$book['ebook_path']);

		return true;
	}

	public function RetrieveByIssue($id) {
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `issue_id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result = $stmt->get_result();
		$book = $result->fetch_array();

		$stmt->close();
		$db->close();
		
		if(isset($book) == false)
			return false;
		
		$this->Set('id', 		    $book['id']);
		$this->Set('issue_id', 		$book['issue_id']);
		$this->Set('thumbnail', 	$book['thumbnail']);
		$this->Set('pdf_path', 		$book['pdf_path']);
		$this->Set('ebook_path', 	$book['ebook_path']);

		return true;
	}

	public function RetrieveVolumes($issue_id) {
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `issue_id`=?");
		$stmt->bind_param('i', $issue_id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result = $stmt->get_result();
		
		$volumes = array();
		while($row = $result->fetch_array()) {
			$book = new Book();
			$book->Set('id', 		    $row['id']);
			$book->Set('issue_id', 		$row['issue_id']);
			$book->Set('thumbnail', 	$row['thumbnail']);
			$book->Set('pdf_path', 		$row['pdf_path']);
			$book->Set('ebook_path', 	$row['ebook_path']);
			
			array_push($volumes, $book);
		}


		$stmt->close();
		$db->close();
		
		//$this->Set('id', 		    $book['id']);
		//$this->Set('issue_id', 		$book['issue_id']);
		//$this->Set('thumbnail', 	$book['thumbnail']);
		//$this->Set('pdf_path', 		$book['pdf_path']);
		//$this->Set('ebook_path', 	$book['ebook_path']);

		return $volumes;
	}

	public function Store() {

		$db = Database::Connect();
		$id       	= $this->Get('id');
		$issue_id 	= $this->Get('issue_id');
		$thumbnail  = $this->Get('thumbnail');
		$pdf_path  	= $this->Get('pdf_path');
		$ebook_path = $this->Get('ebook_path');

		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`issue_id`, `thumbnail`, `pdf_path`, `ebook_path`) VALUES (?, ?, ?, ?)");
			$stmt->bind_param('isss', $issue_id, $thumbnail, $pdf_path, $ebook_path);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `issue_id`= ?, `thumbnail`=?, `pdf_path`=?, `ebook_path`=? WHERE `id`=?");
			$stmt->bind_param('isssi', $issue_id, $thumbnail, $pdf_path, $ebook_path, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);

		$stmt->close();
		$db->close();

		return true;
	}

}

?>
