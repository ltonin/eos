<?php
require_once(__DIR__."/eos_pod_element_database.php");
include_once(__DIR__."/eos_pod_issue.php");
include_once(__DIR__."/eos_pod_user.php");

class ArticleType extends ElementDB {

	function __construct($elname = "type", $tbname = "tb_tipo") {
		parent::__construct($elname, $tbname);
		$this->Set("id", 	NULL);
		$this->Set("label", 	NULL);
	}

	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id`, `nome` FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}


		$stmt->bind_result($id, $label);
		$stmt->fetch();
		$stmt->close();
		$db->close();
		
		$this->Set("id", 	$id);
		$this->Set("label", 	$label);

		return true;
	}


	public function Store() {
		$db = Database::Connect();
		$id    = $this->Get("id");	
		$label = $this->Get("label");	

		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`nome`) VALUES (?)");
			$stmt->bind_param('s', $label);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `nome`= ? WHERE `id`=?");
			$stmt->bind_param('si', $label, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		$this->Set("id", $stmt->insert_id);
		
		$stmt->close();
		$db->close();

		return true;
	}
}

class ArticleStatus extends ElementDB {

	function __construct($elname = "status", $tbname = "article_status") {
		parent::__construct($elname, $tbname);
		$this->Set("id", 	NULL);
		$this->Set("label", 	NULL);
	}

	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id`, `label` FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		$stmt->bind_result($id, $label);
		$stmt->fetch();
		$stmt->close();
		$db->close();
		
		$this->Set("id", 	$id);
		$this->Set("label", 	$label);

		return true;
	}

	public function Store() {
		$db = Database::Connect();
		$id    = $this->Get("id");	
		$label = $this->Get("label");	
		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`label`) VALUES (?)");
			$stmt->bind_param('s', $label);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `label`= ? WHERE `id`=?");
			$stmt->bind_param('si', $label, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);

		
		$stmt->close();
		$db->close();

		return true;
	}
}


class ArticleSection extends ElementDB {

	function __construct($elname = "section", $tbname = "tb_categoria") {
		parent::__construct($elname, $tbname);
		$this->Set("id", 	NULL);
		$this->Set("label", 	NULL);
		$this->Set("position", 	NULL);
	}

	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id`, `nome`, `pos` FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);

		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$stmt->bind_result($id, $label, $position);
		$stmt->fetch();
		$stmt->close();
		$db->close();
		
		$this->Set("id", 	$id);
		$this->Set("label", 	$label);
		$this->Set("position", 	$position);

		return true;
	}

	public function Store() {
		$db = Database::Connect();
		$id    	  = $this->Get("id");	
		$label 	  = $this->Get("label");	
		$position = $this->Get("position");	
		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`nome`, `pos`) VALUES (?, ?)");
			$stmt->bind_param('si', $label, $position);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `nome`= ?, `pos`=? WHERE `id`=?");
			$stmt->bind_param('sii', $label, $position, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);

		
		$stmt->close();
		$db->close();

		return true;
	}
}


class Article extends ElementDB {
	function __construct($elname = "article", $tbname = "tb_articolo") {
		parent::__construct($elname, $tbname);
		$this->Set('id', 	 	NULL);
		$this->Set('issue', 	 	new Issue());
		$this->Set('user', 	 	new User());
		$this->Set('type',		new ArticleType());
		$this->Set('section', 		new ArticleSection());
		$this->Set('status',  		new ArticleStatus());
		$this->Set('title', 	 	NULL);
		$this->Set('text', 	 	NULL);
		$this->Set('created', 	 	NULL);
		$this->Set('modified', 	 	NULL);
		$this->Set('modifiedby', 	new User());
		$this->Set('position', 	 	NULL);
	}

	public function Retrieve($id) {
		if(empty($id) || is_null($id))
			return false;

		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		$result  = $stmt->get_result();
		$article = $result->fetch_array();
		$stmt->close();
		$db->close();

		if(is_null($article))
			return false;

		$this->Set('id',		$article['id']);
		$this->Set('title', 	 	$article['titolo']);
		$this->Set('text', 	 	$article['testo']);
		$this->Set('created', 	 	$article['data_creazione']);
		$this->Set('modified', 	 	$article['data_modifica']);
		$this->Set('position',   	$article['pos']);
		
		$this->Get('issue')->Retrieve($article['id_numero']);
		$this->Get('user')->Retrieve($article['autore']);
		$this->Get('type')->Retrieve($article['tipo']);
		$this->Get('section')->Retrieve($article['categoria']);
		$this->Get('status')->Retrieve($article['stato']);
		$this->Get('modifiedby')->Retrieve($article['modificato_da']);
		
		return true;

	}

	public function Store() {

		$db = Database::Connect();
		$id         = $this->Get('id');
		$issueid    = $this->Get('issue::id');
		$userid     = $this->Get('user::id');
		$typeid     = $this->Get('type::id');
		$sectionid  = $this->Get('section::id');
		$statusid   = $this->Get('status::id');
		$title 	    = $this->Get('title');
		$text  	    = $this->Get('text');
		$modified   = $this->Get('modified');
		$modifiedby = $this->Get("modifiedby::id");
		$position   = $this->Get('position');
		$created    = date("Y-m-d H:i:s");
		
		
		if(is_null($id)) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`id_numero`, `titolo`, `autore`, `testo`, `data_creazione`, `modificato_da`, `tipo`, `categoria`, `stato`, `pos`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('isissiiiii', $issueid,  $title, $userid, $text, $created, $modifiedby, $typeid, $sectionid, $statusid, $position);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `id_numero`= ?, `titolo`=?, `autore`=?, `testo`=?, `modificato_da`=?, `tipo`=?, `categoria`=?, `stato`=?, `pos`=? WHERE `id`=?");
			$stmt->bind_param('isisiiiiii', $issueid,  $title, $userid, $text, $modifiedby, $typeid, $sectionid, $statusid, $position, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);
		
		
		$stmt->close();
		$db->close();

		return true;
	}
}

?>
