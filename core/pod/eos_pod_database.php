<?php
require_once(__DIR__."/../eos_configuration_init.php");

class Database {

	public static function Connect($filename = EOS_CONFIG_BASEPATH) {
		$config = parse_ini_file($filename, true);

		if($config == false) {
			trigger_error('[eos::configuration] - Error parsing configuration file');
			return false;
		}

		$config = $config['database'];

		//$connection = new mysqli($config['host'], 
		$connection = new mysqli("p:".$config['host'], 
							$config['user'], 
							$config['pwd'], 
							$config['name']);

		if ($connection == false)
			die('[eos::mysql] - Connection to database failed: '.$connection->connect_error);

		return $connection;
	}
}

?>
