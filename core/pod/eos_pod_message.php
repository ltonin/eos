<?php

class MessageType {
	const AsInfo    = 0;
	const AsWarning = 1;
	const AsLog     = 2;
	const AsDebug   = 3;
	const AsError   = 4;
}

class Message {

	private $list  = array();
	private $messageid;
	private $typeclass = array("info", "warning", "log", "debug", "error");

	function __construct($messageid = "backend_message") {
		$this->messageid = $messageid; 
	}

	public function AddMessage($string, $type = MessageType::AsInfo) {

		$msg  = "<p class=".$this->GetCss($type).">";
		$msg .= $this->gettag($type)." - ".$string;
		$msg .= "</p>";

		$this->addtoqueue($msg);
	}

	public function GetCss($type) {
		return $this->typeclass[$type];
	}

	public function SetCss($css, $msgtype) {
		$this->typeclass[$msgtype] = $css;
	}

	public function Clear() {
		unset($this->list);
		$this->list = array();
	}

	public function Show() {
		if(empty($this->list) == false) {
			$msg  = "<div id=".$this->messageid.">";
			$msg .= implode("", $this->list);
			$msg .= "</div>";
			print $msg;
		}
	}

	protected function addtoqueue($msg) {
		array_push($this->list, $msg);
	}

	protected function gettag($type) {
		switch($type) {
			case MessageType::AsInfo:
				$tag = "[eos::info]";
				break;
			case MessageType::AsWarning:
				$tag = "[eos::warning]";
				break;
			case MessageType::AsLog:
				$tag = "[eos::log]";
				break;
			case MessageType::AsDebug:
				$tag = "[eos::debug]";
				break;
			case MessageType::AsError:
				$tag = "[eos::error]";
				break;
		}

		return $tag;
	}




}

?>
