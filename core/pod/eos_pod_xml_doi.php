<?php 

class XmlDOI {

	protected $resource;

	function __construct($xml_template) {

		if($this->init_xml_template($xml_template) === false) {
			exit("Failed to load XML template");
		}
	}

	protected function init_xml_template($xml_template) {

		$res = false;
		if(file_exists($xml_template)) {
			$this->_resource = simplexml_load_file($xml_template);
			$res = true;
		}

		return $res;
	}

	// Mandatory fields	
	public function SetDoi($input) {
		$this->_resource->identifier = $input;
	}

	public function SetAuthor($input) {
		$this->_resource->creators->creator[0]->creatorName = htmlspecialchars($input);
	}
	
	public function SetTitle($input) {
		$this->_resource->titles->title[0] = htmlspecialchars($input);
	}
	
	public function SetPublisher($input) {
		$this->_resource->publisher = htmlspecialchars($input);
	}
	
	public function SetYear($input) {
		$this->_resource->publicationYear = $input;
	}
	
	public function SetLanguage($input) {
		$this->_resource->language = $input;
	}
	
	public function SetResourceType($input) {
		$this->_resource->resourceType = $input;
	}

	// Optional/recommended fields	
	public function SetAbstract($input) {

		if(empty($input)) {
			return;
		}

		if(count((array)$this->_resource->descriptions->children()) == 0) {
			$this->_resource->AddChild('descriptions');
		}

		$isfield = false;
		$idx = 0;
		foreach ($this->_resource->descriptions->description as $desc) {
			if((string) $desc['descriptionType'] == "Abstract") {
				$this->_resource->descriptions->description[$idx] = $input;
				var_dump($desc);
				$isfield = true;
			}
			$idx++;
		}

		if($isfield == false) {	
			$this->_resource->descriptions->AddChild('description', htmlspecialchars($input) );
			$this->_resource->descriptions->description->addAttribute('descriptionType', "Abstract");
		}
	}

	public function SetKeywords($input, $delimiter=";") {
		if(empty($input)) {
			return;	
		}

		$input = htmlspecialchars($input);

		$keywords = explode($delimiter, $input);
		
		if(count((array)$this->_resource->subjects->children()) == 0) {
			$this->_resource->AddChild('subjects');
		}
		
		foreach($keywords as $value) {
			$this->_resource->subjects->AddChild("subject", ltrim(rtrim($value)));		
		}
	}	

	public function SetAlternateIdentifier($input = "", $type = NULL) {
		
		if(empty($input)) {
			return;
		}

		if(count((array)$this->_resource->alternateIdentifiers->children()) == 0) {
			$this->_resource->AddChild('alternateIdentifiers');
		}

		$this->_resource->alternateIdentifiers->AddChild('alternateIdentifier', $input);

		if(is_null($type) == false) {
			$this->_resource->alternateIdentifiers->alternateIdentifier->addAttribute('alternateIdentifierType', $type);
		}
	}

	public function SetRelatedIdentifier($input = "", $type = NULL, $relationType = NULL) {
	
		if(empty($input)) {
			return;
		}
		
		if(count((array)$this->_resource->relatedIdentifiers->children()) == 0) {
			$this->_resource->AddChild('relatedIdentifiers');
		}

		$this->_resource->relatedIdentifiers->AddChild('relatedIdentifier', $input);

		if(is_null($type) == false) {
			$this->_resource->relatedIdentifiers->relatedIdentifier->addAttribute('relatedIdentifierType', $type);
		}
		
		if(is_null($relationType) == false) {
			$this->_resource->relatedIdentifiers->relatedIdentifier->addAttribute('relationType', $relationType);
		}

	
	}

	public function SetRights($input = "", $uri = NULL) {

		if(empty($input)) {
			return;
		}

		if(count((array)$this->_resource->rightsList->children()) == 0) {
			$this->_resource->AddChild('rightsList');
		}
		
		$this->_resource->rightsList->AddChild('rights', $input);

		if(is_null($uri) == false) {
			$this->_resource->rightsList->rights->addAttribute('rightsURI', $uri);
		}


	}

	public function Display() {
		echo $this->_resource->asXML();
	}

	public function GetXML() {

		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;
		$dom->loadXML($this->_resource->asXML());
		return $dom->saveXML();
	}

/*
	protected function init_xml() {
		
		$this->_resource = new SimpleXmlElement('<resource></resource>');
		$this->_resource->addAttribute('xmlns', 'http://datacite.org/schema/kernel-4');
		$this->_resource->addAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$this->_resource->addAttribute('xsi:schemaLocation', 'http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd');

		$identifier = $this->_resource->AddChild('identifier', '#');
		$identifier->addAttribute('identifierType', 'DOI');

		$creators	 = $this->_resource->AddChild('creators');
		$creator  	 = $creators->AddChild('creator');
		$creatorName = $creator->AddChild('creatorName', '#');

		$titles = $this->_resource->AddChild('titles');
		$title  = $titles->AddChild('title', '#');

		$publisher = $this->_resource->AddChild('publisher', '#');

		$publicationYear = $this->_resource->AddChild('publicationYear', '#');

		$dates = $this->_resource->AddChild('dates');
		$date = $dates->AddChild('date', '#');
		$date->addAttribute('dateType', 'Submitted');
		$date = $dates->AddChild('date', '#');
		$date->addAttribute('dateType', 'Accepted');
		$date = $dates->AddChild('date', '#');
		$date->addAttribute('dateType', 'Updated');
		$date = $dates->AddChild('date', '#');
		$date->addAttribute('dateType', 'Issued');

		$language = $this->_resource->AddChild('language', '#');

		$resourceType = $this->_resource->AddChild('resourceType', '#');
		$resourceType->addAttribute('resourceTypeGeneral', 'Text');

		$alternateIdentifiers = $this->_resource->AddChild('alternateIdentifiers');
		$alternateIdentifier  = $alternateIdentifiers->AddChild('alternateIdentifier', '#');
		$alternateIdentifier->addAttribute('alternateIdentifierType', 'publisherId');

		$relatedIdentifiers = $this->_resource->AddChild('relatedIdentifiers');
		$relatedIdentifier  = $relatedIdentifiers->AddChild('relatedIdentifier', '#');
		$relatedIdentifier->addAttribute('relatedIdentifierType', 'DOI');
		$relatedIdentifier->addAttribute('relationType', 'IsPartOf');

		$rightsList = $this->_resource->AddChild('rightsList');
		$rights     = $rightsList->AddChild('rights', '#');
		$rights->addAttribute('rightsURI', 'https://creativecommons.org/licenses/by-nc-sa/4.0/');

		$descriptions = $this->_resource->AddChild('descriptions');
		$description  = $descriptions->AddChild('description', '#');
		$description->addAttribute('descriptionType', 'Abstract');
		$description  = $descriptions->AddChild('description', '#');
		$description->addAttribute('descriptionType', 'SeriesInformation');

	}
 */
}
?>
