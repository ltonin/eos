<?php
require_once(__DIR__."/../eos_configuration_init.php");

class ElementType {

	const AsInteger = 'i';
	const AsDouble  = 'd';
	const AsString  = 's';
	const AsBlob    = 'b';
}

abstract class Element {
	
	protected $_name = '';
	protected $_fields = array();
	
	function __construct($name) {
		$this->_name = $name;
	}

	public function Get($expression) {
		if(is_string($expression) == false) {
			trigger_error("[eos::element] Error: only string keys are allowed for element fields");
		}

		$keylist = explode("::", $expression);
		
		// If count(keylist) == 0, only one element requested, return it	
		if(count($keylist) == 1) {
			return $this->GetField($keylist[0]);
		}

		$newexpression = implode("::", array_slice($keylist, 1));
		$currobj = $this->GetField($keylist[0]);
		if(is_object($currobj) == false) {
			trigger_error("[eos::element] Error: '".$currobj."' is not an eos::element");
		}

		return $currobj->Get($newexpression);
	}

	public function Set($key, $value) {

		if($this->IsField($key)) {
			$this->UpdateField($key, $value);
		} else {
			$this->SetField($key, $value);
		}

	}

	protected function IsField($key) {
		return array_key_exists($key, $this->_fields);
	}


	protected function GetField($key) {
		if (array_key_exists($key, $this->_fields)) {
			return $this->_fields[$key];
		} else {
			trigger_error("[eos::element] Error: ".$this->_name." does not have field '".$key."'");
		}
	}

	protected function SetField($key, $value) {
		if(is_string($key) == false) {
			trigger_error("[eos::element] Error: only string keys are allowed for element fields");
		}
		$narray = array($key => $value); 
		$this->_fields = array_merge($this->_fields, $narray);
	}

	protected function UpdateField($key, $value) {
		if (array_key_exists($key, $this->_fields)) {
			$this->SetField($key, $value);
		} else {
			trigger_error("[eos::element] Error: ".$this->_name." does not have field '".$key."' to be updated");
		}
	}

	public function Dump($key) {
		print $this->Get($key);
	}

	public function GetName() {
		return $this->_name;
	}
	
	public function SetName($name) {
		$this->_name = $name;
	}

}
?>
