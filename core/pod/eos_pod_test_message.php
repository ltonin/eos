<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once(__DIR__."/../eos_configuration_init.php");
include_once(EOS_BASEPATH."core/pod/eos_pod_message.php");

$message = new Message();
?>

<html>
<head>
<style>

.msgbox {
	border:1px solid #2f608c;
	background-color: #d7ecff;
	margin: 5px;
}

.msgbox p {
	font-family:Verdana, Arial, Helvetica,sans-serif;
	font-size:11px;
	font-weight: bold;
	
}

.msginfo {
	color: #006699; 
}

.msgwarning {
	color: #cc9900;
}

.msglog {
	color: #363636;
}

.msgdebug {
	color: #660066;
}

.msgerror {
	color: #990000;
}

</style>
</head>
<body>

<?php 
$message->AddMessage("Info message", MessageType::AsInfo);
$message->Show();
$message->Clear();

$message->AddMessage("Warning message", MessageType::AsWarning);
$message->Show();
$message->Clear();

$message->AddMessage("Log message", MessageType::AsLog);
$message->Show();
$message->Clear();

$message->AddMessage("Debug message", MessageType::AsDebug);
$message->Show();
$message->Clear();

$message->AddMessage("Error message", MessageType::AsError);
$message->Show();
$message->Clear();


$message->AddMessage("Info message", MessageType::AsInfo);
$message->AddMessage("Warning message", MessageType::AsWarning);
$message->AddMessage("Log message", MessageType::AsLog);
$message->AddMessage("Debug message", MessageType::AsDebug);
$message->AddMessage("Error message", MessageType::AsError);
$message->Show();

?>
</body>
</html>
