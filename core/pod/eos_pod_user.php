<?php
require_once(__DIR__."/eos_pod_element_database.php");

class UserType extends ElementDB {

	function __construct($elname = "type", $tbname = "user_type") {

		parent::__construct($elname, $tbname);

		$this->Set("id", 	NULL);
		$this->Set("label", 	NULL);
	}
	
	public function Retrieve($id) {
		
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT `id`, `label` FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		$stmt->bind_result($id, $label);
		$stmt->fetch();
		$stmt->close();
		$db->close();
		
		$this->Set("id", $id);
		$this->Set("label", $label);

		return true;
	}


	public function Store() {
		$db = Database::Connect();
		$id    = $this->Get("id");	
		$label = $this->Get("label");	

		if(is_null($id) == true) {
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`label`) VALUES (?)");
			$stmt->bind_param('s', $label);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `label`= ? WHERE `id`=?");
			$stmt->bind_param('si', $label, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		if(is_null($id))
			$this->Set('id', $stmt->insert_id);
		
		$stmt->close();
		$db->close();

		return true;
	}
}

class User extends ElementDB {
	
	public function __construct($elname = "user", $tbname = "tb_utente") {
		
		parent::__construct($elname, $tbname);

		$this->Set('id', 	NULL);
		$this->Set('nickname', 	NULL);
		$this->Set('password', 	NULL);
		$this->Set('type', 	new UserType());
		$this->Set('firstname', NULL) ;
		$this->Set('lastname', 	NULL);
		$this->Set('email', 	NULL);
		$this->Set('created', 	NULL);
		$this->Set('modified', 	NULL);
	}

	public function Retrieve($id) {
		$db = Database::Connect();
		$stmt=$db->prepare("SELECT * FROM `".$this->_tbname."` WHERE `id`=?");
		$stmt->bind_param('i', $id);
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}
		
		$result = $stmt->get_result();
		$user = $result->fetch_array();
		$stmt->close();
		$db->close();

		if(isset($user) == false)
			return false;

		$this->Set('id', 	$user['id']);
		$this->Set('nickname', 	$user['nickname']);
		$this->Set('password', 	$user['pwd']);
		$this->Set('firstname', $user['firstname']);
		$this->Set('lastname', 	$user['lastname']);
		$this->Set('email', 	$user['email']);
		$this->Set('created', 	$user['created']);
		$this->Set('modified', 	$user['modified']);
		
		$this->Get('type')->Retrieve($user['tipo']);

		return true;
	}

	public function Exist() {
		
		$nickname = $this->Get("nickname");
		$checkuser = new User();
		$checkuser->RetrieveBy('nickname', ElementType::AsString, $nickname);	
		if(is_null($checkuser->Get("id"))) {
			return false;
		} 			
		return true;
	}

	public function IsAuthorized() {
		$nargs 	  = func_num_args();
		$arglist  = array();
		$isauth   = false;
		$usertype = $this->Get("type::label");

		if($nargs == 0) {

			$db = Database::Connect();
			$stmt = $db->prepare("SELECT * FROM `user_type`");
			$stmt->execute();
			$result = $stmt->get_result();

			while($row = $result->fetch_array())
				array_push($arglist, $row['label']);
		} else {
			$arglist = func_get_args();
		}
		
		foreach($arglist as $carg) {
			if($carg === $usertype) {
				$isauth = $isauth || true;
			}
		}

		return $isauth;
	}

	public function Store() {

		$db = Database::Connect();
		$id       	= $this->Get('id');
		$nickname 	= $this->Get('nickname');
		$utypeid  	= $this->Get('type::id');
		$password 	= $this->Get('password');
		$firstname 	= $this->Get('firstname');
		$lastname 	= $this->Get('lastname');
		$email 		= $this->Get('email');

		if(is_null($id)) {

			$created    = date("Y-m-d H:i:s");
			$stmt = $db->prepare("INSERT INTO `".$this->_tbname."` (`nickname`, `pwd`, `tipo`, `firstname`, `lastname`, `email`, `created`) VALUES (?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('ssissss', $nickname, $password, $utypeid, $firstname, $lastname, $email, $created);
		} else {
			$stmt = $db->prepare("UPDATE `".$this->_tbname."` SET `nickname`= ?, `pwd`=?, `tipo`=?, `firstname`=?, `lastname`=?, `email`=?  WHERE `id`=?");
			$stmt->bind_param('ssisssi', $nickname, $password, $utypeid, $firstname, $lastname, $email, $id);
		}
		
		if($stmt->execute() == false) {
			$merror = $stmt->error;
			trigger_error('[mysql::'.$this->_name.'] '.$merror); 
			return false;
		}

		if(is_null($id))
			$this->Set('id', $stmt->insert_id);

		
		$stmt->close();
		$db->close();

		return true;
	}
}
?>
