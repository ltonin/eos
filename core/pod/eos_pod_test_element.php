<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_element.php");

class ElementTest extends Element{};


$expression = "element2::element1::id";
$element1 = new ElementTest("element1");
$element2 = new ElementTest("element2");
$element3 = new ElementTest("element3");


$element1->Set("id", "id1");
$element2->Set("id", "id2");
$element3->Set("id", "id3");

$element2->Set("element1", $element1);
$element3->Set("element2", $element2);

echo $element3->Get("element2::id");


?>
