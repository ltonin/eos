<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/******************************************************************************/
/* 						       Categories                                     */
/******************************************************************************/
$tbcat = new Table('tb_categoria');
$tbcat->SetCondition('is_visible', 1, ElementType::AsInteger);
$tbcat->SetOrder('pos');
$tbcat->Select('id', 'nome');

$result_cat = $tbcat->GetResult();

// Get articles per categories
$categories = array();
while($rowcat = $result_cat->fetch_array()) {
	$catentry['id']   = $rowcat['id'];
	$catentry['nome'] = $rowcat['nome'];
	$tbart = new Table("tb_articolo");
	$tbart->SetCondition('stato', 1, ElementType::AsInteger);
	$tbart->SetCondition('categoria', $rowcat['id'], ElementType::AsInteger);
	$tbart->SetOrder('pos');
	$tbart->Select('id', 'titolo');
	$result_art = $tbart->GetResult();

	$articles = array();
	while($rowart = $result_art->fetch_array()) {
		$article_entry['id']    = $rowart['id'];
		$article_entry['title'] = $rowart['titolo'];
		$articles[] = $article_entry;
	}	
	$catentry['articles'] = $articles;

	$categories[] = $catentry;
}

/******************************************************************************/
/* 						       Issues/Archivio                                */
/******************************************************************************/

$tbissue = new Table('tb_numero');
$tbissue->SetCondition('stato', 3, ElementType::AsInteger);
$tbissue->SetOrder('numero');
$tbissue->Select('numero');

$result_issues = $tbissue->GetResult();

$issues = array();
while($rowissue = $result_issues->fetch_array()) {
	$issues[] = $rowissue['numero'];
}

?>

<div class="eos-footer-container">

<?php foreach($categories as $cat) { ?>
<div class="eos-footer-item">
	<div class="eos-footer-item-title"><?php print $cat['nome']; ?></div>

	<div class="eos-footer-item-content">
		
		<?php  foreach($cat['articles'] as $art) { ?>
		<div class="eos-footer-link">
			<a href="<?php print EOS_BASEURL.'index.php?id_articolo='.$art['id']; ?>">
				<?php print $art['title']; ?>
			</a>
		</div>
		<?php } ?>

	</div>

</div>
<?php } ?>

<div class="eos-footer-item">
	<div class="eos-footer-item-title">libreria</div>
	
	<div class="eos-footer-item-content">
		<div class="eos-footer-link">
			<a href="index.php?libreria">libreria</a>
		</div>
	</div>
</div>

<div class="eos-footer-item-archive">
	<div class="eos-footer-item-title">archivio</div>

	<div class="eos-footer-archive-content">
		<div class="eos-footer-archive-link"><a href="">pdf</a></div>
		<?php foreach($issues as $issue) { ?> 
		<div class="eos-footer-archive-link">
		<a href="<?php print EOS_BASEURL.'core/frontend/eos_goto.php?issue='.$issue; ?>">
			<?php print $issue; ?>
		</a>
		</div>
		<?php } ?>
	</div>
</div>

</div>
