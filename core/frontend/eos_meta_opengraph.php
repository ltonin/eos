<?php
/*
if(isset($_SESSION['issueshown'])){
	$ogtitle="engramma n. ".$_SESSION['issueshown'];
} else {
	$ogtitle="engramma - la tradizione classica nella memoria occidentale";
}
 */
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");

$ogissue="";

if(isset($_GET['issue'])){
	$ogissue=$_GET['issue'];
}

if(isset($_GET['id_articolo'])) {
	$articleid=$_GET['id_articolo'];

	$article = new Article();
	if($article->Retrieve($articleid) == true) {
		$issue = $article->Get("issue");
		$ogissue = $issue->Get("number");
	}
}

if(empty($ogissue) == false) {
	$ogtitle="engramma n. ".$ogissue;
} else {
	$ogtitle="engramma - la tradizione classica nella memoria occidentale";
}


?>


<meta property="og:title" content="<?php print $ogtitle; ?>" >
<meta property="og:type" content="article">

<!--
<meta property="og:site_name" content="Our Website">
<meta property="og:url" content="http://www.example.org/ogp_test.html">
<meta property="og:image" content="http://www.example.org/teddy.png">
<meta property="og:image:type" content="image/jpg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="627" />
<meta property="og:image:alt" content="A teddy bear" />
<meta property="article:published_time" content="2019-04-25">
<meta property="article:author" content="Carl">
-->
