<?php 
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");

// Not found article id
$notfound_id = 3242;

$article = new Article();
if($article->Retrieve($notfound_id) == false)
	eos_redirect(EOS_BASEURL.'index.php?error=1');

?>
<script type="text/javascript">
$(document).ready(function(){
	//Examples of how to assign the ColorBox event to elements
	$(".popbox").colorbox( {
		rel:'nofollow',
		transition:'none',
		innerWidth:'45%', 
		right:'2%',
		maxHeight:'90%',
		opacity:'0'
	});
	$(".slideshow").colorbox({
		rel:'nofollow',
		transition:'none',
		innerWidth:'45%',
		maxHeight:'90%',
		opacity:'0'
	});
	$(".box").colorbox({
		rel:'nofollow',
		transition:'none',
		innerWidth:'85%'
	});
	$(".bbox").colorbox({
		rel:'nofollow',
		transition:'none',
		maxWidth:'85%',
		maxHeight:'90%'
	});
});
</script>		
<body  class="article">
<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>

 <div id="article">
  <?php
 print $article->Get("text"); ?>
 </div>


