<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");

$table = new Table('issue_status');
$table->SetCondition('label', 'Archiviato', ElementType::AsString);
$table->Select('id');
$table->Get('id', $archiveid, 0);

$table = new Table('tb_numero');
$table->SetCondition('stato', $archiveid, ElementType::AsInteger);
$table->SetOrder('numero', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $listissue);

$table = new Table('tb_numero');
$table->SetCondition('stato', $archiveid, ElementType::AsInteger);
$table->SetOrder('numero', QueryOrder::AsAscending);
$table->Select('id');
$table->Get('id', $listissue2);

?>
<?php
/*
<style>
h2.titolo{
	font: normal 36pt "<?php print EOS_FONTS_BASEURL; ?>Museo500-Regular",verdana, sans-serif;
	text-align: center;
}

div.issue{
	font: normal 10pt "<?php print EOS_FONTS_BASEURL; ?>Museo500-Regular",verdana, sans-serif;
}

</style>			
 */
?>
<body class="article">
<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
<div id="corpo">
<div class="archive">
<h2 class="titolo">Archivio</h2>

<?php

foreach($listissue2 as $issueid) {
	$issue = new Issue();
	$issue->Retrieve($issueid);
	$href  = EOS_BASEURL.'core/frontend/eos_goto.php?issue='.$issue->Get('number');
	$title = $issue->Get('number')." | ".$issue->Get('special'); 
?> 
<a target="_blank"  href="<?php print $href ?>" class="numbox" title="<?php print $title; ?>" >
<?php print $issue->Get('number'); ?>
</a>  
<?php
}
?>





<div class="dida">Le versioni pdf dei numeri di Engramma sono in corso di pubblicazione e via via saranno scaricabili dalla pagina
<a class="dida" href="<?php print EOS_BASEURL.'index.php?libreria'; ?>" target="_blank">Libreria</a>.<br />
</div>
<hr>
<?php
	foreach($listissue as $issueid) {
		$issue = new Issue();
		$issue->Retrieve($issueid);
		$description = $issue->Get('description');
		if(empty($description) == false)
			$description = " - ".$issue->Get('description');
		$title ="n. ".$issue->Get('number')." ".$issue->Get('special').$description;
		$href    = EOS_BASEURL.'core/frontend/eos_goto.php?issue='.$issue->Get('number');

?>
<a class="issue" href="<?php print $href; ?>" ><?php print $title; ?>
</a><br>

<?php
	}
?>
<hr />
<br>
<br>
</div>
</div>
</body>
