<?php
require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
//$urlroot = EOS_BASEURL;
#$urlroot = '/var/www/engramma/eOS/';
$urlroot = '/var/www/html/eOS/';

$imgname = $urlroot.'core/img/grid6x8.png';
$imbase  = $urlroot.'core/img/noimage.jpg';
$hexa    ='#ff0000';	

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);
   $r = hexdec(substr($hex,0,2));
   $g = hexdec(substr($hex,2,2));
   $b = hexdec(substr($hex,4,2));

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   }
   $rgb = array($r, $g, $b);
   return $rgb;
}

//verifica se la cover è nei numeri con immagine in db
if(isset($_GET['num']) ) {
	$issue = new Issue();
	$issue->RetrieveBy("numero", ElementType::AsInteger, $_GET['num']);
	$number=$issue->Get("number");

	if($number<999) {
		if($issue->Get("img")!='') {
			$urlbase=$issue->Get("img");
			//echo "ciao: ".$imbase;
		}
		if($issue->Get("color")!='') {
			$hexa=$issue->Get("color");
		}
		
	}
}
$rgb=hex2rgb ($hexa);
// Creo l'immagine della griglia base
$im = imagecreatefrompng($imgname);
if(!$im)
    {
        // Create a blank image 
        $im  = imagecreatetruecolor(150, 30);
        $bgc = imagecolorallocate($im, 255, 255, 255);
        $tc  = imagecolorallocate($im, 0, 0, 0);

        imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

        // Output an error message 
        imagestring($im, 1, 5, 5, '' . $imgname, $tc);
}
// tpglie il nero dalla palette immagine
imagecolordeallocate($im,1);
//mette nella palette il nuovo colore al posto del nero
imagecolorallocatealpha($im, $rgb[0], $rgb[1], $rgb[2], 0);
// cambia i puntini neri in puntini colorati
imagecolorset($im, 1, $rgb[0], $rgb[1], $rgb[2]);

//creo l'immagine di base
//echo $imgname;
$parseurl = parse_url($urlbase);
$relpath = $parseurl["path"];
//echo "RelPath: " . $relpath;
//$imbase = '/var/www/engramma/' . $relpath;
$imbase = '/var/www/html/' . $relpath;
$base=imagecreatefromjpeg($imbase);
if(!$base)
    {
        // Create a blank image
        $base  = imagecreatetruecolor(600, 800);
        $bgc = imagecolorallocate($base, 255, 255, 255);
        $tc  = imagecolorallocate($base, 0, 0, 0);

        imagefilledrectangle($base, 0, 0, 150, 30, $bgc);

        // Output an error message 
        imagestring($base, 1, 5, 5, '' . $imbase, $tc);
}

//applico la griglia colorata
imagecopymerge($base, $im, 0, 0, 0, 0, 600, 800, 100);
header('Content-Type: image/png');

imagepng($base);
imagepng($im);
imagedestroy($base);
imagedestroy($im);
?>
