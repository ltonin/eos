
<div style="color:white; font-weight: normal; font-family: EngrammaDemi; font-size: 36px; text-align: left; padding-top: 40px; padding-left: 60px; padding-bottom: 40px; padding-right: 40px; ">
dona il 2x1000 a Associazione Culturale Engramma
CF 94051700279*
</div>

<div style="color:white; font-family: EngrammaRegular; font-size: 16px; text-align: left; padding-top: 40px; padding-left: 60px; padding-bottom: 40px; padding-right: 150px;">
*le associazioni culturali possono essere scelte per devolvere il contributo del
2x1000 nella propria dichiarazione dei redditi.  Chi ha diritto a percepire la
donazione è inserito in un elenco aggiornato al novembre 2022 dal Consiglio dei
Ministri: Associazione Culturale Engramma è presente nell’elenco alla pagina
116, posizione n 2025. 
<!--
*le associazioni culturali possono essere scelte per devolvere <br>
il contributo del 2x1000 nella propria dichiarazione dei redditi. <br>
Chi ha diritto a percepire la donazione è inserito in un elenco <br>
aggiornato al novembre 2022 dal Consiglio dei Ministri: <br>
Associazione Culturale Engramma è presente nell’elenco alla <br>
pagina 116, posizione n 2025. 
-->
</div>

<div style=" 
position: absolute;
right: -100px;
bottom: -100px;
width: 300px;
height: 300px;
margin: 0 0 0 0;
padding: 0 0 0 0;
background-image: url('<?php print EOS_BASEURL.'core/frontend/modals/20210518_promo_2x1000/engramma_logo_white.gif'; ?>');
background-repeat: no-repeat;
background-position: 0 0;
background-size: cover;
opacity: 0.3;
">
</div>
<!--
<div style="display: inline-block">
	<p>Scopri engramma nella nuova versione e-book e cartacea</p>
	<a href="#" class="btn">Libreria</a>
</div>
-->

