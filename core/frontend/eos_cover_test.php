<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_book.php");


$issuenumber = "";
if(isset($_SESSION['issueshown'])) 
	$issuenumber = $_SESSION['issueshown'];

$issue = new Issue();
if($issue->RetrieveBy("numero", ElementType::AsInteger, $issuenumber) == false) {
	echo "error issue";
}

$articletype = new ArticleType();
if($articletype->RetrieveBy("nome", ElementType::AsString, "Copertina") == false) {
	echo "error articletype";
}

$articlestatus = new ArticleStatus();
if($articlestatus->RetrieveBy("label", ElementType::AsString, "ok") == false) {
	echo "error articlestatus";
}

$issueid         = $issue->Get("id");
$articletypeid   = $articletype->Get("id");
$articlestatusid = $articlestatus->Get("id");

$table = new Table('tb_articolo');
$table->SetCondition('id_numero', $issueid, ElementType::AsInteger);
$table->SetCondition('tipo', $articletypeid, ElementType::AsInteger);
$table->SetCondition('stato', $articlestatusid, ElementType::AsInteger);
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $coverid, 0);

$cover = new Article();
if($cover->Retrieve($coverid) == false) {
	echo "error cover: ".$coverid;
}

$pdfname = $issue->Get("number")."_larivistadiengramma.pdf";
$pdfpath = EOS_BOOKSTORE_BASEPATH.$pdfname;
$pdfurl  = EOS_BOOKSTORE_BASEURL.$pdfname;
$pdflink = "";
if(file_exists($pdfpath))
	$pdflink = "<ul><li class='vMenu'><a href=".$pdfurl."><span class='tMenu'>[Versione PDF]</span></a></li></ul>";


$book = new Book();
$book->RetrieveByIssue($issueid);

?>
<script type="text/javascript">
$(document).ready(function(){
	//per creare popup nella pagina cover
	$(".popup").colorbox({inline:true, width:"80%", height:"80%",open:true, opacity:0});
	});
</script>

<body class="cover">

<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>

<div id="corpo">
<?php

//if($issuenumber == 100 || $issuenumber == 101 || $issuenumber ==150) {
if($issuenumber == 100 || $issuenumber ==150 || $issuenumber == 200) {
	print $cover->Get("text");
} else {
?>
	<div id="imgNumero">
	<img alt="" src="<?php print EOS_BASEURL.'core/frontend/eos_colorgrid.php?num='.$issuenumber; ?>"  /></div>
	<div id="infoNumero">
	<h2><?php echo $issue->Get("number"); ?></h2>
	<h3><?php echo $issue->Get("special"); ?></h3>
	<h1><?php echo $issue->Get("description"); ?></h1>
	</div>


<div class="botton-cover-div">
<?php if(empty($book->Get("pdf_path")) == false) { ?>
	<div class="book_btn_cover">
	<a href="<?php print $book->Get("pdf_path"); ?>" target="_blank">cartaceo</a>
	</div>
<?php } ?>
<?php if(empty($book->Get("ebook_path")) == false) { ?>
	<div class="book_btn_cover">
	<a href="<?php print $book->Get("ebook_path"); ?>" target="_blank">ebook</a>
	</div>
<?php } ?>
</div>

	<div id="index">
<?php
	print $cover->Get("text");
	//print $pdflink;
?>
</div>
<?php
}
?>

<?php 

$ENABLE_PROMO = true;

if ($issuenumber == 200 ) {
	$ENABLE_PROMO = false;
}

//if(isset($_SESSION['promo']) == false && $ENABLE_PROMO == true) {
if($ENABLE_PROMO == true) {
	$_SESSION['promo'] = true;

?>
<!-- Modal -->
<div class="modal" id="modal-one" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-header">
			<a href="#modal-one" class="btn-close" aria-hidden="false">×</a>
        </div>
        <div class="modal-body">
			
			<?php include(EOS_BASEPATH . '/core/frontend/modals/20230308_promo_duecento/eos_promo_duecento.php');  ?>
        </div>
		<!--
		<div class="modal-footer">
		</div>
-->
    </div>
</div>

<?php
}
?>

</div>
