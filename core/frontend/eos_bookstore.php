<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_book.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$pdfrootpath = EOS_BOOKSTORE_BASEPATH;
$pdfrooturl  = EOS_BOOKSTORE_BASEURL;

$table = new Table('issue_status');
$table->SetCondition('label', 'Archiviato', ElementType::AsString);
$table->Select('id');
$table->Get('id', $issue_archived_id, 0);

$table = new Table('tb_numero');
//$table->SetCondition('stato', $issue_archived_id, ElementType::AsInteger);
$table->SetOrder('numero', QueryOrder::AsDescending);
$table->Select('id', 'numero');
$table->Get('id', $issues_id);
$table->Get('numero', $issues_number);

$issues = array();
for ($i=0; $i<count($issues_id); $i++) {
	$cissue['id']     = $issues_id[$i];
	$cissue['number'] = $issues_number[$i];

	$cbook = new Book();
	$cbook->RetrieveByIssue($cissue['id']);
	$cbook2 = new Book();
	$volumes = $cbook2->RetrieveVolumes($cissue['id']);
	
	for($u = 0; $u<count($volumes); $u++) {
		$ccissue = $cissue;
		$ccissue['pdf_path'] = $volumes[$u]->Get('pdf_path');
		$ccissue['ebook_path'] = $volumes[$u]->Get('ebook_path');
		$ccissue['thumbnail'] = $volumes[$u]->Get('thumbnail');

		$ckey = array_search($ccissue['pdf_path'], array_column($issues, 'pdf_path'));
		if($ckey === false) {
			array_push($issues, $ccissue);
		}
		unset($ccissue);
	}

	unset($cissue);
}

$table = new Table('editorial_bookshop_type');
$table->SetOrder('id', QueryOrder::AsAscending);
$table->Select('id', 'name');
$table->Get('id', $editorial_type_id);
$table->Get('name', $editorial_type_name);

$types = array();
for($i = 0; $i < count($editorial_type_id); $i++) {
	$ctype['id'] = $editorial_type_id[$i];
	$ctype['name'] = $editorial_type_name[$i];
	array_push($types, $ctype);
}



//var_dump($types[array_search('Edizioni Engramma', array_column($types, 'name'))]['id']); 

$table = new Table('editorial_bookshop');
$table->SetOrder('id', QueryOrder::AsDescending);
$table->Select('pdf_path', 'thumbnail', 'type_id');
$table->Get('pdf_path', $editorial_pdf_path);
$table->Get('thumbnail', $editorial_thumbnail);
$table->Get('type_id', $editorial_type_id);

$editorials_engramma = array();
$editorials_ronzani = array();
for($i=0; $i<count($editorial_pdf_path); $i++) {
	$ceditorial['pdf_path'] = $editorial_pdf_path[$i];
	$ceditorial['thumbnail'] = $editorial_thumbnail[$i];
	if($editorial_type_id[$i] == 1)
		array_push($editorials_engramma, $ceditorial);
	elseif($editorial_type_id[$i] == 2)
		array_push($editorials_ronzani, $ceditorial);
}


?>
<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
<body>

<section class="eos-bookshop">
	<div class="eos-bookshop-left">
		<div class="subtitle">Edizioni Engramma</div>
			<div class="book-grid">
			<?php 
				for($i=0; $i<count($editorials_engramma); $i++) {
					$cthumb = $editorials_engramma[$i]['thumbnail'];
					$cpdf   = $editorials_engramma[$i]['pdf_path'];
			?>
				<div class="book-item">
					<a href="<?php print $cpdf; ?>" target="_blank"><img src="<?php print $cthumb;  ?>" ></a>
				</div>
			<?php } ?>
				
			</div>
		<div class="subtitle">Ronzani EngrammaSaggi</div>
			<div class="book-grid">
			<?php 
				for($i=0; $i<count($editorials_ronzani); $i++) {
					$cthumb = $editorials_ronzani[$i]['thumbnail'];
					$cpdf   = $editorials_ronzani[$i]['pdf_path'];
			?>
				<div class="book-item">
					<a href="<?php print $cpdf; ?>" target="_blank"><img src="<?php print $cthumb;  ?>" ></a>
				</div>
			<?php } ?>
				
			</div>

	</div>
	<div class="eos-bookshop-main">
		<div class="subtitle">La Rivista di Engramma</div>

		<div class="book-grid">
			<?php
				for($i=0; $i<count($issues); $i++) {
					$cthumb = $issues[$i]['thumbnail'];
					$cpdf   = $issues[$i]['pdf_path'];
					$cebook = $issues[$i]['ebook_path']; 
					if( (empty($cthumb) == true) ) {
						continue;
					}

			?>	
				<div class="book-item">
					<a href="<?php print EOS_BASEURL . "index.php?issue=".$issues[$i]['number']; ?>"><img src="<?php print $cthumb;  ?>"></a>
					
					<div class="button">
						<?php if(empty($cebook) == false) { ?>
							<div class="book_btn">
							<a href="<?php print $cebook; ?>" target = "_blank">ebook</a>
							</div>
						<?php } ?>
						<?php if(empty($cpdf) == false) { ?>
							<div class="book_btn">
							<a href="<?php print $cpdf; ?>" target="_blank">cartaceo</a>
							</div>
						<?php } ?>
					</div>
				</div>
			
			<?php
				}
			?>
		</div>
	<div class="footer">per informazioni sull’acquisto dell’intera collezione dei numeri di Engramma, scrivi a <a href="mailto:edizioni@engramma.it">edizioni@engramma.it</a></div>
	</div>


</section>
  		
		
