<head>
<title>engramma - la tradizione classica nella memoria occidentale 
<?php
if(isset($_SESSION['issuecurrent'])){
	$issuecurrent=$_SESSION['issuecurrent'];
	print("n.".$issuecurrent);
} 

// What is used for?
$adde = ''; 
if(isset($_GET['id_articolo'])) {
	$adde='?id_articolo='.$_GET['id_articolo'];
} elseif (isset($_GET['archivio'])||isset($_GET['cerca'])||isset($_GET['login'])||isset($_GET['libreria'])) {
	$adde='?altro';
}
/////////////////////////////////////
?>
</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles.php'.$adde; ?>" media="screen"></link>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles_header.php'; ?>" media="screen"></link>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles_footer.php'; ?>" media="screen"></link>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles_bookshop.css.php'; ?>" media="screen"></link>

<!--[if lt ie 7]>
<style type="text/css">
@import "stilnovo/iestilnovo.css";
</style>
<![endif]--> 
<!--[if IE]>  
<script>  
 document.createElement("header");  
 document.createElement("barter");  
 document.createElement("nav");  
 document.createElement("article");  
 document.createElement("section");  
</script>  
<![endif]-->

<link rel="apple-touch-icon" sizes="180x180" href="<?php print EOS_BASEURL.'core/img/apple-touch-icon.png'; ?>">
<link rel="icon" type="image/png" href="<?php print EOS_BASEURL.'core/img/favicon-32x32.png'; ?>" sizes="32x32">
<link rel="icon" type="image/png" href="<?php print EOS_BASEURL.'core/img/favicon-16x16.png'; ?>" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="<?php print EOS_BASEURL.'core/img/safari-pinned-tab.svg'; ?>" color="#5bbad5">
<meta name="theme-color" content="#ffffff">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<script src="<?php print EOS_BASEURL.'core/functions/eos_javascript_support.js'; ?>" type="text/javascript"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_dcmegamenu.js'; ?>" type="text/javascript"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_head.js'; ?>" type="text/javascript"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_colorbox.js'; ?>"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_zoom.js'; ?>"></script>

<script src="<?php print EOS_PLUGINS_BASEURL; ?>jscolor/jscolor.js"></script>
<script src="<?php print EOS_CKEDITOR_BASEURL; ?>ckeditor.js"></script>
<script src="<?php print EOS_CKFINDER_BASEURL; ?>ckfinder.js"></script> 

<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_BASEURL.'core/styles/eos_styles_colorbox.php'; ?>" media="screen"></link>
<script>
	jQuery(document).ready(
		function () {
                	jQuery('a.popup').colorbox({ width:200});
		}
	);
</script>
<script src="<?php print EOS_ANALYTICS_BASEURL.'eos_analytics_tracking.js' ?>" type="text/javascript"></script>
<?php		
if(isset($_GET['lang'])) {
	//per l'inglese
	$lang=$_GET['lang'];
?>
<script type="text/javascript" >
$(document).ready(function() {
	var lang = '<?php print $lang; ?>';
	showDiv(lang);
}
</script>
<?php
}
?>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles_minicolors.php'; ?>" media="screen"></link>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_minicolors.js'; ?>" type="text/javascript"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_javascript_support.js'; ?>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php print EOS_CSS_BASEURL.'eos_styles_backend.php'; ?>">

<link rel="stylesheet" type="text/css" href="<?php print EOS_CSS_BASEURL.'eos_styles_modal.php'; ?>">

<?php include("eos_meta_opengraph.php"); ?>
<?php include("eos_meta_citation.php"); ?>


</head>

