<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$articletype = new ArticleType();
if($articletype->RetrieveBy("nome", ElementType::AsString, "Istituzionale") == false)
	eos_redirect(EOS_BASEURL.'index.php?error=1');

$articlestatus = new ArticleStatus();
if($articlestatus->RetrieveBy("label", ElementType::AsString, "ok") == false)
	eos_redirect(EOS_BASEURL.'index.php?error=1');

$articletypeid   = $articletype->Get("id");
$articlestatusid = $articlestatus->Get("id");

$table = new Table('tb_categoria');
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listsection);
?>

<footer class="eos_footer">
<?php
foreach($listsection as $csectionid) {
	$articlesection = new ArticleSection();
	
	if($articlesection->Retrieve($csectionid) == false) {
		continue;
	}
	//	eos_redirect(EOS_BASEURL.'index.php?error=1');
	
	if(strcmp($articlesection->Get("label"), "temi di ricerca") == 0)
		continue;

	$articlesectionid = $articlesection->Get("id");
?>

<div class="eos_footer_element">
<h1><?php print $articlesection->Get("label"); ?></h1>
<div>
	<?php
		$atable = new Table('tb_articolo');
		$atable->SetCondition('tipo', $articletypeid, ElementType::AsInteger);
		$atable->SetCondition('stato', $articlestatusid, ElementType::AsInteger);
		$atable->SetCondition('categoria', $articlesectionid, ElementType::AsInteger);
		$atable->SetOrder('pos');
		$atable->Select('id');
		$atable->Get('id', $listarticle);
	        foreach($listarticle as $carticleid) {
			$article = new Article();
	
			if($article->Retrieve($carticleid) == false)
				eos_redirect(EOS_BASEURL.'index.php?error=1');
	
	?>  
	<a href="<?php print EOS_BASEURL.'index.php?id_articolo='.$article->Get("id"); ?>" ><?php print $article->Get("title"); ?></a>
			<?php
			}
			?>
</div>
</div>
<?php 
}

?>

<div class="eos_footer_element">
<h1>libreria</h1>
<div><a target="_blank"  title="archivio pdf" href="index.php?libreria">libreria</a></div>
</div>
<div class="eos_footer_archive">
<h1>archivio</h1>

<div>
<a target="_blank"  title="archivio pdf" href="index.php?libreria">pdf</a>
<?php
$issuestatus = new IssueStatus();
if($issuestatus->RetrieveBy("label", ElementType::AsString, "Archiviato") == false)
	eos_redirect(EOS_BASEURL.'index.php?error=1');
$issuestatusid = $issuestatus->Get("id");

$table->SetTable('tb_numero');
$table->SetCondition('stato', $issuestatusid, ElementType::AsInteger);
$table->SetOrder('numero');
$table->Select('id');
$table->Get('id', $listissue);

foreach($listissue as $cissueid) {
	$issue = new Issue();
	if($issue->Retrieve($cissueid) == false)
		eos_redirect(EOS_BASEURL.'index.php?error=1');
	
	$special = $issue->Get("special");
	$number  = $issue->Get("number");
	$href    = EOS_BASEURL.'core/frontend/eos_goto.php?issue='.$number;
	$title   = $number.'|'.$special;
?> 
<a target="_blank"  href="<?php print $href ?>"  title="<?php print $title; ?>">
<?php print $number; ?>
</a>  
<?php
}
?>
</div>
</div>
</footer>

