<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) {
	$user->Retrieve($_SESSION['userid'] );
}

$table = new Table('tb_articolo');
$hstatus  = new ArticleStatus();
$hsection = new ArticleSection();

$hstatus->RetrieveBy("label", ElementType::AsString, "ok");
?>

<header>
<span class="eos_header_issn">&quot;La Rivista di Engramma (open access)&quot;  ISSN 1826-901X</span>
<?php  
if($user->IsAuthorized()) {
?>
	<span id='tipo_utente'>
	<a href="<?php print EOS_BASEURL.'core/backend/eos_backend_index.php'; ?>"><?php print $user->Get("type::label"); ?></a><br>
	<a href="<?php print EOS_BASEURL.'core/frontend/eos_logout.php'; ?>">Logout</a>
	</span>
<?php 
} 
?>

<?php

$table = new Table('tb_categoria');
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listsection);

$table->SetTable('article_status'); 
$table->SetCondition('label', 'ok', ElementType::AsString);
$table->Select('id');
$table->Get('id', $statusid);


?>
<a href="<?php print EOS_BASEURL.'core/frontend/eos_goto.php?issue=current'; ?>" id="engrammaLogo">Engramma</a>
<div id="tabs_container">
<ul id="tabs">

<li><a class="mainMenu" href="<?php print EOS_BASEURL.'core/frontend/eos_atlas_index.php'; ?>" target="blank">warburg &amp; mnemosyne</a></li>
<?php /* <li><a class="mainMenu" href="#tab1">temi di ricerca</a></li> */ ?>
<li><a class="mainMenu" href="#tab2">indici</a></li>
<li><a class="mainMenu" href="<?php print EOS_BASEURL.'index.php?archivio'; ?>">archivio</a></li>
<?php /* <li><a class="mainMenu" href="#tab3">archivio</a></li> */ ?>
<li><a class="mainMenu eos-bookshop-navigation" href="<?php print EOS_BASEURL.'index.php?libreria'; ?>" target="blank">libreria</a></li>
<!--<li><a class="mainMenu eos-bookshop-navigation" href="<?php print EOS_BASEURL.'index.php?id_articolo=3792'; ?>" target="blank">libreria</a></li> -->
<li><a class="mainMenu" href="<?php print EOS_BASEURL.'index.php?id_articolo=1270'; ?>">in fieri</a></li>
<li><a class="mainMenu" href="#tab4">colophon</a></li>
<li><a class="mainMenuc" href="#tab5">&nbsp;</a></li>

<?php /*
<div id="tabs_content_container">
<!-- -------------------- -->
<!-- TAB: Temi di ricerca -->
<!-- -------------------- -->
<div id="tab1" class="tab_content" style="display: none; "><div>
<li>
<!-- <a class="mTabs3" href="<?php print EOS_BASEURL.'core/frontend/eos_atlas_index.php'; ?>" target="blank">mnemosyne atlas</a> -->
<?php

$hsection->RetrieveBy("nome", ElementType::AsString, "temi di ricerca");
$table->SetTable('tb_articolo');
$table->SetCondition('stato', $hstatus->Get('id'), ElementType::AsInteger);
$table->SetCondition('categoria', $hsection->Get('id'), ElementType::AsInteger);
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listarticle);

foreach($listarticle as $hcarticleid) {
	$harticle = new Article();
	$harticle->Retrieve($hcarticleid);
?>	
<a href="<?php print EOS_BASEURL.'index.php?id_articolo='.$harticle->Get('id'); ?>" class="mTabs" ><?php print $harticle->Get('title'); ?></a>
<?php
}
?>
	</div>
</div>
 */ ?>
<!-- -------------------- -->
<!-- TAB: Indici          -->
<!-- -------------------- -->
<div id="tab2" class="tab_content" style="display: none; "><div>    
<?php

$hsection->RetrieveBy("nome", ElementType::AsString, "indici");
$table->SetTable('tb_articolo');
$table->SetCondition('stato', $hstatus->Get('id'), ElementType::AsInteger);
$table->SetCondition('categoria', $hsection->Get('id'), ElementType::AsInteger);
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $hlistarticle);

$count = 0;
foreach($hlistarticle as $hcarticleid) {
	$harticle = new Article();
	$harticle->Retrieve($hcarticleid);
	$htitle = $harticle->Get('title');

?>
<a href="<?php print EOS_BASEURL.'index.php?id_articolo='.$harticle->Get('id'); ?>" class="mTabs" ><?php print $htitle; ?></a>
<?php

	if($count == 3) {
?>

<a href="<?php print EOS_BASEURL.'core/frontend/eos_atlas_index.php?id_articolo=1614#monographicissues'; ?>" class="mTabs" >Indice Warburg</a>
<?php
	}
	$count++;
}
?>
</div></div>

<!-- -------------------- -->
<!-- TAB: Archivio        -->
<!-- -------------------- -->
<div id="tab3" class="tab_content" style="display: none; "><div>


<?php 
/*
$hissuestatus = new IssueStatus();
$hissuestatus->RetrieveBy("label", ElementType::AsString, "Archiviato");

$table->SetTable('tb_numero');
$table->SetCondition('stato', $hissuestatus->Get('id'), ElementType::AsInteger);
$table->SetOrder('numero');
$table->Select('id');
$table->Get('id', $hlistissue);

foreach($hlistissue as $hcissueid) {
	$hissue = new Issue();
	$hissue->Retrieve($hcissueid);
	$hhref  = EOS_BASEURL.'core/frontend/eos_goto.php?issue='.$hissue->Get('number');
	$htitle = $hissue->Get('number')."|".$hissue->Get('special'); 
?> 
<a target="_blank"  href="<?php print $hhref ?>" class="mTabs" title="<?php print $htitle; ?>" >
<?php print $hissue->Get('number'); ?>
</a>  
<?php

 
}*/
?>

</div></div>

<!-- -------------------- -->
<!-- TAB: Colophon        -->
<!-- -------------------- -->
<div id="tab4" class="tab_content" style="display: none; "><div>    
<?php

$hsection->RetrieveBy("nome", ElementType::AsString, "colophon");
$table->SetTable('tb_articolo');
$table->SetCondition('stato', $hstatus->Get('id'), ElementType::AsInteger);
$table->SetCondition('categoria', $hsection->Get('id'), ElementType::AsInteger);
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listarticle);

foreach($listarticle as $hcarticleid) {
	$harticle = new Article();
	$harticle->Retrieve($hcarticleid);
?>
<a href="<?php print EOS_BASEURL.'index.php?id_articolo='.$harticle->Get('id'); ?>" class="mTabs" ><?php print $harticle->Get('title'); ?></a>
<?php
}
?>
<a href="https://www.classica-iuav.it/" class="mTabs" target="_blank">Centro studi classicA</a>
<a href="http://www.engramma.org/" class="mTabs" target="_blank">Associazione culturale Engramma</a>
</div></div>

<!-- -------------------- -->
<!-- TAB: Google          -->
<!-- -------------------- -->
<div id="tab5" class="tab_content" style="display: none; "><div>
<div id="cse-search-form">loading</div>
<!--
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<link rel="stylesheet" href="<?php print EOS_BASEURL.'core/styles/eos_styles_google.php'; ?>" type="text/css" />
<script type="text/javascript"> 
	(function() {
    //var cx = '008966862368361509778:arg8o6ltdfk';
	var cx = '017158788331768607198:ylpecntn_g4';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:searchbox-only></gcse:searchbox-only>
-->
<link rel="stylesheet" href="<?php print EOS_BASEURL.'core/styles/eos_styles_google.php'; ?>" type="text/css" />
<!--
<script async src="https://cse.google.com/cse.js?cx=017158788331768607198:ylpecntn_g4"></script>
<div class="gcse-searchbox-only"></div>
-->
<script async src="https://cse.google.com/cse.js?cx=017158788331768607198:ylpecntn_g4"></script>
<div class="gcse-searchbox-only" 
	data-resultsUrl="<?php print EOS_BASEURL . 'index.php?cerca'; ?>"></div>
</div></div>
</div>
</header>
