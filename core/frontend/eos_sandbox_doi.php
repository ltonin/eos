<?php 

//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once("../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$table = new Table("article_doi");
$table->SetOrder('article_id');
$table->Select('article_id', 'title', 'creator', 'year');
$result = $table->GetResult();

$list = array();
while($row = $result->fetch_array()) {
	$entry = array();
	$entry['article_id'] = $row['article_id'];
	$entry['title'] 	 = $row['title'];
	$entry['creator'] 	 = $row['creator'];
	$entry['year'] 		 = $row['year'];

	$list[] = $entry;
}

foreach($list as $row) {


	$csv = $row['article_id'] . ";" . "\"" . htmlspecialchars($row['title']) . "\"" . ";" . $row['year'] . ";" . "\"" . htmlspecialchars($row['creator']) . "\"" . "<br>";

	print($csv);
}



?>
