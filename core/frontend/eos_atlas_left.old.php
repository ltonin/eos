<div class="clefthome">
<div id="divlogo">
<a href="<?php print EOS_BASEURL; ?>" target="_blank">
<img id="logo" src="<?php print EOS_BASEURL; ?>core/img/Logob.png">
</a>
</div>
<h4><a class="eng" href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?lang=eng">Aby Warburg<br />Mnemosyne Atlas</a>
<a class="ita"  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php">Aby Warburg<br />Mnemosyne Atlas</a></h4>
<div class="ita">
<ul class="categorie">
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=1177" >Warburg e Mnemosyne</a></li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=3464" >Genesi dell'Atlante</a></li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=865" >Letture e Percorsi</a>
  <ul class="sotto">
    <li><a class="alfa"  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=866">&alpha;. coordinate della memoria</a></li>
    <li><a class="primo" href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=868">I. astrologia e mitologia</a></li>
    <li><a class="ii"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=869">II. modelli archeologici</a></li>
    <li><a class="iii"   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=870">III. migrazioni degli antichi dei</a></li>
    <li><a class="iv"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=871">IV. veicoli della tradizione</a></li>
    <li><a class="v"     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=872">V. irruzione dell’antico</a></li>
    <li><a class="vi"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=873">VI. formule di pathos dionisiaco</a></li>
    <li><a class="vii"   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=874">VII. Nike e Fortuna</a></li>
    <li><a class="viii"  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=875">VIII. dalle Muse a Manet</a></li>
    <li><a class="ix"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=876">IX. D&uuml;rer: divinit&agrave; al nord</a></li>
    <li><a class="x"     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=877">X. l’era di Nettuno</a></li>
    <li><a class="xi"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=878">XI. arte ufficiale e barocco</a></li>
    <li><a class="xii"   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=879">XII. riemersioni dell'antico</a></li>
    <li><a class="omega" href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=880">&omega;. attualit&agrave; della memoria</a></li>
  </ul>
</li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=1614" >Bibliografia</a></li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=3465" >Contributi in engramma</a></li>
</ul>
</div>

<div class="eng">
<ul class="categorie">
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=1177&lang=eng" >Warburg and Mnemosyne</a></li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=3464&lang=eng" >Atlas Genesis</a></li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=865&lang=eng" >Readings and Pathways</a>
  <ul class="sotto">
    <li><a class="alfa"  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=866&lang=eng">&alpha;. coordinates of memory</a></li>
    <li><a class="primo" href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=868&lang=eng">I. astrology and mythology</a></li>
    <li><a class="ii"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=869&lang=eng">II. archaeological models</a></li>
    <li><a class="iii"   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=870&lang=eng">III. migrations of the ancient gods</a></li>
    <li><a class="iv"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=871&lang=eng">IV. vehicles of tradition</a></li>
    <li><a class="v"     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=872&lang=eng">V. irruption of antiquity</a></li>
    <li><a class="vi"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=873&lang=eng">VI. Dionysiac formulae of emotions</a></li>
    <li><a class="vii"   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=874&lang=eng">VII. Nike and Fortuna</a></li>
    <li><a class="viii"  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=875&lang=eng">VIII. from the Muses to Manet</a></li>
    <li><a class="ix"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=876&lang=eng">IX. D&uuml;rer: the gods go North</a></li>
    <li><a class="x"     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=877&lang=eng">X. the age of Neptune</a></li>
    <li><a class="xi"    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=878&lang=eng">XI. 'art officiel' and the baroque</a></li>
    <li><a class="xii"   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=879&lang=eng">XII. re-emergence of antiquity</a></li>
    <li><a class="omega" href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=880&lang=eng">&omega;. the classical tradition today</a></li>
  </ul>
</li>
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=1614" >Bibliography</a></li>	
<li><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_articolo=3465&lang=eng" >Contributions in engramma</a></li>	
</ul>
</div>

<h5 class="lingua"><a class="ita" href="#">english</a><a class="eng" href="#">italiano</a></h5>
<div style="vertical-align:bottom; padding-bottom:20px">
<img alt="arancio" src="<?php print EOS_BASEURL; ?>core/img/tumb_arancio.png" style="vertical-align:baseline">
<p class="sotto" style="float:right; padding-top:100px">
<span class="ita" style="">con approfondimenti</span>
<span class="eng">content analysis</span>
</p>
</div>
<div style="vertical-align:bottom">
<img alt="gialla" src="<?php print EOS_BASEURL; ?>core/img/tumb_gialla.png" style="vertical-align:baseline">
<p class="sotto" style="float:right; ">
<span class="ita" style="">con approfondimenti e dettagli</span>
<span class="eng">content analysis and details</span>
</p>
</div>
</div>
