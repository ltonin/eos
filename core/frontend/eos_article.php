<?php 
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_book.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_doi.php");

if(isset($_GET['id_articolo']))
	$articleid=$_GET['id_articolo'];

$article = new Article();
$article->Retrieve($articleid);

if($article->Get('id') == NULL)
	eos_redirect(EOS_BASEURL.'index.php?notfound');

if($article->Get("status::label") == "cestino")
	eos_redirect(EOS_BASEURL.'index.php?notfound');


/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );


if($article->Get("status::label") == "materiali") {
	if($user->IsAuthorized() == false) {
		eos_redirect(EOS_BASEURL);
	}
}





$issueid = $article->Get("issue::id");
//$issuenumber = "";
//if(isset($_SESSION['issueshown'])) 
//	$issuenumber = $_SESSION['issueshown'];

$issue = new Issue();
if($issue->Retrieve($issueid) == false)
	$issue = null;
//	eos_redirect(EOS_BASEURL.'index.php?error=1');
//if($issue->RetrieveBy("numero", ElementType::AsInteger, $issuenumber) == false)
//	eos_redirect(EOS_BASEURL.'index.php?error=1');

$book = new Book();
$book->RetrieveByIssue($issueid);

$doi = new DOI();
$doi->RetrieveByArticle($articleid);


?>
<script type="text/javascript">
$(document).ready(function(){
	//Examples of how to assign the ColorBox event to elements
	$(".popbox").colorbox( {
		rel:'nofollow',
		transition:'none',
		innerWidth:'45%', 
		maxWidth:'50%',
		right:'2%',
		maxHeight:'90%',
		opacity:'0'
	});
	$(".slideshow").colorbox({
		rel:'nofollow',
		transition:'none',
		innerWidth:'45%',
		maxHeight:'90%',
		opacity:'0'
	});
	$(".box").colorbox({
		rel:'nofollow',
		transition:'none',
		innerWidth:'85%'
	});
	$(".bbox").colorbox({
		rel:'nofollow',
		transition:'none',
		maxWidth:'85%',
		maxHeight:'90%'
	});
	$(".sbox").colorbox({'onComplete': function(){ 
        $('#cboxLoadedContent').zoom({ on:'click' });
    }
	
	});
});
jQuery.colorbox.settings.maxWidth  = '95%';
	jQuery.colorbox.settings.maxHeight = '95%';

	// ColorBox resize function
	var resizeTimer;
	function resizeColorBox()
	{
		if (resizeTimer) clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
				if (jQuery('#cboxOverlay').is(':visible')) {
						jQuery.colorbox.load(true);
				}
		}, 300);
	}

	// Resize ColorBox when resizing window or changing mobile device orientation
	jQuery(window).resize(resizeColorBox);
	window.addEventListener("orientationchange", resizeColorBox, false);
	
</script>		
<body  class="article">
<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
<div id="corpo">

<?php
// Do not show issue number in ops page
if($article->Get("id") != 3242) {
?>

<div class="top-article-div">
	<?php if(empty($book->Get("pdf_path")) == false) { ?>
	<div class="top-article-left">
		<div class="book_btn">
		<a href="<?php print $book->Get("pdf_path"); ?>" target="_blank">cartaceo</a>
		</div>
	</div>
	<?php } ?>
	<?php if(empty($book->Get("ebook_path")) == false) { ?>
	<div class="top-article-left">
		<div class="book_btn">
		<a href="<?php print $book->Get("ebook_path"); ?>" target="_blank">ebook</a>
		</div>
	</div>
	<?php } ?>

	<?php if(empty($issue) == false) { ?>	
	<div class="top-article-right">
		<div class="info-numero">
  		<h3>
  		<a href="<?php print EOS_BASEURL.'core/frontend/eos_goto.php?issue='.$issue->Get("number"); ?>"><?php print $issue->Get("number"); ?> | <?php print $issue->Get("special"); ?></a>
  		</h3>
  		<h4><?php print $issue->GetIsbn(); ?></h4>
		</div>
	</div>
	<?php } ?>
</div>

<?php
}
?>
<div id="article">
  <?php
 print $article->Get("text"); ?>
 </div>

<?php 
if ( empty($doi->Get('id')) == false ) {
?>
<div class="bottom-article-doi">
<?php print "doi: ".$doi->Get('doi'); ?>
</div>
<?php
}
?>

<div class="bottom-article-div">
<?php if(empty($book->Get("pdf_path")) == false) { ?>
<div class="top-article-right">
	<div class="book_btn">
	<a href="<?php print $book->Get("pdf_path"); ?>" target="_blank">cartaceo</a>
	</div>
</div>
<?php } ?>
<?php if(empty($book->Get("ebook_path")) == false) { ?>
<div class="top-article-right">
	<div class="book_btn">
	<a href="<?php print $book->Get("ebook_path"); ?>" target="_blank">ebook</a>
	</div>
</div>
</div>
<?php } ?>

</div>

