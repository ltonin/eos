<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$tavolanumber=$_GET['id_tavola'];

$tavola = new Issue();
if($tavola->RetrieveBy("numero", ElementType::AsInteger, $tavolanumber) == false)
	eos_redirect(EOS_BASEURL.'index.php?error=1');

// Retrieve id of trash status
$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

$table->SetTable('tb_articolo');
$table->SetCondition('id_numero', $tavola->Get('id'), ElementType::AsInteger);
$table->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->Select('id');
$table->Get('id', $listarticles);

foreach($listarticles as $carticleid) {
	$article = new Article();    
	$article->Retrieve($carticleid);

	// @Elisa: Boh!
	if( preg_match('/cover/', $article->Get('title')) ) {
		$testo  = $article->Get('text');
	} elseif( preg_match('/appunti/', $article->Get('title')) ) {
		$testo1 = $article->Get('text');
	} elseif(preg_match('/hr/', $article->Get('title')) ) {
		$testo2 = $article->Get('text');
	} elseif(preg_match('/zoom/', $article->Get('title')) ) {
		$testo3 = $article->Get('text');
	} elseif(preg_match('/didaok/', $article->Get('title')) ) {	
		$testo4 = $article->Get('text');
	}

	$testob="<div class='ita'><p>in preparazione</p></div><div class='eng'><p>in fieri</p></div>";
}

?>

<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_atlas_tabs.js'; ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".box").colorbox({rel:'nofollow',transition:'none',maxWidth:'95%',
		maxHeight:'195%'});
});
</script>

<div id="article">
<?php print $testo; ?>
 <a name="m"></a>
 <div class="ita">
 <ul id="menuTavola">
<li ><a class="tab1" href="#cont1">Appunti di Warburg e collaboratori</a></li>
<li><a class="tab2" href="#cont2">Tavola</a></li>
<li><a class="tab3" href="#cont3">Didascalie</a></li>
<li><a class="tab4" href="#cont4">Dettagli</a></li>
</ul>
</div>
 <div class="eng">
 <ul id="menuTavola">
<li><a class="tab1" href="#cont1">Warburg and coll. notes</a></li>
<li><a class="tab2" href="#cont2">Panel</a></li>
<li><a class="tab3" href="#cont3">Captions</a></li>
<li><a class="tab4" href="#cont4">Details</a></li>
</ul>
</div>
<div id="cont1">
<?php
if(isset ($testo1) )
 	print $testo1;
else
	print $testob; 
?>
</div>
<div id="cont2">
<?php
if(isset ($testo2) )
	print $testo2;
else 
	print $testob;  
?>
</div>
<div id="cont3">
<?php 
if(isset ($testo4) )
 	print $testo4;
else
	print $testob; 
?>
</div>
<div id="cont4">
<?php
if(isset ($testo3) )
	print $testo3;
else
	print $testob;
?>
</div>
</div>
