<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
$csspath=EOS_CSS_BASEURL."eos_styles_atlas_cover.php";

if(isset($_GET['id_articolo'])) {
	$csspath=EOS_CSS_BASEURL."eos_styles_atlas_article.php";
} elseif(isset($_GET['id_tavola'])) {
	$csspath=EOS_CSS_BASEURL."eos_styles_atlas_table.php";
}
?>
<html>
<head>
<title>engramma - atlante di Aby Warburg </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL; ?>eos_styles_atlas.php" media="screen"></link>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php  print $csspath; ?>" media="screen"></link>
	
<!--[if lt ie 7]>
<style type="text/css">
@import "stilnovo/iestilnovo.css";
</style>
<![endif]--> 
<!--[if IE]>  
<script>  
 document.createElement("header");  
 document.createElement("footer");  
 document.createElement("nav");  
 document.createElement("article");  
 document.createElement("section");  
</script>  
<![endif]-->
<link rel="icon" href="<?php print EOS_BASEURL.'core/img/favicon-16x16.png'; ?>" type="image/x-icon"></link>
<link rel="shortcut icon" href="<?php print EOS_BASEURL.'core/img/favicon-32x32.png'; ?>" type="image/x-icon"></link>
  
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_colorbox.js'; ?>"></script>

<script type="text/javascript">

$(document).ready(function(){
	
$("a.ita").click(function(){$(".ita").hide(); 
$(".eng").show();
});
$("a.eng").click(function(){$(".eng").hide(); 
$(".ita").show();
});

});
</script>
<script src="<?php print EOS_ANALYTICS_BASEURL.'eos_analytics_tracking.js' ?>" type="text/javascript"></script>

