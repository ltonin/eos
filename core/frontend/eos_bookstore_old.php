<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$pdfrootpath = EOS_BOOKSTORE_BASEPATH;
$pdfrooturl  = EOS_BOOKSTORE_BASEURL;

$table = new Table('issue_status');
$table->SetCondition('label', 'Archiviato', ElementType::AsString); 
$table->Select('id');
$table->Get('id', $archiveid, 0);

$table->SetTable('tb_numero');
$table->SetCondition('stato', $archiveid, ElementType::AsInteger);
$table->SetCondition('numero', 0, ElementType::AsInteger, QueryCondition::IsGreater);
$table->SetCondition('numero', 999, ElementType::AsInteger, QueryCondition::IsLesser);
$table->SetOrder('numero', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $listissue);

//preparo il contenitore vuoto della lista
$pdflist="";
//verifico quanti sono i numeri in archivio ed itero
foreach($listissue as $cissueid) {
	$issue = new Issue();
	$issue->Retrieve($cissueid);	

	//setto il nome del pdf e della cover
	$pdfname = $issue->Get("number")."_larivistadiengramma.pdf";
	$imgname = $issue->Get("number")."_larivistadiengramma.jpg";

	$pdffile = $pdfrootpath.$pdfname;
	$imgfile = $pdfrootpath.$imgname;

	//se ho caricato la cover metto quella, altrimenti una standard
	if(file_exists($imgfile) == false){
		$imgname = "00_larivistadiengramma.jpg";
	}
	$pdfurl = $pdfrooturl.$pdfname;
	$imgurl = $pdfrooturl.$imgname;

	$pdfhtml = "";
	//se esiste il pdf creo le icone ed i dati
	if(file_exists($pdffile)) {
		$pdflink = "<a href=".$pdfurl." target=_blank><img src=".$imgurl." alt='pdf engramma n.".$issue->Get('number')."' class='cover'> </a>";
		//verifico se ho il titolo compresso
		$datac=$issue->Get("datelabel");
		if($datac==NULL)
			$datac=$issue->Get('special');

		//verifico se ho la descrizione del numero
		$desc = $issue->Get("description");
		if($desc==NULL) 
			$desc="";

		//verifico se ho i nomi degli autori
		$autori=$issue->Get('authors');
		if($autori==NULL)
			$autori="";
		
		//verifico se ho info extra per il titolo
		$extra=$issue->Get('extra');
		if($extra==NULL)
			$extra="";

		//numero ISBN
		$isbnpad=str_pad($issue->Get('isbn'), 3, '0', STR_PAD_LEFT);
		$isbn="ISBN:978-88-98260-".substr($isbnpad,0,2)."-".substr($isbnpad,2,1);

		if($issue->Get('isbn')==NULL)
			$isbn="";
		
		$testo_numero="<div class='numeri'>N. ".$issue->Get('number')."<br />".$datac."</div>";
		$titolo="<div class='titolo_n'>".$desc."</div><div class='autori'>".$autori."</div><div class='extra'>".$extra."</div><div class='isbn'>".$isbn."</div>";
		
		//preparo il codice per l'icona del pdf
		$pdfhtml="<div class='lista'><div class='icona'>".$pdflink."</div><div class='label'>".$testo_numero.$titolo."</div></div>\n";
		
	} 	

	//aggiungo il codice all'elenco
	$pdflist=$pdflist.$pdfhtml;
}

?>
<body  class="library">

<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
	
<div id="library">
<h1 class="titolo"><span class="ita">Libreria di Engramma</span><span class="eng">Engramma's Library</span></h2>

<div id="rivista_lib">
<div class="title">
<h2 class="titolo"><span class="ita">Pdf "La Rivista di Engramma"</span><span class="eng">Engramma Journal's Pdf</span></h1>
<h6 class="is">ISSN 2281-9045</h6></div>
<?php 
print $pdflist; 
?>
</div>
<div class="collana_lib">
<div class="title">
<h2 class="titolo"><span class="ita">Quaderni Engramma</span><span class="eng">Engramma's Notebooks</span></h2>
<h6 class="is">ISSN 1826-901X</h6>
</div>
 	
<div class='lista'>
<div class='icona'>
<a href="<?php  print $pdfrooturl.'Qe02.pdf'; ?>" target="blank">
<img src="<?php print $pdfrooturl.'qe02_cover.jpg'; ?>" alt="Quaderni di Engranmma n.2" class="cover"/>
</a>
</div>

<div class='label'>
<div class='numeri'>Qe02</div>
<div class='titolo_n'>Esercizi di Iconologia</div>
<div class='autori'>Fabrizio Lollini<br/>Venezia, 2012<br/>ISBN <br/>978-88-98260-01-0</div></div></div>
<div class='lista'>
<div class='icona'>
<a  href="<?php $pdfrooturl.'Qe03.pdf'; ?>" target="blank">
<img src="<?php $pdfrooturl.'qe03_cover.jpg'; ?>" alt="Quaderni di Engranmma n.3" class="cover"/>
</a>
</div>

<div class='label'>
<div class='numeri'>Qe03</div>
<div class='titolo_n'>Pensiero in azione. Bertolt Brecht, Robert Wilson, Peter Sellars: tre protagonisti del teatro contemporaneo</div>
<div class='autori'>Daniela Sacco<br/>Venezia, 2012<br/>ISBN <br/>978-88-98260-02-7</div></div></div>
</div>

<div class="collana_lib">
<div class="title">
<h2 class="titolo"><a href="http://www.guaraldi.it/guaraldi/Engramma.aspx" target="blank"><span class="ita">Collana Guaraldi - Engramma</span>
<span class="eng">Guaraldi - Engramma Collection</span></a></h2>
<h6 class="is"></h6></div>

<div class='lista'>
<div class='icona'>
<a href="http://www.guaraldi.it/Scheda.aspx?id=891" target="blank">
<img src="<?php print $pdfrooturl.'ge01.jpg'; ?>" alt="Guaraldi-Engramma n.1" class="cover"/>
</a>
</div>

<div class='label'>
<div class='numeri'>E01</div>
<div class='titolo_n'>Indagine su Santo Spirito di Brunelleschi</div>
<div class='autori'>Leonardo Benevolo<br/>Marzo 2015<br/>ISBN <br/>978-88-8692-7009-3</div></div></div>	

<div class='lista'>
<div class='icona'>
<a href="http://www.guaraldi.it/Scheda.aspx?id=892" target="blank">
<img src="<?php print $pdfrooturl.'ge02.jpg'; ?>" alt="Guaraldi-Engramma n.2" class="cover"/>
</a>
</div>

<div class='label'>
<div class='numeri'>E02</div>
<div class='titolo_n'>Isabella d'Este. La Signora del Rinascimento</div>
<div class='autori'>Lorenzo Bonoldi<br/>Marzo 2015<br/>ISBN <br/>978-88-8692-7011-6</div></div></div>

<div class='lista'>
<div class='icona'>
<a href="http://www.guaraldi.it/Scheda.aspx?id=893" target="blank">
<img src="<?php print $pdfrooturl.'ge03.jpg'; ?>" alt="Guaraldi-Engramma n.3" class="cover"/>
</a>
</div>

<div class='label'><div class='numeri'>E03</div><div class='titolo_n'>Pellegrino Prisciani: Spectacula</div>
<div class='autori'>A cura di Elisa Bastianello<br/>Glossario a cura di Olivia Sara Carli<br/>Marzo 2015<br/>ISBN <br/>978-88-8692-7010-9</div></div></div>

<div class='lista'>
<div class='icona'>
<a href="http://www.guaraldi.it/Scheda.aspx?id=894" target="blank">
<img src="<?php print $pdfrooturl.'ge04.jpg'; ?>" alt="Guaraldi-Engramma n.4" class="cover"/>
</a>
</div>
<div class='label'>

<div class='numeri'>E04</div>
<div class='titolo_n'>Scene dal mito.Iconologia del dramma antico</div>
<div class='autori'>A cura di Giulia Bordignon<br/>Marzo 2015<br/>ISBN <br/>978-88-8692-7012-3</div></div></div>

</div>
</div>
<p>La produzione editoriale della Rivista, dei Quaderni di Engramma, e della collana Guaraldi/Engramma è curata da <a href="http://www.engramma.org" target="blank">Associazione culturale Engramma</a>.</p>


  		
		
