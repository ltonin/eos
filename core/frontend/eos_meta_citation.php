<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_citation.php");

$articleid = -1;

if(isset($_GET['id_articolo']))
	$articleid=$_GET['id_articolo'];

$citation = new Citation();
$isfound = $citation->RetrieveByArticle($articleid);
if($isfound == true) {
	echo $citation->meta_title();
	echo $citation->meta_authors();
	echo $citation->meta_journal_title();
	echo $citation->meta_volume();
	echo $citation->meta_issue();
	echo $citation->meta_year();
	echo $citation->meta_publication_date();
	echo $citation->meta_doi();
	echo $citation->meta_abstract();
}

?>
