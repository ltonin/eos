<script type="text/javascript">
	$(document).ready(function(){
	$("a.alfa").hover(function(){$('td.palfa').toggleClass('selected'); });
	$("a.primo").hover(function(){$('td.pprimo').toggleClass('selected');});
	$("a.ii").hover(function(){$('td.pii').toggleClass('selected');});
	$("a.iii").hover(function(){$('td.piii').toggleClass('selected');});
	$("a.iv").hover(function(){$('td.piv').toggleClass('selected');});
	$("a.v").hover(function(){$('td.pv').toggleClass('selected');});
	$("a.vi").hover(function(){$('td.pvi').toggleClass('selected');});
	$("a.vii").hover(function(){$('td.pvii').toggleClass('selected');});
	$("a.viii").hover(function(){$('td.pviii').toggleClass('selected');});
	$("a.ix").hover(function(){$('td.pix').toggleClass('selected');});
	$("a.x").hover(function(){$('td.px').toggleClass('selected');});
	$("a.xi").hover(function(){$('td.pxi').toggleClass('selected');});
	$("a.xii").hover(function(){$('td.pxii').toggleClass('selected');});
	$("a.omega").hover(function(){$('td.pomega').toggleClass('selected');});
	
	$("td.palfa").hover(function(){$('td.palfa').toggleClass('selected');$('a.alfa').toggleClass('selected'); });
	$("td.pprimo").hover(function(){$('td.pprimo').toggleClass('selected');$('a.primo').toggleClass('selected'); });
	$("td.pii").hover(function(){$('td.pii').toggleClass('selected');$('a.ii').toggleClass('selected'); });
	$("td.piii").hover(function(){$('td.piii').toggleClass('selected');$('a.iii').toggleClass('selected'); });
	$("td.piv").hover(function(){$('td.piv').toggleClass('selected');$('a.iv').toggleClass('selected'); });
	$("td.pv").hover(function(){$('td.pv').toggleClass('selected');$('a.v').toggleClass('selected'); });
	$("td.pvi").hover(function(){$('td.pvi').toggleClass('selected');$('a.vi').toggleClass('selected'); });
	$("td.pvii").hover(function(){$('td.pvii').toggleClass('selected');$('a.vii').toggleClass('selected'); });
	$("td.pviii").hover(function(){$('td.pviii').toggleClass('selected');$('a.viii').toggleClass('selected'); });
	$("td.pix").hover(function(){$('td.pix').toggleClass('selected');$('a.ix').toggleClass('selected'); });
	$("td.px").hover(function(){$('td.px').toggleClass('selected');$('a.x').toggleClass('selected'); });
	$("td.pxi").hover(function(){$('td.pxi').toggleClass('selected');$('a.xi').toggleClass('selected'); });
	$("td.pxii").hover(function(){$('td.pxii').toggleClass('selected');$('a.xii').toggleClass('selected'); });
	$("td.pomega").hover(function(){$('td.pomega').toggleClass('selected');$('a.omega').toggleClass('selected'); });
	});
</script>

<?php
$addlang = "";
if(isset($lang) && $lang === "eng")
	$addlang = "&lang=eng";
?>

<style type="text/css">
#intestazione{ display:inline; }
</style>

<div id="intestazione">
<a href="<?php print EOS_BASEURL.'core/frontend/eos_atlas_index.php'.$addlang; ?>">Aby Warburg - Mnemosyne Atlas</a>
</div>

<table class="atlante">
<tbody>
<tr>
<td class="palfa"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=10001<?php print $addlang; ?>" target="_blank"><img alt="A" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tavan.png"><br>A</a></td>
<td class="palfa"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=10002<?php print $addlang; ?>" target="_blank"><img alt="B" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tavbn.png"><br>B</a></td>
<td class="palfa"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=10003<?php print $addlang; ?>" target="_blank"><img alt="C" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tavcn.png"><br>C</a></td>
<td class="pprimo"><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1001<?php print $addlang; ?>" target="_blank"><img alt="1"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav1n.png"><br>1</a></td>
<td class="pprimo"><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1002<?php print $addlang; ?>" target="_blank"><img alt="2"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav2n.png"><br>2</a></td>
<td class="pprimo"><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1003<?php print $addlang; ?>" target="_blank"><img alt="3"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav3n.png"><br>3</a></td>
<td class="pii"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1004<?php print $addlang; ?>" target="_blank"><img alt="4"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav4n.png"><br>4</a></td>
<td class="pii"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1005<?php print $addlang; ?>" target="_blank"><img alt="5"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav5n.png"><br>5</a></td>
<td class="pii"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1006<?php print $addlang; ?>" target="_blank"><img alt="6"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav6n.png"><br>6</a></td>
</tr>
<tr>
<td class="pii"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1007<?php print $addlang; ?>" target="_blank"><img alt="7"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav7n.png"><br>7</a></td>
<td class="pii"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1008<?php print $addlang; ?>" target="_blank"><img alt="8"  src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav8n.png"><br>8</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1020<?php print $addlang; ?>" target="_blank"><img alt="20" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav20n.png"><br>20</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1021<?php print $addlang; ?>" target="_blank"><img alt="21" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav21n.png"><br>21</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1022<?php print $addlang; ?>" target="_blank"><img alt="22" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav22n.png"><br>22</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1023<?php print $addlang; ?>" target="_blank"><img alt="23" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav23n.png"><br>23</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=10231<?php print $addlang; ?>" target="_blank"><img alt="23a" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav23an.png"><br>23a</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1024<?php print $addlang; ?>" target="_blank"><img alt="24" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav24n.png"><br>24</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1025<?php print $addlang; ?>" target="_blank"><img alt="25" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav25n.png"><br>25</a></td>
</tr>
<tr>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1026<?php print $addlang; ?>" target="_blank"><img alt="26" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav26n.png"><br>26</a></td>
<td class="piii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1027<?php print $addlang; ?>" target="_blank"><img alt="27" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav27n.png"><br>27</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1028<?php print $addlang; ?>" target="_blank"><img alt="28-29" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav28-29n.png"><br>28-29</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1030<?php print $addlang; ?>" target="_blank"><img alt="30" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav30n.png"><br>30</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1031<?php print $addlang; ?>" target="_blank"><img alt="31" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav31n.png"><br>31</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1032<?php print $addlang; ?>" target="_blank"><img alt="32" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav32n.png"><br>32</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1033<?php print $addlang; ?>" target="_blank"><img alt="33" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav33n.png"><br>33</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1034<?php print $addlang; ?>" target="_blank"><img alt="34" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav34n.png"><br>34</a></td>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1035<?php print $addlang; ?>" target="_blank"><img alt="35" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav35n.png"><br>35</a></td>
</tr>
<tr>
<td class="piv"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1036<?php print $addlang; ?>" target="_blank"><img alt="36" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav36n.png"><br>36</a></td>
<td class="pv"><a     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1037<?php print $addlang; ?>" target="_blank"><img alt="37" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav37n.png"><br>37</a></td>
<td class="pv"><a     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1038<?php print $addlang; ?>" target="_blank"><img alt="38" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav38n.png"><br>38</a></td>
<td class="pv"><a     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1039<?php print $addlang; ?>" target="_blank"><img alt="39" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav39n.png"><br>39</a></td>
<td class="pvi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1040<?php print $addlang; ?>" target="_blank"><img alt="40" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav40n.png"><br>40</a></td>
<td class="pvi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1041<?php print $addlang; ?>" target="_blank"><img alt="41" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav41n.png"><br>41</a></td>
<td class="pvi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=10411<?php print $addlang; ?>" target="_blank"><img alt="41a" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav41an.png"><br>41a</a></td>
<td class="pvi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1042<?php print $addlang; ?>" target="_blank"><img alt="42" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav42n.png"><br>42</a></td>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1043<?php print $addlang; ?>" target="_blank"><img alt="43" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav43n.png"><br>43</a></td>
</tr>
<tr>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1044<?php print $addlang; ?>" target="_blank"><img alt="44" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav44n.png"><br>44</a></td>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1045<?php print $addlang; ?>" target="_blank"><img alt="45" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav45n.png"><br>45</a></td>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1046<?php print $addlang; ?>" target="_blank"><img alt="46" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav46n.png"><br>46</a></td>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1047<?php print $addlang; ?>" target="_blank"><img alt="47" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav47n.png"><br>47</a></td>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1048<?php print $addlang; ?>" target="_blank"><img alt="48" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav48n.png"><br>48</a></td>
<td class="pvii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1049<?php print $addlang; ?>" target="_blank"><img alt="49" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav49n.png"><br>49</a></td>
<td class="pviii"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1050<?php print $addlang; ?>" target="_blank"><img alt="50-51" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav50-51n.png"><br>50-51</a></td>
<td class="pviii"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1052<?php print $addlang; ?>" target="_blank"><img alt="52" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav52n.png"><br>52</a></td>
<td class="pviii"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1053<?php print $addlang; ?>" target="_blank"><img alt="53" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav53n.png"><br>53</a></td>
</tr>
<tr>
<td class="pviii"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1054<?php print $addlang; ?>" target="_blank"><img alt="54" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav54n.png"><br>54</a></td>
<td class="pviii"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1055<?php print $addlang; ?>" target="_blank"><img alt="55" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav55n.png"><br>55</a></td>
<td class="pviii"><a  href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1056<?php print $addlang; ?>" target="_blank"><img alt="56" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav56n.png"><br>56</a></td>
<td class="pix"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1057<?php print $addlang; ?>" target="_blank"><img alt="57" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav57n.png"><br>57</a></td>
<td class="pix"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1058<?php print $addlang; ?>" target="_blank"><img alt="58" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav58n.png"><br>58</a></td>
<td class="pix"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1059<?php print $addlang; ?>" target="_blank"><img alt="59" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav59n.png"><br>59</a></td>
<td class="px"><a     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1060<?php print $addlang; ?>" target="_blank"><img alt="60" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav60n.png"><br>60</a></td>
<td class="px"><a     href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1061<?php print $addlang; ?>" target="_blank"><img alt="61-62-63-64" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav61-62-63-64n.png"><br>61/64</a></td>
<td class="pxi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1070<?php print $addlang; ?>" target="_blank"><img alt="70" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav70n.png"><br>70</a></td>
</tr>
<tr>
<td class="pxi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1071<?php print $addlang; ?>" target="_blank"><img alt="71" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav71n.png"><br>71</a></td>
<td class="pxi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1072<?php print $addlang; ?>" target="_blank"><img alt="72" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav72n.png"><br>72</a></td>
<td class="pxi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1073<?php print $addlang; ?>" target="_blank"><img alt="73" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav73n.png"><br>73</a></td>
<td class="pxi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1074<?php print $addlang; ?>" target="_blank"><img alt="74" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav74n.png"><br>74</a></td>
<td class="pxi"><a    href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1075<?php print $addlang; ?>" target="_blank"><img alt="75" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav75n.png"><br>75</a></td>
<td class="pxii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1076<?php print $addlang; ?>" target="_blank"><img alt="76" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav76n.png"><br>76</a></td>
<td class="pxii"><a   href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1077<?php print $addlang; ?>" target="_blank"><img alt="77" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav77n.png"><br>77</a></td>
<td class="pomega"><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1078<?php print $addlang; ?>" target="_blank"><img alt="78" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav78n.png"><br>78</a></td>
<td class="pomega"><a href="<?php print EOS_BASEURL; ?>core/frontend/eos_atlas_index.php?id_tavola=1079<?php print $addlang; ?>" target="_blank"><img alt="79" src="<?php print EOS_IMAGES_BASEURL; ?>atlas/tumb_tav79n.png"><br>79</a></td>
</tr>
</tbody>
</table>
