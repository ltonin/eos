<?php
require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
eos_session_start();
if(isset($_GET['issue'])) {
	if($_GET['issue'] === 'current') {
		$_SESSION['issueshown']=$_SESSION['issuecurrent'];
	} else {
		$_SESSION['issueshown']=$_GET['issue'];
	}
}
header('location:'.EOS_BASEURL);
?>
