<?php 
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");

$articleid=$_GET['id_articolo'];


$article = new Article();
if($article->Retrieve($articleid) == false)
	eos_redirect(EOS_BASEURL.'index.php?error=1');
?>
<div id="article">
<?php print $article->Get("text"); ?>
</div>
