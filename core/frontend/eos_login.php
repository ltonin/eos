<?php
require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");


if(isset($_POST['nickname']) && isset($_POST['password'])) {
	
	$error = "Nome utente/password errati.<br>Login non effettuato";
	
	$postnickname = $_POST['nickname'];
	$postpassword = $_POST['password'];
	
	$user = new User();
	if($user->RetrieveBy('nickname', ElementType::AsString, $postnickname) == false) {
	}
	$nickname = $user->Get("nickname");

	$postpassword = md5($postpassword);
	
	if($user->Get("password") == $postpassword) {
		eos_session_set($user->Get("type"), $_SESSION['issueshown'], $user->Get("id"));
		$redirect = EOS_BASEURL.'core/backend/eos_backend_index.php';
		?>
		<script>
			window.location = '<?php print $redirect; ?>';
		</script>
		<?php 
		//header('location:'.$redirect);
	} 		
	
	$_SESSION['usertype'] = 0;
}
?>
<body>
<div id="container">
<div id="top">
<?php
/******** PAGE HEADER (old TOP) *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
</div>
<div id="main_box">
  <div id="testo">
<?php 
if (isset($error)) {
print "<h2 style='color:red;' >".$error."</h2>";
}
?>
  <form name="login" method="post" action="<?php print EOS_BASEURL.'index.php?login'; ?>">
  <table id="tabella_numero">
    <tr>
      <td width="47%"><div class="titolo_tabella">Nickname:</div></td>
      <td width="53%"><input type="text" name="nickname"></td>
    </tr>
    <tr>
      <td><div class="titolo_tabella">Password:</div></td>
      <td><input type="password" name="password"></td>
    </tr>
  </table>
  <p align="center">
    <input type="submit" name="Submit" value="Login">
  </p>
</form>

