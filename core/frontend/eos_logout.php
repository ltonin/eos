<?php
require_once(__DIR__."/../eos_configuration_init.php");

session_start();
session_destroy();

header('location:'.EOS_BASEURL);
?>
