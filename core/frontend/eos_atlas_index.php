<?php
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
eos_session_start();


include(EOS_BASEPATH."core/frontend/eos_atlas_headhtml.php");
if(isset($_GET['lang'])) {
	$lang=$_GET['lang'];
?>
	
<script type="text/javascript" >
$(document).ready(function() {
	var lang = '<?php print $lang; ?>';
	showDiv(lang);
});
function showDiv(lang) {
	if (lang='eng'){
		$('.ita').hide();
		$('.eng').show();}
	else {
		$('.eng').hide();
		$('.ita').show();}

}
</script>
<?php
}
?>

<body>
<div id="container">
<div id="top">
<?php include(EOS_BASEPATH."core/frontend/eos_atlas_header.php"); ?>
</div>
<?php include(EOS_BASEPATH."core/frontend/eos_atlas_left.php"); ?>
<div class="cright">
<?php 
if(isset($_GET['id_articolo'])) {
	include(EOS_BASEPATH."core/frontend/eos_atlas_article.php");
} elseif(isset($_GET['id_tavola'])) {
	include(EOS_BASEPATH."core/frontend/eos_atlas_table.php");
} else  {
	include(EOS_BASEPATH."core/frontend/eos_atlas_cover.php");
}

?>

<div id="bottom_main">
<div class="bHead">La Rivista di Engramma<br />ISSN 1826-901X<br />
Mnemosyne Atlas on line [2004] 2012-<br />
</div>
<div class="ita">
<ul>
<li class="c1"><a href="<?php print EOS_BASEURL; ?>index.php?id_articolo=1140">Mnemosyne Atlas website: &copy;engramma </a></li>
<li class="c2">testi: centro studi classicA-Iuav</li>
<li class="c2">webmaster: elisa bastianello</li>
</ul><ul>
<li class="c1">Atlas plates: &copy;The Warburg Institute Archive</li>
<li class="c2">versione inglese: elizabeth thomson</li>
<li class="c2">progetto grafico: daniele savasta</li>
</ul>
</div>
<div class="eng">
<ul>
<li class="c1"><a href="<?php print EOS_BASEURL; ?>index.php?id_articolo=1140">Mnemosyne Atlas website: &copy;engramma </a></li>
<li class="c2">texts: centro studi classicA-Iuav</li>
<li class="c2">webmaster: elisa bastianello</li>
</ul><ul>
<li class="c1">Atlas plates: &copy;The Warburg Institute Archive</li>
<li class="c2">english version: elizabeth thomson</li>
<li class="c2">graphic design: daniele savasta</li>
</ul>
</div>
</div>


</div> 

</body>
</html>
