<?php header("Content-type: text/css"); ?>

.eos-bookshop {
	width: 90%;
    font-family: "Museo500","newathenaunicoderegular","Shobhika",verdana, sans-serif;
}

.eos-bookshop .title {
	padding: 1%;
	font-size: 36px;
	width: 100%;
}

.eos-bookshop .subtitle {
	font-size: 24px;
}

.eos-bookshop-left {
	width: 30%;
	float: left;
}

.eos-bookshop-main {
	width: 70%;
	margin-left: 30%;
	padding-left: 5%;
}

.eos-bookshop-left .book-grid {
	display: flex;
	flex-wrap: wrap;
	align-content: space-between;
}

.eos-bookshop-left .book-item {
	padding-bottom: 10px;
	padding-top: 10px;
	padding-left: 5px;
	padding-right: 5px;
}

.eos-bookshop-left .book-item img {
	max-width: 180px;
}

.eos-bookshop-main .book-grid {
	display: flex;
	flex-wrap: wrap;
	align-content: space-between;
}

.eos-bookshop-main .book-item {
	padding-bottom: 10px;
	padding-top: 10px;
	padding-left: 5px;
	padding-right: 5px;
}

.eos-bookshop-main .button {
	width:  20%;
    font-family: "Museo500","newathenaunicoderegular","Shobhika",verdana, sans-serif;
	font-size: 12px;
  	display: inline;
	margin: 2px 2px 2px 2px;
	text-align: right;
	position: relative;
	right: 0px;
	padding-top: 5px;
	z-index: 100;
	vertical-align: top;
}

.eos-bookshop-main .book_btn a {
  display: inline-block;
  color: black;
  border-width: 2px;
  border-style: solid;
  border-color: black;
  background-color: Transparent;
  border-radius: 16px;
  padding: 5px 8px 5px 8px;
  font-size: 14px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px 8px 2px;
  float: right;
}
.eos-bookshop-main .book_btn a:visited{
  color: black;
}
.eos-bookshop-main .book_btn a:hover{
  color: white;
  background-color: black;
}

.eos-bookshop-main .footer {
	text-align: right;
	font-size: 14px;
	padding-top: 20px;
	font-weight: bold;
}

.eos-bookshop-main .footer a {
	text-decoration: underline;
	color: black;
}

.eos-bookshop-navigation {
	background-color: #4c3273 !important;
	color: white;
}

.eos-bookshop-navigation:hover {
	background-color: white !important;
}
