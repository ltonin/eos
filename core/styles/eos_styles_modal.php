<?php header("Content-type: text/css");
?>
.modal:target:before {
    display: none;
}
.modal:before {
    content:"";
    display: block;
    background: rgba(0, 0, 0, 0.6);
	
	/* middle window */
	position: fixed;
    top: -100%;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 3110;
	
	/* botton strip
    position:fixed;
    top:auto;
    right:auto;
    left:0;
    bottom:-100%;
	z-index: 10;
	*/
}
.modal .modal-dialog {
	background: #604486;
	#background: #bd8c84;
    border: #333333 solid 1px;
    border-radius: 5px;
	/*margin-left: -300px; */
	position: fixed;
    left: 50%;
    z-index: 3111;
    #width: 1000px;
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(-50%, 0);
    -webkit-transition: -webkit-transform 1.0s ease-out;
    -moz-transition: -moz-transform 1.0s ease-out;
    -o-transition: -o-transform 1.0s ease-out;
    transition: transform 1.0s ease-out;
    top: 15%;
	overflow: hidden;

	/* botto strip
	#background: rgba(95, 75, 139, 0.6);
	border: #333333 solid 1px;
    margin-left: 0px;
    z-index: 11;
	-webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.3s ease-out;
    -moz-transition: -moz-transform 0.3s ease-out;
    -o-transition: -o-transform 0.3s ease-out;
    transition: transform 0.3s ease-out;
    width: 100%;
   position:fixed;
   left:50%;
   bottom:0;
	transform: translateX(-50%);
   right:auto;
   top:auto;
	*/
}
.modal:target .modal-dialog {
	top: -100%;
    -webkit-transform: translate(0, -500%);
    -ms-transform: translate(0, -500%);
    transform: translate(0, -500%);
}
.modal-body {
	color: black;
	font-family: "Museo500", verdana;
	font-size: 30px;
    padding: 0px;
	text-align: center;
}
.modal-header, .modal-footer {
	padding: 0px;
	color:  white;
}

.modal-footer {
	text-align: center;
}

.btn-close {
    color: #000000;
    font-size: 30px;
    text-decoration: none;
    position: absolute;
    right: 5px;
    top: 0;
}
.btn-close:hover {
    color: #919191;
}

.btn {
    background: #4f3e74;
    border: white solid 1px;
    border-radius: 3px;
    color: #fff;
    display: inline-block;
    font-size: 18px;
	font-family: "Museo500", verdana;
    padding: 8px 15px;
    text-decoration: none;
    text-align: center;
    min-width: 60px;
    position: relative;
    transition: color .1s ease;
}
.btn:hover {
    background: #392d53;
}
