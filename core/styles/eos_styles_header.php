<?php 
header("Content-type: text/css");

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");

eos_session_start();
$IMGRELPATH = EOS_BASEURL.'core/img/';

$issue = new Issue();

if(isset($_GET['id_articolo'])) {
	$table = new Table('tb_articolo');
	$table->SetCondition('id', $_GET['id_articolo'], ElementType::AsInteger);
	$table->Select('id_numero');
	$table->Get('id_numero', $issueid, 0);
} elseif(isset($_SESSION['issueshown'])) {
	$table = new Table('tb_numero');
	$table->SetCondition('numero', $_SESSION['issueshown'], ElementType::AsInteger);
	$table->Select('id');
	$table->Get('id', $issueid, 0);
}

$issue->Retrieve($issueid);
$color	  = $issue->Get('color');
$altcolor = $issue->Get('altcolor');
$shade	  = $issue->Get('shade');

class IssueStyle {
	public $color;
	public $altcolor;
	public $shade;
	public $logo;

	public function init($color, $altcolor, $shade) {
		$this->color	= $color;
		$this->altcolor = $altcolor;
		$this->shade    = $shade;
		$this->logo     = $this->get_current_logo($shade);
	}

	public function get_current_logo($shade) {
		switch($shade) {
			case 4:
				$path = "engrammaMarchio-w.png";
				break;
			default:
				$path = "engrammaMarchio-b.png";
		}
		return $path;
	}
}

$IssueStyle = new IssueStyle();
$IssueStyle->init($color, $altcolor, $shade);
?>
#engrammaLogo { 
	text-indent:-9999px;
	background: url(<?php print $IMGRELPATH . $IssueStyle->logo; ?>) no-repeat center;
	width:100%;
	height:150px;
	display:block;
}

@media only screen and (max-width: 680px){
  	#engrammaLogo { 
	background: url(<?php print $IMGRELPATH . $IssueStyle->logo; ?>) no-repeat center;
	}
}

header {
	background-color:<?php print $IssueStyle->color;?>;
	color:<?php print $IssueStyle->altcolor;?>;
	width:100%;
	height:185px;
	padding:10px 0 5px 0;
	text-align:center;
	position: relative;
}

.eos_header_issn {
	font: normal 10px verdana, sans-serif;
	position: absolute;
	bottom: 30px;
	right: 2%;
}
