<?php header("Content-type: text/css");
require_once(__DIR__."/../eos_configuration_init.php");
?>

/******************************************************************************/
/* Backend general styles 						      */
/******************************************************************************/
#backend {
	font-family:Verdana,GillSans,"Gill Sans","Gill Sans MT",sans-serif;
	font-size:8px;
	color:#333;

	display:inline-block;
	width:77%;
	margin-top:0px;
	margin-left:auto;
	border-spacing: 1px;
}

#backend .title {

	color:#003366;
	font-size:16px;
	text-align:left;
	border-bottom:1px solid #003366;
	font-weight:bold;
	margin-top: 15px;
	margin-right: 5px;
	margin-bottom: 5px;
	margin-left: auto;
}

#backend .subtitle {
	font-weight:bold;
	font-size:12px;
	color:#003366;
}

#backend a {
	font-family:Verdana,GillSans,"Gill Sans","Gill Sans MT",sans-serif;
	color:#111;
    	text-align: center; 
    	text-decoration: none;
}
#backend a:hover {
    	text-decoration: underline;
}

/******************************************************************************/
/* Backend left styles 						      	      */
/******************************************************************************/

#backend_left {
	float:left;
	width:12%;
	margin-left:15px;
	margin-right:25px;
	margin-top:0px;
	border-right:1px solid #333;
	display:inline-block;
	vertical-align:top;
	
	font-family:Verdana,GillSans,"Gill Sans","Gill Sans MT",sans-serif;
	font-size:11px;
	text-align: right;
}

#backend_left .title {

	color:#003366;
	font-size:14px;
	text-align:right;
	border-bottom:1px solid #003366;
	font-weight:bold;
	margin-top: 15px;
	margin-right: 5px;
	margin-bottom: 5px;
	margin-left: auto;
}

#backend_left .subtitle {
	font-weight:bold;
	font-size:12px;
	color:#333;
  text-decoration: underline;
	margin-right: 5px;
	margin-top: 5px;
}
#backend_left a {
	color:#111;
    	text-decoration: none;
	margin-right: 5px;
}
#backend_left a:hover {
    	text-decoration: underline;
}

/******************************************************************************/
/* Backend show styles	 						      */
/******************************************************************************/

#backend_show table {
	color:#333;
 	width:100%; 
	margin-top:0px;
	margin-bottom:5px;
  text-align:left;
	border-collapse: collapse;
	border-bottom:2px solid #333;
	border-top:2px solid #333;
}

#backend_show th {
	background-color: #a6a6a6;
	color:#000;
	text-align:left;
	font-size:12px;
	font-weight: bold;
} 

#backend_show tr:nth-child(even) {
	background-color: #d9d9d9;
} 

#backend_show tr {
	border:none;
	border-collapse: collapse;
}

#backend_show td {
	border-collapse: collapse;
	text-align:left;
	font-size:14px;
} 

#backend_show form {
	margin: 0;
 	padding: 0;
	border:none;
	width:100%;
}

#backend_show select {
	font-size:14px;
	color: #333;
	background-color: transparent;
}

/*#backend_show button {
	font-size:11px;
	color:#003366;
    	border: 1px solid #333;
	background-color: transparent;
	display: inline-block;
	white-space:nowrap;
}*/

#backend_show button:hover {
	text-decoration: underline;
}

#backend_show label {
	font-weight:bold;
	font-size:12px;
	color:#333;
}

#backend_show input {
	width:95%;
	margin-right:15px;
	background-color: #eff2f6;
    	border: 1px solid #333;
	font-family:Verdana,GillSans,"Gill Sans","Gill Sans MT",sans-serif;
	font-size:10px;
	color:#111;
}

#backend_show .toolbar form, 
#backend_show .toolbar form div {
	display: inline;
}

#backend_show .toolbar button {
	display:inline;
	vertical-align: middle;
  color: #333;
  font-size: 14px;
}

/******************************************************************************/
/* Backend manage styles 						      */
/******************************************************************************/
#backend_manage table {
	/*background-color: #EAEBF6;*/
	font-size:12px;
	color:#333;
	width:80%;	
	margin-top:10px;
	margin-bottom:10px;

	border-collapse: collapse;
	border-top:2px solid #333;
	border-bottom:2px solid #333;
}

#backend_manage th {
	/*background-color: #003366;
	color:#D2D6F6;*/
	text-align:left;
	font-size:12px;
	font-weight: bold;
} 

#backend_manage tr {
	border:none;
	border-collapse: collapse;
}

#backend_manage td {
	border-collapse: collapse;
	text-align:left;
} 

#backend_manage label {
	font-weight:bold;
	font-size:14px;
	color:#333;
	text-align: left;
}

#backend_manage input {
	margin-right:15px;
	/*background-color: #eff2f6;*/
    	border: 1px solid #333;
	font-family:Verdana,GillSans,"Gill Sans","Gill Sans MT",sans-serif;
	font-size:10px;
	color:#111;
	width:100%;
}

#backend_manage form {
	margin: 0;
 	padding: 0;
	border:none;
	display:inline-block;
	width:100%;
}

#backend_manage select {
	font-size:11px;
	/*color: #003366;*/
	background-color: transparent;
}

#backend_manage select:disabled {
	color: #94a8bc;
}

#backend_manage button {
	font-size:11px;
	/*color:#003366;*/
    	border: 1px solid #333;
	background-color: transparent;
	display:inline-block;
}

#backend_manage button:hover {
	text-decoration: underline;
}

/******************************************************************************/
/* Backend tools styles 						      */
/******************************************************************************/
#backend_tools {
	margin-top:0px;

}

#backend_tools table {
	width:75%;
	margin-top:10px;
	border: 1px solid #003366;
	border-spacing: 1px;

	margin-left:10px;	
	margin-right:auto;	
	background-color: #EAEBF6;
	border-collapse: collapse;
	padding:0px 0px 0px 0px;
}

#backend_tools th {
	background-color: #003366;
	color:#D2D6F6;
	text-align:left;
	font-size:12px;
	font-weight: bold;
	text-align:center;
} 

#backend_tools td {
	text-align:center;
	vertical-align:top;
} 

/******************************************************************************/
/* Backend Menu styles 							      */
/******************************************************************************/

#backend_menu {
	font-family:Verdana,GillSans,"Gill Sans","Gill Sans MT",sans-serif;
	border:1px solid #2f608c;
	background-color: #dee6ed;
	margin-top:10px;
	margin-bottom:10px;
	margin-left:auto;
	margin-right:auto;
	width:60%;
}

#backend_menu table {
	margin-left:auto;
	margin-right:auto;
	border:none;
	text-align:center;
	width:100%;
}

#backend_menu a {
	font-weight:bold;
	font-size:12px;
	color:#003366;
    	text-align: center; 
    	text-decoration: none;
    	display: inline-block;
}

#backend_menu a:hover {
	color:#44607C;
}


/******************************************************************************/
/* Backend message styles 						      */
/******************************************************************************/
#backend_message {
	border:1px solid #333;
	background-color: #DDD;
	margin-top: 10px;
	margin-bottom: 10px;
	margin-right: 50%;
	padding: 10px;
}

#backend_message p {
	font-family:Verdana, Arial, Helvetica,sans-serif;
	font-size:11px;
	font-weight: bold;
	
}

#backend_message .info { color: #333; }
#backend_message .warning { color: #cc9900;}
#backend_message .log { color: #363636;}
#backend_message .debug { color: #660066; }
#backend_message .error { color: #990000;}

/******************************************************************************/
