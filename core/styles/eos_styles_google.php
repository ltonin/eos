<?php header("Content-type: text/css");
require_once(__DIR__."/../eos_configuration_init.php");
?>
/**
 * Default theme
 *
 */
/* Slight reset to make the preview have ample padding. */

/* Search button */

#tab5 > div{
    bottom: 5px;
    margin-bottom: 0;
    margin-left: 5%;
    margin-right: 5%;
    margin-top: 0;
    position: absolute;
    width: 90%;
    }
input.gsc-search-button {
	border:none;width:20px;height:20px;  
	font-family: inherit;
	color: transparent;
	background:#000 url(<?php print EOS_BASEURL."core/img/cerca.png"; ?>) no-repeat;
	background-position:4px 2px;
	cursor:pointer;
}

#cse-search-form input.gsc-search-button{margin-left:0;}
.gsc-clear-button, .gsc-search-button{background:#000;}
#cse-search-form .gsc-branding {display:none}
#cse-search-form{margin:auto;width:40%}
#cse-search-form .gsc-input{height:20px}
#cse-search-form div.gsc-clear-button{
	border:none;width:20px;height:20px;  
	font-family: inherit;
	color: transparent;
	background:#000 url(<?php print EOS_BASEURL."core/img/clear.png"; ?>) no-repeat;
	background-position:-3px -3px;
	cursor:pointer;
}

@media only screen and (max-width: 680px){#cse-search-form{width:90%}}
