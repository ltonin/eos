<?php 
header("Content-type: text/css");

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");

session_start();

$color	  ='#ffffff';
$altcolor = '#000000';
$shade	  ='1';

if(isset($_SESSION['color']))
	$color = $_SESSION['color'];
if(isset($_SESSION['altcolor']))
	$altcolor = $_SESSION['altcolor'];
if(isset($_SESSION['shade']))
	$shade    = $_SESSION['shade'];


?>

@font-face {
	font-family: 'Museo300';
	src: url(<?php print EOS_FONTS_BASEURL."Museo300-Regular.eot"; ?>);
	src: local('Museo 300'), 
	local('Museo300'), 
	url(<?php print EOS_FONTS_BASEURL."Museo300-Regular.otf"; ?>) 
	format('opentype');
}

/* Style the footer */
.eos-footer {
	padding: 10px;
  	text-align: center;
	position: relative;
	bottom: -150px;
}

.eos-footer-container {
	display: flex;
	justify-content: space-between;
	align-items: flex-start;
}

.eos-footer-item {
	flex-basis: 0;
	flex-grow: 1;
  	padding-right: 2px;
  	padding-left: 2px;
	font-family: 'Museo300', normal;
}

.eos-footer-item-archive {
	flex-basis: 0;
	flex-grow: 2;
  	padding-right: 2px;
  	padding-left: 2px;
	font-family: 'Museo300', normal;
}

.eos-footer-item-title {
	text-align: center;
	border-bottom: 1px dotted <?php print($altcolor); ?>;
	vertical-align: top;
	font-weight: bold;
	font-size: 14px;
	color: <?php print($altcolor); ?>;
}

.eos-footer-item-content {
	display: flex;
	flex-direction: column;
	row-gap: 10px;
	margin: 10px;
}

.eos-footer-link {
	text-align: left;
}

.eos-footer-link a, .eos-footer-link a:visited {
	display: flex;
	flex-basis: 0;
	font-size: 14px;
	color: <?php print($altcolor); ?>;
}

.eos-footer-link a:hover {
	color: <?php print($color); ?>;
	background-color: <?php print($altcolor); ?>;
}

.eos-footer-archive-content {
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	margin: 10px;
}

.eos-footer-archive-link {
	min-width: 30px;
	padding: 5px;
}

.eos-footer-archive-link a, .eos-footer-archive-link a:visited {
	display: block;
	font-size: 14px;
	color: <?php print($altcolor); ?>;
}

.eos-footer-archive-link a:hover {
	color: <?php print($color); ?>;
	background-color: <?php print($altcolor); ?>;
}

