<?php 
header("Content-type: text/css"); 
require_once(__DIR__."/../../eos_configuration_init.php");

$type="pdf";
$isprinted = true;
if(isset($_GET['type'])) { $type = $_GET['type']; }

if($type == 'ebook') { $isprinted = false; }

?>
   

		/*
.########..#######..##....##.########
.##.......##.....##.###...##....##...
.##.......##.....##.####..##....##...
.######...##.....##.##.##.##....##...
.##.......##.....##.##..####....##...
.##.......##.....##.##...###....##...
.##........#######..##....##....##...
*/

@font-face {
    font-family: 'MyLucidaBold';
    /*src: local("Lucida Sans Demibold");*/
    src: url(http://www.engramma.it/eOS/core/fonts/LSANSD.TTF) format('truetype');

}

@font-face {
    font-family: 'Engramma';
    font-style: normal;
    font-weight: normal;
	src: url(<?php print EOS_FONTS_BASEURL."Engramma-Book.otf"; ?>) format("opentype");
}

@font-face {
    font-family: 'Engramma';
    font-style: italic;
    font-weight: normal;
	src: url(<?php print EOS_FONTS_BASEURL."Engramma-BookItalic.otf"; ?>) format("opentype");
}

@font-face {
    font-family: 'Engramma';
    font-style: normal;
    font-weight: bold;
	src: url(<?php print EOS_FONTS_BASEURL."Engramma-Demi.otf"; ?>) format("opentype");
}

@font-face {
    font-family: 'Engramma';
    font-style: italic;
    font-weight: bold;
	src: url(<?php print EOS_FONTS_BASEURL."Engramma-DemiItalic.otf"; ?>) format("opentype");
}

/*Museo*/
@font-face {
    font-family: 'Museo 300';
	src: url(<?php print EOS_FONTS_BASEURL."Museo300-Regular.eot"; ?>);
    src: url(<?php print EOS_FONTS_BASEURL."Museo300-Regular.otf"; ?>) format('opentype');
}

@font-face {
    font-family: 'Museo 300';
    src: url(<?php print EOS_FONTS_BASEURL."museo-300-italic.eot"; ?>);
    src: url(<?php print EOS_FONTS_BASEURL."Museo-300Italic.otf"; ?>) format('opentype');
    font-style: italic;
}

@font-face {
    font-family: 'Museo 500';
	src: url(<?php print EOS_FONTS_BASEURL."Museo500-Regular.eot"; ?>);
    src: url(<?php print EOS_FONTS_BASEURL."Museo500-Regular.otf"; ?>) format('opentype');
}

@font-face {
    font-family: 'Museo 500';
	src: url(<?php print EOS_FONTS_BASEURL."museo-500-italic.eot"; ?>);
	src: url(<?php print EOS_FONTS_BASEURL."Museo-500Italic.otf"; ?>) format('opentype');
    font-style: italic;
}

@font-face {
    font-family: 'Museo 700';
	src: url(<?php print EOS_FONTS_BASEURL."Museo700-Regular.eot"; ?>);
    src: url(<?php print EOS_FONTS_BASEURL."Museo700-Regular.otf"; ?>) format('opentype');
}

/*hindi*/

@font-face {
    font-family: 'hindi';
	src: url(<?php print EOS_FONTS_BASEURL."Shobhika-Regular.eot"; ?>);
    src: url(<?php print EOS_FONTS_BASEURL."Shobhika-Regular.otf"; ?>) format('opentype');
    unicode-range: U+0900-097F, U+0400-04FF;
}

/* arabic */
@font-face {
    font-family: 'Cairo';
    font-style: normal;
    font-weight: 400;
	src: url(<?php print EOS_FONTS_BASEURL."Cairo-Regular.otf"; ?>) format('opentype'),
        local('Cairo'), local('Cairo-Regular'), url(https://fonts.gstatic.com/s/cairo/v5/SLXGc1nY6HkvalIkTpu0xg.woff2) format('woff2');
    unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
}

/* greek-ext and cyrillic */
@font-face {
    font-family: 'Arev';
	src: url(<?php print EOS_FONTS_BASEURL."878812403fc38d24102f0458bf6e4744.eot"; ?>);
	src: url(<?php print EOS_FONTS_BASEURL."878812403fc38d24102f0458bf6e4744.eot?#iefix"; ?>) format("embedded-opentype"),
		 url(<?php print EOS_FONTS_BASEURL."878812403fc38d24102f0458bf6e4744.woff2"; ?>) format("woff2"),
		 url(<?php print EOS_FONTS_BASEURL."878812403fc38d24102f0458bf6e4744.woff"; ?>) format("woff"),
		 url(<?php print EOS_FONTS_BASEURL."878812403fc38d24102f0458bf6e4744.ttf"; ?>) format("truetype"),
		 url(<?php print EOS_FONTS_BASEURL."878812403fc38d24102f0458bf6e4744.svg#Lucida Grande V1"; ?>) format("svg");
	src: url(<?php print EOS_FONTS_BASEURL."LucidaSansUnicode.ttf"; ?>) format('truetype');
	src: url(<?php print EOS_FONTS_BASEURL."Arev.eot"; ?>);
	src: url(<?php print EOS_FONTS_BASEURL."Arev.eot?#iefix"; ?>) format('embedded-opentype'),
		 url(<?php print EOS_FONTS_BASEURL."Arev.ttf"; ?>) format('truetype');
    unicode-range: U+1F00-1FFF, U+0370-03FF, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116, U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}

/* arabic */
@font-face {
    font-family: 'Tajawal';
    font-style: normal;
    font-weight: 400;
	src: url(<?php print EOS_FONTS_BASEURL."Tajawal-Regular.otf"; ?>) format('opentype'), local('Tajawal'), local('Tajawal-Regular'), url(https://fonts.gstatic.com/s/tajawal/v2/Iura6YBj_oCad4k1nzSBC45I.woff2) format('woff2');
    unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
}


/*
.########.....###.....######...########
.##.....##...##.##...##....##..##......
.##.....##..##...##..##........##......
.########..##.....##.##...####.######..
.##........#########.##....##..##......
.##........##.....##.##....##..##......
.##........##.....##..######...########
*/
/*Settaggi di impostazione della pagina*/

@page {
    size: 170mm 240mm;
    /* size: 170mm 240mm; */
    /* margin: 23.5mm 27.5mm 23mm; */
    /* margin: 21mm 26mm 20.5mm; */

    margin-top: 24mm;
    margin-bottom: 23.5mm;
    margin-inside: 19.5mm;
    margin-outside: 26mm;


    @bottom-center {
        content: element(footer);
    }
    /* -prince-page-fill: prefer-fill; */
}

@page colophon {
    @bottom-center {
        content: "";
    }

    @bottom-right-corner {
        content: "";
    }

    @bottom-left-corner {
        content: "";
    }
}

@page nera {
    @bottom-center {
        content: "";
    }

    @bottom-right-corner {
        content: "";
    }

    @bottom-left-corner {
        content: "";
    }

	<?php if ($isprinted == true) { ?>
		prince-bleed: 3mm 0 3mm 3mm !important;
	<?php } ?>
    background-color: black;
    content: element(sinistra);
}

@page titolone {
    @bottom-center {
        content: "";
    }

    @bottom-right-corner {
        content: "";
    }

    @bottom-left-corner {
        content: "";
    }

}

@page sommario {
    @bottom-center {
        content: "";
    }

    @bottom-right-corner {
        content: "";
    }

    @bottom-left-corner {
        content: "";
    }

}

@page :right {

    @bottom-right-corner {
        content: counter(page);
        font: normal bold 8pt 'Engramma';
        text-align: center;
        /* padding-left: 2pt; */
    }

	<?php if ($isprinted == true) { ?>
		prince-bleed: 3mm 3mm 3mm 0mm !important;
	<?php } ?>
}

@page :left {

    @bottom-left-corner {
        content: counter(page);
        font: normal bold 8pt 'Engramma';
        text-align: center;
        /* padding-right: 2pt; */
    }

	<?php if ($isprinted == true) { ?>
		prince-bleed: 3mm 0mm 3mm 3mm !important;
	<?php } ?>
}

@page :first {
    @bottom-center {
        content: "";
    }

    @bottom-right-corner {
        content: "";
    }

}

@page :blank {
    @bottom-center {
        content: "";
    }

    @bottom-left-corner {
        content: "";
    }

    @bottom-right-corner {
        content: "";
    }
}

body {
    /* font-family: MyLucida; */
    /* font-family: Engramma, MyLucida; */
    font-family: Engramma;
    margin: 0;
}

:lang(fr) {
    prince-text-replace: 
        "!" "\202f!"  /* add short nbsp before exclamation mark */
        "?" "\202f?"  /* add short nbsp before question mark */
        ":" "\202f:"  /* add short nbsp before colon */
        ";" "\202f;"  /* add short nbsp before semi-colon */
        "!\202f!" "!!"  /* remove short nbsp before consecutive marks */
        "!\202f?" "!?"  /* remove short nbsp before consecutive marks */
        "?\202f!" "?!"  /* remove short nbsp before consecutive marks */
        "?\202f?" "??"  /* remove short nbsp before consecutive marks */
        "\20\202f" "\202f" /* remove space before short nbsp */
}

footer {
    position: running(footer);
    font-family: Engramma;
    font-size: 8pt;
    text-align: center;
    /*font-variant: prince-no-kerning;*/
}

/*
.########.....###.....######...########....########..########..########....###....##....##
.##.....##...##.##...##....##..##..........##.....##.##.....##.##.........##.##...##...##.
.##.....##..##...##..##........##..........##.....##.##.....##.##........##...##..##..##..
.########..##.....##.##...####.######......########..########..######...##.....##.#####...
.##........#########.##....##..##..........##.....##.##...##...##.......#########.##..##..
.##........##.....##.##....##..##..........##.....##.##....##..##.......##.....##.##...##.
.##........##.....##..######...########....########..##.....##.########.##.....##.##....##
*/
/*Interruzioni di pagina*/

h2.titolo {
    page-break-before: right;
}
h2.titolo-no-indice {
    page-break-before: always;
}


h1.titolo {
    page: titolone;
    page-break-before: always;
}

.break {
    page-break-before: always
}


h2.titolo,
h2.titolo-no-indice,
h3.sottotitolo,
h4.autore {
    page-break-after: avoid;
    page-break-inside: avoid;
    column-span: all;
}
h4.autore::after {
    columns: 1;
}
h4.autore-no-indice {
    page-break-after: avoid;
    page-break-inside: avoid;
    column-span: all;
}
h4.autore-no-indice::after {
    columns: 1;
}

h6.paragrafo {
    page-break-after: avoid;
    page-break-inside: avoid;
}

h5.biblio {
    break-inside: avoid;
    columns: 1;
    column-span:all;
    /* break-before: page; */
}
h5.biblio ~ h5.biblio {
    break-before: auto;
}

img,
p img {
    page-break-inside: avoid;
    page-break-after: avoid;
}


td {
    page-break-inside: auto;
}

.colophon {
    page: colophon;
}

.sommario {
    page: sommario;
}

.sinistra {
    page-break-before: left;
    display: block;
    page: nera;
    color: white;
    font: normal 36pt/60pt "Museo 300";
    text-align: left;
}

.sinistra b {
    font: normal 36pt/60pt "Museo 700";
}

div.text {
    page-break-inside: avoid;
}

h5.galleria {
    page-break-inside: avoid;
    page-break-after: avoid;
    page-break-before: always;
    column-span:all;
    
}
h6.galleria__titolo {
    page-break-inside: avoid;
    page-break-after: avoid;
    column-span:all;
    
}

.hidden {
    display: none;
}

/*
..######..##.......########....###....##....##
.##....##.##.......##.........##.##...###...##
.##.......##.......##........##...##..####..##
.##.......##.......######...##.....##.##.##.##
.##.......##.......##.......#########.##..####
.##....##.##.......##.......##.....##.##...###
..######..########.########.##.....##.##....##
*/
/*pulizia generale*/
html,
body {
    font: 9pt/13pt Engramma;
}

div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
font,
img,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-weight: inherit;
    font-style: inherit;
    font-size: 100%;
    font-family: inherit;
    vertical-align: baseline;
    orphans: 2;
    widows: 2;
}

/*tolgo il riferimento a English abstract*/
h5.abstract-ref,
h5.abstract-ref a {
    display: none;
}
h5.abstract,
h5.abstract a {
    display: none;
}

h5.biblio {
    orphans: 3;
    widows: 3;
}

/*faccio partire il testo a 75 mm dal blocco titolo*/
.articolo div:first-child {
    height: 50mm
}

.articolo {
    columns: 1;
}

/*
.########..#######..##....##.########.....######..########.##....##.##.......########
.##.......##.....##.###...##....##.......##....##....##.....##..##..##.......##......
.##.......##.....##.####..##....##.......##..........##......####...##.......##......
.######...##.....##.##.##.##....##........######.....##.......##....##.......######..
.##.......##.....##.##..####....##.............##....##.......##....##.......##......
.##.......##.....##.##...###....##.......##....##....##.......##....##.......##......
.##........#######..##....##....##........######.....##.......##....########.########
*/
/*i font*/
p,
ol {
    /* font: 9pt/13pt hindi, Tajawal, Arev, Engramma; */
    font: 9pt/13pt Engramma;
    /* font: 9pt/13pt hindi, Tajawal, Arev, MyLucida; */
    padding-bottom: 2.5mm;
    /* padding-bottom: 13pt; */
    text-align: justify;
    hyphens: auto;
    hyphenate-lines: 1;
}

strong {
    font-weight: bold;
}


figcaption {
    font: 8pt/11pt Engramma;
    text-align: left;
}

h5.occhiello {
    font: normal 11pt/13.5pt "Museo 300";
    text-align: center;
    padding-top: 15mm
}

h1.titolo {
    font: normal 36pt/50pt hindi, Cairo, Arev, "Museo 500";
    text-align: left;
    /*padding-top: 45mm;*/
}


h2.titolo {
    font: normal 18pt/22pt hindi, Cairo, Arev, "Museo 700";
}

h2.titolo-no-indice {
    font: normal 18pt/22pt hindi, Cairo, Arev, "Museo 300";
}

h3.sottotitolo {
    font: normal 17pt/22pt hindi, Cairo, Arev, "Museo 300";
}

h4.autore {
    font: normal 9pt/13pt hindi, Cairo, Arev, "Museo 300";
    padding: 0 0 20mm;
}

.autore+.autore {
    margin-top: -30px
}

h4.autore-no-indice {
    font: normal 9pt/13pt hindi, Cairo, Arev, "Museo 300";
    padding: 0 0 20mm;
}

.autore-no-indice+.autore-no-indice {
    margin-top: -30px
}


h6.paragrafo {
    font: bold 9pt/13pt hindi, Tajawal, Arev, Engramma;
    /*padding-bottom: 4.5mm;*/
}
/* .clearfix::after {
    content: "";
    display: block;
    clear: both;
} */
h5.galleria {
    font: bold 9pt/13pt hindi, Tajawal, Arev, Engramma;
}
h6.galleria__titolo {
    font: bold 8pt/11pt hindi, Tajawal, Arev, Engramma;
}

div.paragrafo {
    font: bold 9pt/13pt hindi, Tajawal, Arev, Engramma;
    /*padding-bottom: 4.5mm;*/
}

p.caption {
    font-size: 8pt;
    line-height: 9.5pt;
    margin-top: -4.5mm;
    padding-bottom: 4.5mm;
}

p.caption+p.caption {
    margin-top: -4.5mm
}

p.citazione,
p.citadx {
    font-size: 8pt;
    line-height: 11.5pt;
}

p.abstract {
    font-size: 8pt;
    line-height: 9.5pt;
}

p.abstract {
    padding-bottom: 5pt;
}

p.citazione,
p.citadx {
    padding-left: 5mm;
    padding-right: 5mm;
    overflow: auto;
}

td p.citazione {
    padding: 0 2pt 0 0
}

p.citadx {
    text-align: right;
}

.citazione sup {
    font-size: 80%;
    vertical-align: top;
}

sup {
    font-size: 80%;
    vertical-align: top;
}

.maiuscoletto {
    font-variant: small-caps;
}

ul.biblio,
ul.biblio li {
    font: 8pt/11pt Engramma;
    list-style: none;
    padding-bottom: 5pt;
}

ul.biblio li a {
    display: inline !important;
}

h5.biblio {
    font: 9pt/13pt "MyLucidaBold";
    text-align: left;
    padding: 20px 0px 10px;
    margin-top: 20px;
    border-top: 1px dotted #000;
    clear: top;
}

h5.abstract {
    font: 9pt/13pt "MyLucidaBold";
    text-align: left;
    padding: 20px 0px 10px;
    margin-top: 20px;
    border-top: 1px dotted #000;
}

h5.biblio a {
    text-decoration: none;
}

em,
i {
    font-style: italic;
    hyphens: none;
}

h1 em,
h1 i,
h2 em,
h2 i,
h3 em,
h3 i,
h4 em,
h4 i,
h5 em,
h5 i {
    font-style: normal;
}

/*collegameti*/
a:link,
a:visited,
a {
    background: transparent;
    color: #000;
    text-decoration: none;
    /*font-weight: bold;

text-align: left;*/
}

a {
    page-break-inside: avoid
}

table, td, th {
    text-align: center;
}

th {
    font-weight: bold;
}

.no-hyphen {
    hyphens: none;
}

/* link espliciti
    a[href^=http]::after {
        content: " <"attr(href) "> ";
    }

    a[href*=images]::after {
        content: "";
    }
*/

/*.grid_5+.grid_5>p {padding:20px 20px;clear: right;}*/
/*.grid_7>p, .grid_6>p, .grid_5dx>p{padding:20px 20px;}
.grid_10>h6.paragrafo {padding:20px 0 0;}
.grid_3,  .grid_4, .grid_5sx {padding:0 0 0; }*/
/*.grid_1 img, .grid_2 img, .grid_3 img, .grid_4 img, .grid_5 img, .grid_5sx img {
          width: 59.6%;
          float: none;
      }*/



/*
.####.##.....##....###.....######...########..######.
..##..###...###...##.##...##....##..##.......##....##
..##..####.####..##...##..##........##.......##......
..##..##.###.##.##.....##.##...####.######....######.
..##..##.....##.#########.##....##..##.............##
..##..##.....##.##.....##.##....##..##.......##....##
.####.##.....##.##.....##..######...########..######.
*/
figure {
    counter-increment: figure;
    margin: 0 0 5mm 0;
    /* margin-bottom: 5mm; */
    page-break-inside: avoid;
    /* page-break-before: avoid; */
    -prince-float-policy: in-order;
}
figure.fig-small {
    max-width: 40%;
    -prince-float: outside;
    /* float: left; */
    -prince-margin-inside: 1em;
    -prince-margin-outside: -12mm;
    -prince-clear: outside;
}

figure.fig-multi {
    /* width: 144mm; */
    width: 136.5mm;
    /* -prince-float: outside; */
    /* float: left; */
    /* -prince-margin-inside: 1em; */
    -prince-float: column top;
    -prince-clear: end;
    -prince-margin-outside: -12mm;
    /* -prince-clear: outside; */
}
div.multi-imgs {
    max-width: 40%;
    -prince-float: outside;
    margin-bottom: 2mm;
    /* float: left; */
    -prince-margin-inside: 1em;
    -prince-margin-outside: -12mm;
}
div.multi-imgs figure.fig-small {
    max-width: 100%;
}

figure.fig-big {
    /* width: 144mm; */
    width: 136.5mm;
    
    -prince-float: column top;
    -prince-clear: end;
    -prince-margin-outside: -12mm;
    /* object-fit: scale-down; */
    /* -prince-margin-inside: -13mm; */
}

img {
    /* max-width: 131mm !important; */
    width: 100% !important;
    height: auto !important;
    max-height: 170mm !important;
    object-fit: contain;
    /* max-height: 100mm !important; */


	<?php if ($isprinted == true) { ?>
    filter: gray;
    /* IE6-9 */
    -webkit-filter: grayscale(1);
    /* Google Chrome, Safari 6+ & Opera 15+ */
    filter: grayscale(1);
    /* Microsoft Edge and Firefox 35+ */
	<?php } ?>

}

div img:first-child {
    padding-top: 1mm
}


/* TABLE */

table {
    table-layout: auto;
    width: 100% !important;
    margin-bottom: 2mm;
}
td, th {
    padding-left: 2.5mm !important;
    padding-right: 2.5mm !important;
    break-inside: avoid;
}
table, td, th {
    border-collapse: collapse;
}
table:not(.border) td:first-child {
    padding-left: 0mm !important;
}
table:not(.border) td:last-child {
    padding-right: 0mm !important;
}

/* Da confermare */
table.poesia,
table.poesia3 {
    table-layout: fixed;

}

table.poesia td {
    width: 50%;
    vertical-align: top;
}

table.poesia3 td {
    width: 33.33%;
    vertical-align: top;
}

table.border {
    border: 1px solid;
}

.align-left {
    text-align: left;
}

table.border th {
    border: 1px solid;
}
table.border td {
    border: 1px solid;
  }

/* TOC */
#toc {
    position: relative;
    /* left:-1cm; */
    margin-top: 40mm;
}
#toc li { list-style-type: none }
#toc li table {
    border-spacing: 0;
    text-align: left;
    margin-bottom: 0;
}
#toc li td {
    text-align: left;
}
#toc li td.toc_page {
    padding-right: 0.3cm;
    width: 1cm;
}
#toc li.h2 td.toc_page {
    content: target-counter(attr(href), page);
    text-align: right;
}
/* Title */
#toc li.h2 td.toc_title {
    font-style: italic;
}
/* Author */
#toc li.h4 {
    break-before: avoid;
    margin-bottom: 1mm;
}
/* Section */
#toc li.div {
    break-after: avoid;
}
#toc li.div td.toc_title {
    font-weight: bold;
}


/*


.biblio {clear:both;}
ul.biblio {font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em}
ul.biblio, ul.biblio li{list-style:none inside ;padding: 5px 10px;}
ul.biblio li a {display: inline !important;}
h5.biblio {font:normal 12pt "Museo 500";text-align:left; padding:20px 0px 10px; margin-top:20px; border-top:1px dotted #000;}
h5.biblio a{text-decoration:none; } 

p.abstract{font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em;padding:20px 20px 0}
ol.note {font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em; color: #444;}
ol.note, ol.note li{padding: 0 10px}
ol li{list-style-type:decimal;}

.caption {padding:5pt;}



*/

/* OK */
/* 
#toc {
    position: relative;
    left:-1cm;
    margin-top: 20mm;
}
#toc li { list-style-type: none }
#toc li.h2 { font-style: italic;}
#toc li.h4 { 
    break-before: avoid;
    margin-bottom: 1mm;
}
#toc li.div {
    font-weight: bold;
    break-after: avoid;

}
#toc li a {
    /* content: target-counter(attr(href), page); */
    /* position: relative; */
    /* left: -1cm; */
    /* width: 40px !important; */
    /* height: auto; */
    /* text-align: right; */
    /*
    display: inline-block;
    /* border: 1px solid black; */
    /*
}
#toc li a::before {
    content: " ";
    display: inline-block;
    padding: 0cm 0.3cm 0cm 0cm;
    /* margin: 0cm 0cm 0cm -1cm; */
    /*
    width: 0.7cm;
    height: auto !important;
    
    text-align: right;
    font-style: normal;

    /* border: 1px solid red; */
    /*
}
#toc li.h2 a::before {
    content: target-counter(attr(href), page);
} */

/* #toc li.h2 pre { 
    color:red; 
    position: absolute;
    bottom: 0;
    left: 1cm;
    width: auto;
    height: auto;
    text-align:left;
    font-style: italic;
}
#toc li pre {
    border: 1px solid blue;
} */


/* #toc li.h4 { margin-left: 3em; } */

