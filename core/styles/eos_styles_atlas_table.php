<?php header("Content-type: text/css");
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
?>

@import url(<?php print EOS_CSS_BASEURL."eos_styles_colorbox.php"; ?>);

#approfondimenti{display:block;width:49%;float:left}
.grid_5{display:block;float:left;margin-top:0;}
.grid_5 + .grid_5{margin-left: 1%;  clear: right;}

.grid_5>img{max-width:100%;}
.grid_5+.grid_5 img{max-width:100%;}
#cont1 .grid_5 {width:43%;}
#cont1 .grid_5 + .grid_5{width: 56%}
#cont3 .grid_5, #cont4 .grid_5 {width:59%;}
#cont3 .grid_5 + .grid_5{height: 700px;overflow: auto;width:40%;}
#cont4 .grid_5 + .grid_5{width:40%;}
.appuntiW{display:block;width:45%;float:left; margin:1% 0 0}
.caption {font:1em/1.4em "Lucida Sans","Lucida Sans Unicode","Trebuchet MS",arial,sans-serif; text-indent: -1em;}
#menuTavola>li{display:inline-block;border-right:1px solid #000; padding: 0;}
#menuTavola>li>a{padding: 0.5em 1em;display:block;}
#menuTavola>li>a:hover{background:#000;color:#fff !important}
#menuTavola{margin: 5% 5% 0;border-bottom:0.3em solid #000;text-align:right}
#cont1,#cont2, #cont3, #cont4 {margin:1% 5%}
#cont1 h6.paragrafo {padding-top: 4em;}
#cont1 a:link, #cont2 a:link, #cont3 a:link, #cont4 a:link{background:#fff}
#cont1 a:hover, #cont2 a:hover, #cont3 a:hover, #cont4 a:hover{background:#444; color:#fff}

#cont1 p{font:normal 1em/1.4em Arial, sans-serif;text-align:left;letter-spacing:0.04em;padding:20px 20px 0}
#cont1 p.abstract{font:normal 0.9em/1.3em Arial, sans-serif;text-align:left;letter-spacing:0.04em;padding:20px 20px 0}


/*
#cont3>img:first-of-type {position: fixed;}
#cont2{display: none;}
#cont3{display: none;}
#cont4{display: none;}*/
.atlasthd{margin:1% 5%;width:90%}


/*.zoom11, .zoom1, .zoom2, .zoom3, .zoom4, .zoom5, .zoom6, .zoom7, .zoom8, .zoom9, .zoom10, .zoom12, .zoom13, .zoom14, .zoom15, .zoom16, .zoom17, .zoom18, .zoom19, .zoom20, .zoom21, .zoom22, .zoom23, .zoom24, .zoom25, .zoom26, .zoom27, .zoom28, .zoom29, .zoom30, .zoom31, .zoom32 { display: none;}
*/.zoom img{width: 200px;}
.selected {display: inline;}
.attiva  {background:#000; color: #fff !important }

