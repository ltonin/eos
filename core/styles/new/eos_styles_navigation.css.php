<?php header("Content-type: text/css"); ?>

.eos-navigation {
	display: flex;
	justify-content: space-between;
	align-items: flex-start;
	font-family: 'Museo300', normal;
	font-size: 12px;
}

.eos-navigation-item {
	display: flex;
	flex-basis: 0;
	flex-grow: 1;
	align-items: center;
	justify-content: center;
  	margin-right: 2px;
  	margin-left: 2px;
	height: 20px;
	
	background-color: black;
	color: white;
	text-decoration: none;
}

.eos-navigation-item:hover {
	background-color: white;
	color: black;
	text-decoration: underline;
}

.eos-navigation-submenu {
	display: none;
	justify-content: center;
	align-items: self-end;
	align-content: flex-end;
	font-family: 'Museo300', normal;
	font-size: 12px;
	z-index: 2;
	position: relative;
	margin-top: -45px;
}

.eos-navigation-submenu-item {
	display: flex;
	justify-content: center;
  	margin-right: 2px;
  	margin-left: 2px;
  	padding-right: 5px;
  	padding-left: 5px;
	height: 18px;
	
	background-color: white;
	color: black;
	text-decoration: none;

}
