<?php 
header("Content-type: text/css"); 

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once("../../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/new/eos_session_support.php");

eos_session_start();
$issue = $_SESSION['issueshown'];

//print_r($_SESSION);
//print_r($issue);
$bgcolor  = $issue['color'];
$txtcolor = $issue['altcolor'];

?>


body {
	font-family: Arial, Helvetica, sans-serif;
	color: <?php print($txtcolor); ?>;
	background-color: <?php print($bgcolor); ?>;
}

header .title {
	display: block;
	margin-left: auto;
	margin-right: auto;
	max-width: 50%;
	max-height: 150px;
}
