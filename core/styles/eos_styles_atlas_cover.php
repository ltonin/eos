<?php header("Content-type: text/css");
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
?>

@font-face {
	font-family: 'Museo700-Regular';
	src: url(<?php print EOS_FONTS_BASEURL."Museo700-Regular.eot"; ?>);
	src: local(<?php print EOS_FONTS_BASEURL."Museo700-Regular"; ?>), 
	local(<?php print EOS_FONTS_BASEURL."Museo700-Regular"; ?>), 
	url(<?php print EOS_FONTS_BASEURL."Museo700-Regular.otf"; ?>) 
	format('opentype');
}

.atlante td { 
	padding: 0.125em 0.125em 0.25em;
	text-align: center;
	font-family: Museo700-Regular,verdana,sans-serif;
	font-size: 1.8em;
}

.atlante>tbody>tr>td>a>img{
	border: 0.3em solid #000000;
}


