<?php header("Content-type: text/css");
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
?>

/*foglio di stile*/
.grid_5{width:49.6%;float: left;margin-top:2%;}
.grid_5>img{text-align:center;margin:0 auto;display:block;}
.sottotitolo{border-bottom:0.2em solid #000; background:#fff;margin:-5% 5%}
.atlas-perc-tavs img{width: 150px}
h6.paragrafo {padding-top: 2em; line-height: 1.4em;}
h6.paragrafo + h6.paragrafo{padding-top: 0}
