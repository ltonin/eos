.eos-admin-container {
	width: 70%;
	margin-left: 15%;
	min-height: 100%;
	position: absolute;
	background-color: white;
	display: block;
	font-family: verdana, sans-serif;
}

.eos-admin-form {
	width: 88%;	
	margin-top: 10px;
	margin-left: auto;
	margin-right: auto;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 20px;
}

.eos-admin-form * {
	box-sizing: border-box;
}

.eos-admin-form .row {
	background-color: #f2f2f2;
	font-family: verdana, sans-serif;
}

/* Clear floats after the columns */
.eos-admin-form .row:after {
	content: "";
  	display: table;
  	clear: both;
}

.eos-admin-form .col-left {
	float: left;
  	width: 10%;
  	margin-top: 6px;
}

.eos-admin-form .col-right {
	float: left;
  	width: 80%;
  	margin-top: 6px;
}

.ui-menu-item {
  	font-family: verdana, sans-serif;
	font-size: 12px;
}

.eos-admin-form input[type=text], input[type=password], input[type=email], input[type=url], select, textarea {
	/*width: 100%;*/
  	padding: 12px;
  	border: 1px solid #ccc;
  	border-radius: 4px;
  	resize: vertical;
  	font-family: verdana, sans-serif;
	font-size: 12px;
}

.eos-admin-form label {
	padding: 12px 12px 12px 0;
  	display: inline-block;
	font-family: verdana, sans-serif;
	font-size: 14px;
}

.eos-admin-form input[type=submit], input[type=button], input[type=reset] {
	background-color: transparent;
  	color: black;
  	padding: 5px 10px;
  	border: 2px solid black;
  	border-radius: 14px;
  	cursor: pointer;
	/*float: right;*/
	font-family: verdana, sans-serif;
	font-size: 14px;
	font-weight: bold;

}

.eos-admin-form input[type=radio] {
	accent-color: black;
}

.eos-admin-form input[type=submit]:hover, input[type=button]:hover, input[type=reset]:hover {
	background-color: black;
  	color: white;
}

/* Responsive layout - when the screen is less than 1000px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 1000px) {
  .eos-admin-form .col-left, .eos-admin-form .col-right, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}

.eos-admin-form .editor {
	width: 40% !important;
	float: left;
	margin-right: 5%;
}

/*
.eos-admin-container h1 {
	font-weight: bold;
	font-size: 16px;
	text-decoration: underline;
	margin-left: 5%;
}

.eos-admin-container h2 {
	font-weight: bold;
	font-size: 12px;
	text-decoration: none;
}

.eos-admin-table {
	width: 90%;
	display: table;
  	font-family: verdana, sans-serif;
	margin-top: 20px;
	margin-left: auto;
	margin-right: auto;
}

.eos-admin-table .header-row {
	display: table-header-group;
	background-color: black;
	color: white;
	font-weight: bold;
	font-size: 12px;
}

.eos-admin-table .header-cell{
	display: table-cell;
	padding: 5px;
	text-align: center;
	border: 1px solid white;
}

.eos-admin-table .body {
	display: table-row-group;
}

.eos-admin-table .body-row {
	display: table-row;
}

.eos-admin-table .body-row:nth-of-type(odd) {
	display: table-row;
	background-color: #e3e3e3;
}

.eos-admin-table .body-row:hover {
	background-color: #636363;
	color: white;
}

.eos-admin-table .body-cell {
	display: table-cell;
	text-align: center;
	vertical-align: middle;
  	font-family: verdana, sans-serif;
	font-size: 12px;
}

.eos-admin-table img {
	background-color: white;
	max-height: 24px;
	border: 1px solid black;
	padding: 2px;
	border-radius: 6px;
}

.eos-admin-table img:hover {
	background-color: #e3e3e3;
}

.eos-admin-issue-preview {
	display: block;
	border: 1px solid black;
	min-height: 400px;
}

.eos-admin-issue-preview .element {
	display: inline-block;
	border: 1px solid black;
	margin: 5px;
	vertical-align: top;
	width: 48%;
	min-height: 400px;
}

.eos-admin-issue-preview .scaled-content {
	transform: scale(1.0);
}
*/

