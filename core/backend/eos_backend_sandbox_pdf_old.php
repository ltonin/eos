<?php

//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
eos_session_start();
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
/*$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);*/

$issue="";
if(isset($_GET['issue'])) {
	$issue = $_GET['issue'];
}


// Retrieve issueid for number 150
$tav = new Table('tb_numero');
$tav->SetCondition('numero', $issue, ElementType::AsInteger, QueryCondition::IsEqual);
$tav->Select('special');
$tav->Get('special', $issuespecial, 0);

?>
<html>

<head>
    <meta charset="UTF-8">
    <style>
/*testo base*/
      @font-face {
          font-family: 'MyLucida';
          src: local("Lucida Sans Unicode"),
              local("Lucida Grande"),
              local("Lucida Sans"),
              local(arial),
              local(sans-serif);
      }

      /*hindi*/

      @font-face {
          font-family: 'hindi';
          src: url("http://www.engramma.it/eOS/core/fonts/Shobhika-Regular.eot");
          src: local('Shobhika Regular'),
              url("http://www.engramma.it/eOS/core/fonts/Shobhika-Regular.otf") format('opentype');
          unicode-range: U+0900-097F, U+0400-04FF;
      }
      /* arabic */
      @font-face {
          font-family: 'Cairo';
          font-style: normal;
          font-weight: 400;
          src: url("http://www.engramma.it/eOS/core/fonts/Cairo-Regular.otf") format('opentype'),
              local('Cairo'), local('Cairo-Regular'), url(https://fonts.gstatic.com/s/cairo/v5/SLXGc1nY6HkvalIkTpu0xg.woff2) format('woff2');
          unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
      }
      /* greek-ext and cyrillic */
      @font-face {
          font-family: 'Arev';
          src: url("http://www.engramma.it/eOS/core/fonts/Arev.eot");
          src: url("http://www.engramma.it/eOS/core/fonts/Arev.eot?#iefix") format('embedded-opentype'),
              url("http://www.engramma.it/eOS/core/fonts/Arev.ttf") format('truetype');
          unicode-range: U+1F00-1FFF, U+0370-03FF, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116, U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
      }
      /* arabic */
      @font-face {
          font-family: 'Tajawal';
          font-style: normal;
          font-weight: 400;
          src: url("http://www.engramma.it/eOS/core/fonts/Tajawal-Regular.otf") format('opentype'), local('Tajawal'), local('Tajawal-Regular'), url(https://fonts.gstatic.com/s/tajawal/v2/Iura6YBj_oCad4k1nzSBC45I.woff2) format('woff2');
          unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
      }



    /*Settaggi di impostazione della pagina*/

    @page {
        size: 170mm 240mm;
        margin:  23.5mm 27.5mm 23mm;

        @bottom-center {
            content: "La Rivista di Engramma n. <?php print($issue.' - '.$issuespecial) ?>";
            font: normal 8pt MyLucida;
        }
    }

    @page :right {
        size: 17cm 24cm;
        margin: 23.5mm 27.5mm 23mm;
        @bottom-right-corner {
            content: "| "counter(page);
            font: normal 9pt MyLucida;
        }
    }
    @page :left {
        size: 17cm 24cm;
        margin:  23.5mm 27.5mm 23mm;

        @bottom-left-corner {
            content: counter(page) " |";
            font: normal 9pt MyLucida;
        }
    }
    @page :first {
        @bottom-center {
            content: "";
        }
        @bottom-right-corner {
            content: "";
        }
    }
    @page :blank {
        @bottom-center {
            content: "";
        }
        @bottom-left-corner {
            content: "";
        }
    }
    body {
        margin: 0;
        font-family: MyLucida;
    }

    /*Interruzioni di pagina*/

    h2.titolo {
        page-break-before: right;
    }
    h6.paragrafo, h5.biblio, h2.titolo, h3.sottotitolo, h4.autore {
        page-break-after: avoid;
        page-break-inside: avoid
    }
    img, p img {
        page-break-inside: avoid;
        page-break-after: avoid;
    }
    p.caption {
        page-break-before: avoid;
    }
    td {page-break-inside: auto;}



    /*pulizia generale*/
    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, font, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-weight: inherit;
        font-style: inherit;
        font-size: 100%;
        font-family: inherit;
        vertical-align: baseline;
    }
    p, ol {
          font: 11pt/13.5pt hindi, Tajawal, Arev, MyLucida;
          padding-top: 13.5pt;
      }
    /*i font*/
    h5.abstract, h5.abstract a {
        display: none
    }
     h2.titolo {
          font: normal 24pt/28.8pt hindi, Cairo, Arev, "Museo 500";
          padding: 13pt 0;
      }
      h3.sottotitolo {
          font: normal 12pt/18pt hindi, Cairo, Arev, "Museo 300";
          padding: 0 50pt 0 0;
      }
      h4.autore {
          font: normal 12pt/18pt hindi, Cairo, Arev, "Museo 300";
          padding: 0 0 30px;
      }
      .autore+.autore {
          margin-top: -30px
      }
      h6.paragrafo {
          font: bold 11pt/13.5pt hindi, Cairo, Arev, "Museo 300";
          padding-top: 20pt;
      }
      
    p.caption {
        font-size: 8pt;
        padding: 5px;
    }
    p.abstract, p.citazione {
        font-size: 9pt;
        line-height: 13.5pt;
        padding: 20px 15px 10px 5px
    }
    sup {
        font-size: 50%;
        vertical-align: middle;
    }

    .maiuscoletto {
        font-variant: small-caps;
    }
     ul.biblio, ul.biblio li {
    	font: 9pt/11pt hindi, Tajawal, Arev, MyLucida;
        list-style: none inside;
        padding: 5px 0;
    }
    ul.biblio li a {
        display: inline !important;
    }
    h5.biblio {
        font: normal 11pt/13.5pt "Museo 500";
        text-align: left; 
        padding: 20px 0px 10px;
        margin-top: 20px;
        border-top: 1px dotted #000;
    }
    h5.biblio a {
        text-decoration: none;
    }

    /*collegameti*/
    a:link, a:visited, a {
        background: transparent;
        color: #000;
        text-decoration: none;
        /*font-weight: bold;

text-align: left;*/
    }
    a {
        page-break-inside: avoid
    }
/* link espliciti
    a[href^=http]::after {
        content: " <"attr(href) "> ";
    }

    a[href*=images]::after {
        content: "";
    }
*/

    /*immagini*/
    img {
        width: 100%;
    }

    /* colonne */
   .grid_1,.grid_2,.grid_3,.grid_4,.grid_5, .grid_5sx, .grid_5dx, .grid_6,.grid_7,.grid_8,.grid_9,.grid_10{
  display:inline;
float:left;
position:relative;}
.grid_1{width:9.9%;clear: left;}
.grid_2{width:19.9%;clear: left;}
.grid_3{width:29.8%;clear: left;}
.grid_4{width:39.8%;clear: left;}
.grid_5sx{clear: left;}
.grid_5, .grid_5dx, .grid_5sx{width:49.6%;}
.grid_6{width:59.8%;clear: right;}
.grid_7{width:69.4%;clear: right;}
.grid_8{width:79.8%;clear: right;}
.grid_9{width:89.2%;clear: right;}
.grid_10{width:99.8%;clear: both;}
.grid_5+.grid_5>p {padding:20px 20px;clear: right;}
.grid_7>p, .grid_6>p, .grid_5dx>p{padding:20px 20px;}
.grid_10>h6.paragrafo {padding:20px 0 0;}
.grid_3,  .grid_4, .grid_5sx {padding:0 0 0; }
    table.poesia {
    table-layout:fixed;
}
table.poesia td {
    width: 50%;
   }
    /*


.biblio {clear:both;}
ul.biblio {font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em}
ul.biblio, ul.biblio li{list-style:none inside ;padding: 5px 10px;}
ul.biblio li a {display: inline !important;}
h5.biblio {font:normal 12pt "Museo 500";text-align:left; padding:20px 0px 10px; margin-top:20px; border-top:1px dotted #000;}
h5.biblio a{text-decoration:none; } 

p.abstract{font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em;padding:20px 20px 0}
ol.note {font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em; color: #444;}
ol.note, ol.note li{padding: 0 10px}
ol li{list-style-type:decimal;}

.caption {padding:5pt;}

.citazione sup {font-size: 80%;vertical-align: top;}
p.citazione{font:10pt MyLucida;text-align: left; }
p.citadx{font:10pt MyLucida;padding:20px 20px 0;text-align: right;}
*/
    </style>
</head>

<body>
    <h2 class="occhiello">La Rivista di Engramma <br /><?php print($issue) ?></h2>
    <h2 class="titolo">Segnaposto 2</h2>
    <h2 class="titolo">segnaposto 3</h2>

<?php

if(empty($issue)) {
	echo "No issue number provided. Provide the issue number in page
		address. <br><br>Example: eos_backend_sandbox.php<b>?issue=150</b>";
} else {

// Retrieve issueid for number
$table = new Table('tb_numero');
$table->SetCondition('numero', $issue, ElementType::AsInteger, QueryCondition::IsEqual);
$table->Select('id');
$table->Get('id', $issueid, 0);

// Retrieve id of trash status
$table->SetTable('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve all article in issueid (no in trash)
$table->SetTable('tb_articolo');
$table->SetCondition('id_numero', $issueid, ElementType::AsInteger);
$table->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->SetOrder('titolo', QueryOrder::AsAscending);
$table->Select('id');
$table->Get('id', $listarticles);

foreach($listarticles as $carticleid) {
	$carticle = new Article();
	$carticle->Retrieve($carticleid);

	print $carticle->Get('text')."<div class=grid_10></div>";

}
}
?>


</body>
</html>
