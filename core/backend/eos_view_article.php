<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();
$selfurl   = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=12';
$parenturl = EOS_BASEURL.'core/backend/eos_backend_index.php';
$manageurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=11';

$action = "";
if(isset($_GET['action'])) 
	$action = $_GET['action'];
elseif(isset($_POST['action']))
	$action = $_POST['action'];

switch($action) {
	case "modify":
		eos_redirect($manageurl."&action=modify&articleid=".$_POST['articleid']);
		break;
	case "close":
		eos_redirect($parenturl);
		break;
}

if(isset($_GET['articleid'])) {
	$articleid = $_GET['articleid'];
} else if(isset($_POST['articleid'])) {
	$articleid = $_POST['articleid'];
}

$article = new Article();

if($article->Retrieve($articleid) == false)
	$message->AddMessage("Cannot find article in database", MessageType::AsDebug);

?>
<div id="backend_manage">
<div class="title">Visualizza articolo</div>
<?php $message->Show(); ?>
<form action="<?php print $selfurl ?>" method="post">
<table>
<tr>
	<td width=15%><label>Tipo:</label></td>
	<td><?php print $article->Get('type::label'); ?></td>
</tr>
<tr>
	<td><label>Categoria:</label></td>
	<td><?php print $article->Get('section::label'); ?></td>
</tr>
<tr>
	<td><label>Numero:</label></td>
	<td><?php print $article->Get('issue::number'); ?></td>
</tr>
<tr>
	<td><label>Stato:</label></td>
	<td><?php print $article->Get('status::label'); ?></td>
</tr>
<tr>
	<td><label>Creatore:</label></td>
	<td><?php print $article->Get("user::nickname"); ?></td>
</tr>
<tr>
	<td><label>Ultima modifica:</label></td>
	<td><?php print $article->Get("modifiedby::nickname"); ?> (in data: <?php print $article->Get("modified"); ?>)</td>
</tr>
<tr>
	<td><label>Titolo:</label></td>
	<td><?php print $article->Get('title'); ?></td>
</tr>
</table>
<input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
<button type="submit" name="action" value="modify">Modifica</button>
<button type="submit" name="action" value="close">Chiudi</button>
</form>
<?php print $article->Get("text"); ?>
</div>
