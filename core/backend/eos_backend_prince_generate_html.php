<?php

//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
//require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
//require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
//require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
//require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");

eos_session_start();

$issuenumber="";
if(isset($_GET['issue'])) {
	$issuenumber = $_GET['issue'];
}

$issuetype="pdf";
if(isset($_GET['type'])) {
	$issuetype = $_GET['type'];
}

$tb = new Table('tb_numero');
$tb->SetCondition('numero', $issuenumber, ElementType::AsInteger, QueryCondition::IsEqual);
$tb->Select('id', 'special', 'descrizione');
$tb->Get('id', $issueid, 0);
$tb->Get('special', $issuespecial, 0);
$tb->Get('descrizione', $issuetitle, 0);

// Retrieve id of ok elemnts
$tb->SetTable('article_status');
$tb->SetCondition('label', 'ok', ElementType::AsString);
$tb->Select('id');
$tb->Get('id', $statusid, 0);

// Retrieve id of Articles
$tb->SetTable('tb_tipo');
$tb->SetCondition('nome', 'Articolo', ElementType::AsString);
$tb->Select('id');
$tb->Get('id', $typeid, 0);

$tb->SetTable('tb_articolo');
$tb->SetCondition('id_numero', $issueid, ElementType::AsInteger);
$tb->SetCondition('stato', $statusid, ElementType::AsInteger, QueryCondition::IsEqual);
$tb->SetCondition('tipo', $typeid, ElementType::AsInteger, QueryCondition::IsEqual);
$tb->SetOrder('titolo', QueryOrder::AsAscending);
$tb->Select('id', 'testo');
$results = $tb->GetResult();

// Retrieve articles for issue
$articles = array();
while($row = $results->fetch_array()) {
	$entry = $row['testo'];	
	$articles[] = $entry;
}	

?>


<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?php print EOS_CSS_BASEURL . "/prince/engramma_prince_pdf.css.php?type=" . $issuetype; ?>">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
  <script src="<?php print EOS_BASEURL ."/core/functions/prince/func_prince.js" ?>"></script>
  <script src="<?php print EOS_BASEURL ."/core/functions/prince/engramma_prince.js" ?>"></script>
</head>

<footer>
  La Rivista di Engramma <b><?php print($issuenumber) ?></b> <?php print($issuespecial) ?>
</footer>

<body onload="simpleToc('h2.titolo:not(div.sommario>h2.titolo), h4.autore, div.articolo div.sinistra')">
  
	<h5 class="occhiello">La Rivista di Engramma <br />
		<span style="font-family: Museo 700"><?php print($issuenumber) ?></span>
	</h5>
	
	<div class="sinistra">La Rivista di Engramma <br />
		<b><?php print($issuenumber) ?></b><br /><?php print($issuespecial) ?>
	</div>

	<h1 class="titolo"><?php print($issuetitle) ?></h1>
    <div class="colophon"></div>

	<div class="sommario">	
    	<h2 class="titolo">Sommario</h2>
		<ol id=toc></ol>
	</div>

<?php 

	foreach($articles as $article) {
		print "<div class=articolo>".$article."</div>";
	}
?>



</body>
</html>



