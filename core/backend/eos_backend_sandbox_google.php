<?php

//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
eos_session_start();
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
/*$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);*/

$issue="";
if(isset($_GET['issue'])) {
	$issue = $_GET['issue'];
}


// Retrieve issueid for number 150
$tav = new Table('tb_numero');
$tav->SetCondition('numero', $issue, ElementType::AsInteger, QueryCondition::IsEqual);
$tav->Select('special','descrizione');
$tav->Get('special', $issuespecial, 0);
$tav->Get('descrizione',$issuetitle,0);

$pie = '[<b>'.$issue.'</b>] '.$issuespecial;
?>
<html>

<head>
    <meta charset="UTF-8">
    <style>
/*testo base*/
      @font-face {
          font-family: 'MyLucida';
         src: url("http://www.engramma.it/eOS/core/fonts/LSANS.TTF") format('truetype');
      }
            @font-face {
          font-family: 'MyLucidaBold';
          src: url(http://www.engramma.it/eOS/core/fonts/LSANSD.TTF) format('truetype');
        
      }

/*Museo*/
@font-face {
  font-family: 'Museo 300';
  src: url("http://www.engramma.it/eOS/core/fonts/Museo300-Regular.eot");
  src:  
  url("http://www.engramma.it/eOS/core/fonts/Museo300-Regular.otf") 
  format('opentype');
}
@font-face {
  font-family: 'Museo 300';
  src: url("http://www.engramma.it/eOS/core/fonts/museo-300-italic.eot");
  src:  
  url("http://www.engramma.it/eOS/core/fonts/Museo-300Italic.otf") 
  format('opentype');
  font-style: italic;
}
@font-face {
  font-family: 'Museo 500';
  src: url("http://www.engramma.it/eOS/core/fonts/Museo500-Regular.eot");
  src: 
  url("http://www.engramma.it/eOS/core/fonts/Museo500-Regular.otf") 
  format('opentype');
}
@font-face {
  font-family: 'Museo 500';
  src: url("http://www.engramma.it/eOS/core/fonts/museo-500-italic.eot");
  src: 
  url("http://www.engramma.it/eOS/core/fonts/Museo-500Italic.otf") 
  format('opentype');
  font-style: italic;
}
@font-face {
  font-family: 'Museo 700';
  src: url("http://www.engramma.it/eOS/core/fonts/Museo700-Regular.eot");
  src:  
  url("http://www.engramma.it/eOS/core/fonts/Museo700-Regular.otf") 
  format('opentype');
}
      /*hindi*/

      @font-face {
          font-family: 'hindi';
          src: url("http://www.engramma.it/eOS/core/fonts/Shobhika-Regular.eot");
          src: local('Shobhika Regular'),
              url("http://www.engramma.it/eOS/core/fonts/Shobhika-Regular.otf") format('opentype');
          unicode-range: U+0900-097F, U+0400-04FF;
      }
      /* arabic */
      @font-face {
          font-family: 'Cairo';
          font-style: normal;
          font-weight: 400;
          src: url("http://www.engramma.it/eOS/core/fonts/Cairo-Regular.otf") format('opentype'),
              local('Cairo'), local('Cairo-Regular'), url(https://fonts.gstatic.com/s/cairo/v5/SLXGc1nY6HkvalIkTpu0xg.woff2) format('woff2');
          unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
      }
      /* greek-ext and cyrillic */
      @font-face {
          font-family: 'Arev';
           src: url(http://www.engramma.it/eOS/core/fonts/LucidaSansUnicode.ttf) format('truetype');
         /* src: url("http://www.engramma.it/eOS/core/fonts/Arev.eot");
          src: url("http://www.engramma.it/eOS/core/fonts/Arev.eot?#iefix") format('embedded-opentype'),
              url("http://www.engramma.it/eOS/core/fonts/Arev.ttf") format('truetype');*/
          unicode-range: U+1F00-1FFF, U+0370-03FF, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116, U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
      }
      /* arabic */
      @font-face {
          font-family: 'Tajawal';
          font-style: normal;
          font-weight: 400;
          src: url("http://www.engramma.it/eOS/core/fonts/Tajawal-Regular.otf") format('opentype'), local('Tajawal'), local('Tajawal-Regular'), url(https://fonts.gstatic.com/s/tajawal/v2/Iura6YBj_oCad4k1nzSBC45I.woff2) format('woff2');
          unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
      }



    /*Settaggi di impostazione della pagina*/

    @page {
        size: 170mm 240mm;
        margin:  23.5mm 27.5mm 23mm;

        @bottom-center {
           content: element(footer);
        }
    }
    @page colophon {
        @bottom-center { content: "";}
        @bottom-right-corner {
                content: "";
            }
        @bottom-left-corner {
                content: "";
            }
    }
    @page nera {
        @bottom-center { content: "";}
        @bottom-right-corner {
                content: "";
            }
        @bottom-left-corner {
                content: "";
            }
        background-color: black;
        content: element(sinistra);
    }
    @page titolone {
        @bottom-center { content: "";}
        @bottom-right-corner {
                content: "";
            }
        @bottom-left-corner {
                content: "";
            }

    }

    @page :right {
        size: 17cm 24cm;
        margin: 23.5mm 27mm 23mm;
        @bottom-right-corner {
            content:counter(page);
            font: normal 8pt 'Museo 700';
            padding-left: 4pt;
        }
    }
    @page :left {
        size: 17cm 24cm;
        margin:  23.5mm 27.5mm 23mm;

        @bottom-left-corner {
            content: counter(page);
            font: normal 8pt 'Museo 700';
            padding-right: 4pt;
        }
    }
    @page :first {
        @bottom-center {
            content: "";
        }
        @bottom-right-corner {
            content: "";
        }
        
    }
    @page :blank {
        @bottom-center {
            content: "";
        }
        @bottom-left-corner {
            content: "";
        }
    }
    body {
        margin: 0;
        font-family: MyLucida;
    }
    footer {position: running(footer);
      font-family: MyLucida;
      font-size: 8pt;
      text-align: center;
    /*font-variant: prince-no-kerning;*/}

    /*Interruzioni di pagina*/

    h2.titolo {
        page-break-before: right;
    }
    h1.titolo {page:titolone;
        page-break-before: always;
    }
    .break {page-break-before: always}

    h6.paragrafo, h5.biblio, h2.titolo, h3.sottotitolo, h4.autore {
        page-break-after: avoid;
        page-break-inside: avoid
    }
    img, p img {
        page-break-inside: avoid;
        page-break-after: avoid;
    }
    p.caption { page-break-inside: avoid;
        page-break-before: avoid;
    }
    td {page-break-inside: auto;}

    .colophon{page:colophon;}
    .sinistra {
      page-break-before: left;
      display: block;
      page: nera;
      color: white;
      font: normal 36pt/60pt "Museo 300";
      text-align: left;
      }
      .sinistra b{font: normal 36pt/60pt "Museo 700";}
    
    /*pulizia generale*/
    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, font, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-weight: inherit;
        font-style: inherit;
        font-size: 100%;
        font-family: inherit;
        vertical-align: baseline;
        orphans: 2;
        widows: 2;
    }
    /*tolgo il riferimento a English abstract*/
       h5.abstract, h5.abstract a {
        display: none;
    }
    /*faccio partire il testo a 75 mm dal blocco titolo*/
     .articolo div:first-child {height: 50mm} 

    /*i font*/
    p, ol {
          font: 9pt/13pt hindi, Tajawal, Arev, MyLucida;
          padding-bottom: 4.5mm;
      }

      h5.occhiello{font: normal 11pt/13.5pt "Museo 300";
      text-align: center;padding-top: 15mm}
      h1.titolo {
          font: normal 36pt/50pt hindi, Cairo, Arev, "Museo 500";
                text-align: left;
                /*padding-top: 45mm;*/
                }
     h2.titolo {
          font: normal 18pt/22pt hindi, Cairo, Arev, "Museo 700";
                }
     h3.sottotitolo {
          font: normal 17pt/22pt hindi, Cairo, Arev, "Museo 300";
      }
      h4.autore {
          font: normal 9pt/13pt hindi, Cairo, Arev, "Museo 300";
          padding: 0 0 20mm;
      }
      .autore+.autore {
          margin-top: -30px
      }

    h6.paragrafo {font: bold 9pt/13pt hindi, Tajawal, Arev, MyLucidaBold; /*padding-bottom: 4.5mm;*/}  
    p.caption {
        font-size: 8pt;
        line-height: 9.5pt;
        margin-top:-4.5mm;
        padding-bottom: 4.5mm;
    }
    
    p.caption+p.caption{margin-top: -4.5mm}
    p.citazione, p.citadx {
        font-size: 8pt;
        line-height: 13pt;
    }
      p.abstract {
        font-size: 8pt;
        line-height: 9.5pt;
    }
    p.abstract{padding-bottom: 5pt;}
    p.citazione, p.citadx {padding-left: 5mm; padding-right:5mm;}
    td p.citazione {padding: 0 2pt 0 0}
    p.citadx{
      text-align: right;
    }
    .citazione sup {
      font-size: 80%;vertical-align: top;}
    sup {
        font-size: 50%;
        vertical-align: middle;
    }

    .maiuscoletto {
        font-variant: small-caps;
    }
     ul.biblio, ul.biblio li {
    	font: 8pt/11pt hindi, Tajawal, Arev, MyLucida;
        list-style: none;
        padding-bottom: 5pt;
    }
    ul.biblio li a {
        display: inline !important;
    }
    h5.biblio {
        font: 9pt/13pt "MyLucidaBold";
        text-align: left; 
        padding: 20px 0px 10px;
        margin-top: 20px;
        border-top: 1px dotted #000;
    }
    h5.biblio a {
        text-decoration: none;
    }

    em, i {font-style: italic;}
    h1 em, h1 i, h2 em, h2 i, h3 em, h3 i, h4 em, h4 i, h5 em, h5 i {font-style: normal;}

    /*collegameti*/
    a:link, a:visited, a {
        background: transparent;
        color: #000;
        text-decoration: none;
        /*font-weight: bold;

text-align: left;*/
    }
    a {
        page-break-inside: avoid
    }
/* link espliciti
    a[href^=http]::after {
        content: " <"attr(href) "> ";
    }

    a[href*=images]::after {
        content: "";
    }
*/

    /*immagini*/
    img {
        width: 100%;    }
    div img:first-child {padding-top: 1mm} 

    /* colonne */
  .grid_1,.grid_2,.grid_3,.grid_4, .grid_5sx{
  /*, .grid_6,.grid_7,.grid_8,.grid_9 display:inline;*/
float:left;
position:relative;}

.grid_1,.grid_2, .grid_3, .grid_4, .grid_5, .grid_5sx{width:44.8mm; padding-right: 4mm}
.grid_6, .grid_7, .grid_8, .grid_9, .grid_5+.grid_5, .grid_5dx {width:auto;}
.grid_7::after, .grid_6::after,.grid_5+.grid_5::after {
  content: "";
  clear: both;
  display: table;
}
.grid_10{width:116mm;clear: both;}
.grid_5+.grid_5 {padding-right: 0;float: right; display: inline;}
/*.grid_5+.grid_5>p {padding:20px 20px;clear: right;}*/
/*.grid_7>p, .grid_6>p, .grid_5dx>p{padding:20px 20px;}
.grid_10>h6.paragrafo {padding:20px 0 0;}
.grid_3,  .grid_4, .grid_5sx {padding:0 0 0; }*/
          /*.grid_1 img, .grid_2 img, .grid_3 img, .grid_4 img, .grid_5 img, .grid_5sx img {
          width: 59.6%;
          float: none;
      }*/

    table.poesia, table.poesia3 {
    table-layout:fixed;
    
}
table.poesia td {
    width: 50%;
    vertical-align: top;
   }
   table.poesia3 td {
    width: 33.33%;
    vertical-align: top;
   }
    /*


.biblio {clear:both;}
ul.biblio {font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em}
ul.biblio, ul.biblio li{list-style:none inside ;padding: 5px 10px;}
ul.biblio li a {display: inline !important;}
h5.biblio {font:normal 12pt "Museo 500";text-align:left; padding:20px 0px 10px; margin-top:20px; border-top:1px dotted #000;}
h5.biblio a{text-decoration:none; } 

p.abstract{font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em;padding:20px 20px 0}
ol.note {font:normal 8pt MyLucida;text-align:left;letter-spacing:0.04em; color: #444;}
ol.note, ol.note li{padding: 0 10px}
ol li{list-style-type:decimal;}

.caption {padding:5pt;}



*/
    </style>
</head>

<body>
    <h5 class="occhiello">La Rivista di Engramma <br /><span style="font-family: Museo 700"><?php print($issue) ?></span></h5>
    <div class="sinistra">La Rivista di Engramma <br /><b><?php print($issue) ?></b><br /><?php print($issuespecial) ?></div>

  <h1 class="titolo"><?php print($issuetitle) ?></h1>
    <div class="colophon">  </div>
    <h2 class="titolo">Sommario</h2>

<footer>
  La Rivista di Engramma&nbsp;&nbsp;<b><?php print($issue) ?></b>&nbsp;&nbsp;<?php print($issuespecial) ?>
</footer>
<?php

if(empty($issue)) {
	echo "No issue number provided. Provide the issue number in page
		address. <br><br>Example: eos_backend_sandbox.php<b>?issue=150</b>";
} else {

// Retrieve issueid for number
$table = new Table('tb_numero');
$table->SetCondition('numero', $issue, ElementType::AsInteger, QueryCondition::IsEqual);
$table->Select('id');
$table->Get('id', $issueid, 0);

// Retrieve id of ok elemnts
$table->SetTable('article_status');
$table->SetCondition('label', 'ok', ElementType::AsString);
$table->Select('id');
$table->Get('id', $isokid, 0);

// Retrieve id of Articles
$table->SetTable('tb_tipo');
$table->SetCondition('nome', 'Articolo', ElementType::AsString);
$table->Select('id');
$table->Get('id', $isArticle, 0);

// Retrieve all article in issueid (no in trash)
$table->SetTable('tb_articolo');
$table->SetCondition('id_numero', $issueid, ElementType::AsInteger);
$table->SetCondition('stato', $isokid, ElementType::AsInteger, QueryCondition::IsEqual);
$table->SetCondition('tipo', $isArticle, ElementType::AsInteger, QueryCondition::IsEqual);
$table->SetOrder('titolo', QueryOrder::AsAscending);
$table->Select('id');
$table->Get('id', $listarticles);

foreach($listarticles as $carticleid) {
	$carticle = new Article();
	$carticle->Retrieve($carticleid);

	print "<div class=articolo>".$carticle->Get('text')."</div>";

}
}
?>


</body>
</html>
