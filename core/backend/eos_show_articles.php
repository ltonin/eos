<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();

// Define urls
$urlarea   = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlmanage = $urlarea.'?tool=11';
$urlview   = $urlarea.'?tool=12';
$urlself   = $urlarea.'?tool=13';



// Management of actions
$action = "";
if(isset($_GET['action'])) {
	$action = $_GET['action'];
} else if(isset($_POST['action'])) {
	$action = $_POST['action'];
}

$showissue = new Issue();
$showissueid = "";
if(isset($_GET['showissueid'])) {
	$showissueid = $_GET['showissueid'];
} else if(isset($_POST['showissueid'])) {
	$showissueid = $_POST['showissueid'];
}

if(empty($showissueid) == false) {
  $showissue->Retrieve($showissueid);
 }

$listarticles = array();
switch($action) {
	case "setstatus":
	  $articleid = $_POST['articleid'];
		$article = new Article();
		$article->Retrieve($articleid);
		$articlestatus = new ArticleStatus();
		$articlestatus->Retrieve($_POST['statusid']);
		$refarticle = &$article;	
		$refstatus  = &$articlestatus;	
		
		if(!eos_article_setstatus($refarticle, $refstatus))
			$message->AddMessage("Stato non modificato", MessageType::AsWarning);
		else
			$message->AddMessage("Stato articolo modificato ('"
				     .$article->Get('title')."'): '"
			             .$article->Get('status::label')."'", MessageType::AsInfo);
		break;
	default:
		break;
}

// Retrieve id of trash status
$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve all the possible status for articles (except 'trash')
$table->SetTable('article_status');
$table->SetCondition('id', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->Select('id');
$table->Get('id', $liststatus);

// Retrieve all issues status
$table->SetTable('issue_status');
$table->SetOrder('id');
$table->Select('id');
$table->Get('id', $istatus_l);
?>
<div id="backend_show">
<?php $message->Show(); ?>

<form  action="<?php print $urlmanage; ?>" method="post">
<table style="width:20%">
<tr>
<td style="width:50%">Modifica articolo:</td>
<td style="width:40%; text-align:left">
<input style="float:left; font-size:12px" type="text" name="articleid" size="6" maxlength="6" placeholder="ID articolo"></td>
<td style="width:10%">
	<button type="submit" name="action"    value="modify" >
	<img src="<?php print EOS_SYSTEM_IMAGES.'icon-edit.png'; ?>" width="18" title="Modifica">
</button></td>
</table>
</form>
<table>
<?php
// Iterate across issue status (e.g., Infieri, Pubblicato, Archiviato)
foreach($istatus_l as $cistatus_id) {
	$istatus = new IssueStatus();
	$istatus->Retrieve($cistatus_id);
	$table->SetTable('tb_numero');
	$table->SetCondition('stato', $istatus->Get('id'), ElementType::AsInteger);
	$table->SetOrder('numero', QueryOrder::AsDescending);
	$table->Select('id');
	$table->Get('id', $issues_l);
  ?>
  <tr>
  <th style="text-align:left; vertical-align:top;"><?php print $istatus->Get("label").":"; ?></th>
  </tr>
  <?php
  // Iterate across issues belonging to the given status
  foreach($issues_l as $cissue_id) {
    $cissue = new Issue();
    $cissue->Retrieve($cissue_id);

    // Temporary hack to excude Tavole
    if($cissue->Get('number') < 1000 && $cissue->Get('number') != 999) { 	
    ?>
      <tr >
      <td style="text-align:left; vertical-align:top; font-weight:bold;">
      <a href="<?php print $urlself."&showissueid=".$cissue->Get('id'); ?>"><?php print "Numero ".$cissue->Get('number'); ?></a>
      </td>
      </tr>
      <?php
		  // If the current issue is requested then show all articles belonging to
		  if($showissue->Get('id') == $cissue->Get('id')) {
        ?>
        <tr>
        <td>
        <div class="subtable">
        <table style="margin-right:auto; margin-left:0;">
        <tr>
        <th>Titolo</th>
        <th>Creatore</th>
        <th>Ultima modifica</th>
        <th>Tipo</th>
        <th>Stato</th>
        <th>Strumenti</th>
        </tr>
        <?php                               
			  $showtable = new Table('tb_articolo');
			  $showtable->SetCondition('id_numero', $showissue->Get('id'), ElementType::AsInteger);
			  $showtable->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
			  $showtable->SetOrder('pos');
			  $showtable->Select('id');
			  $showtable->Get('id', $listarticles);
			  // Iterate along articles belonging to this issue -->
			  foreach($listarticles as $carticle_id) {
				  $article = new Article();    
				  $article->Retrieve($carticle_id);
          ?>
				  <tr>
				  <!-- General information -->
				  <td style="max-width:300px; word-wrap:break-word;" >
				  <a href="<?php print $urlview."&articleid=".$article->Get("id"); ?>">
				  <?php print $article->Get("title"); ?>
				  </a>
				  </td>
				  <td><?php print $article->Get("user::nickname"); ?></td>
				  <td><?php print $article->Get("modifiedby::nickname")." (".$article->Get("modified").")"; ?></td>
				  <td><?php print $article->Get("type::label"); ?></td>
          
          <!-- Status selection -->
          <td>
          <form action="<?php print $urlself."&showissueid=".$cissue->Get('id'); ?>" method="post">
          <select name="statusid" onchange="this.form.submit()">
          <?php
          reset($liststatus);
          foreach($liststatus as $cstatusid) {
  	        $status = new ArticleStatus();
  	        $status->Retrieve($cstatusid);
  	        $selected = "";
  	        if($status->Get("id") == $article->Get("status::id"))
  	        	$selected = "selected";
          ?>
            <option value="<?php print $status->Get("id"); ?>" <?php print $selected; ?>>
	          <?php print $status->Get("label"); ?>
            </option>
          <?php
          }
          ?>
          </select>
          <input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
          <input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
          <input type="hidden" name="action" value="setstatus">
          </form>
          </td>	
          <!-- Modify/Trash buttons -->
          <td>
          <div class="toolbar">
          <form  action="<?php print $urlmanage; ?>" method="post">
          <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
          <button type="submit" name="action"    value="modify" >
          <img src="<?php print EOS_SYSTEM_IMAGES.'icon-edit.png'; ?>" width="24" title="Modifica">
          </button>
          </form>

		  <form  action="<?php print $urlself."&showissueid=".$cissue->Get('id'); ?>" method="post" 
			onsubmit="return confirm('L\'articolo verr\u00E0 spostato nel cestino. Confermi l\'operazione?')" >
          <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
          <input  type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
          <input  type="hidden" name="statusid"  value="<?php print $trashid; ?>">
          <button type="submit" name="action"    value="setstatus" >
          <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Cestina">
          </button>
          </form>
          </div>
          </td>	
				  </tr>
        <?php
			  } // list articles
        ?>
        </table>
        </div>
      </td>
      </tr>
      <?php
      } // show issue
     } // hack tavole
  } // list issues
} // list issue status	
?>
</table>
</div>
<?php
/*** INCLUDE MENU ARTICLES ***/
include(EOS_BASEPATH.'core/backend/eos_show_articles_menu.php'); 
/*****************************/
?>

