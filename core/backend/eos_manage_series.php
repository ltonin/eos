<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized("Magister Ludi") == false) 
	header('location:'.EOS_BASEURL);

$message = new Message();
$selfurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=31';

/** Manage post action **/
$action = "";
if(isset($_POST['action']))
	$action = $_POST['action'];

switch($action) {
	case "position":
		
		$step = $_POST['setposition'];
		$series = new IssueSeries();
		$series->Retrieve($_POST['seriesid']);
		$refseries = &$series;

		if(!eos_series_setposition($refseries, $step))
			$message->AddMessage("Posizione non modificata", MessageType::AsWarning);
		else
			$message->AddMessage("Posizione modificata ('"
					      .$series->Get('label')."'): "
					      .$series->Get('position'), MessageType::AsInfo);
		break;

	/** Store section action **/
	case "store":
		$series = new IssueSeries();
		if(isset($_POST['seriesid']) && empty($_POST['seriesid']) == false)
			$series->Set('id', $_POST['seriesid']);

		$series->Set('label',    $_POST['slabel']);
		$series->Set('special',  $_POST['sspecial']);
		$series->Set('imgurl',   $_POST['simgurl']);
	  $series->Set('position', $_POST['position']);
		
		if($series->Store() == false) {
			$message->AddMessage("Cannot store series in database", MessageType::AsDebug);
			die();
		}
		$message->AddMessage("Serie salvata ('".$series->Get('label')."')", MessageType::AsInfo);
		break;
	/** Delete section action **/
	case "delete":
		if(isset($_POST['seriesid']))
			$seriesid = $_POST['seriesid'];
		
		$series = new IssueSeries();
		$series->Retrieve($seriesid);
		$series->Drop();
		
		$table = new Table('issue_series');
		$table->SetOrder('position');
		$table->Select('id');
		$table->Get('id', $listseries);

		$pid = 1;
		foreach($listseries as $cseriesid) {
			$cseries = new IssueSeries();
			$cseries->Retrieve($cseriesid);
			$cseries->Set('position', $pid++);
			$cseries->Store();
		}
		$message->AddMessage("Serie eliminata ('".$series->Get('label')."')", MessageType::AsInfo);
		break;
}

// Get list of all attributes needed
$table = new Table('issue_series');
$table->SetOrder('position');
$table->Select('id');
$table->Get('id', $listseries);
$table->Select('MAX(`position`) AS maxpos');
$table->Get('maxpos', $maxposition, 0);
?>
<div id="backend_show">
<div class="title">Gestione serie</div>
<?php $message->Show(); ?>
<table>
<tr>
	<th>Nome Serie</th>
	<th>Speciale</th>
	<th>Url immagine</th>
   	<th>Posizione</th>
	<th>Strumenti</th>
</tr>
<?php 
	foreach($listseries as $seriesid) {
		$series = new IssueSeries;
		$series->Retrieve($seriesid);
?>
<tr>
	<form action="<?php print $selfurl; ?>" method="post" id="<?php print 'form'.$series->Get('id'); ?>">
	<td><input name="slabel"    value="<?php print $series->Get("label"); ?>" required></td>	
	<td><input name="sspecial"  value="<?php print $series->Get("special"); ?>"></td>
  <td>
  <script>
      function openPopup<?php print $series->Get("id"); ?>() {
        CKFinder.popup( { resourceType: "Immagini", chooseFiles: true, 
            onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
            var file = evt.data.files.first();
            document.getElementById( 'simgurl-<?php print $series->Get("id"); ?>' ).value = file.getUrl();
            } );
        finder.on( 'file:choose:resizedImage', function( evt ) {
            document.getElementById( 'simgurl-<?php print $series->Get("id"); ?>' ).value = evt.data.resizedUrl;
            } );
        }
      } );
    }
     </script>

  <div style="float:left">
  <input name="simgurl" id="simgurl-<?php print $series->Get("id"); ?>" value="<?php print $series->Get("imgurl"); ?>">
  </div>
  <div style="float:left; margin-left:5px;">
     <button type="button" id="popup-<?php print $series->Get("id"); ?>" onclick="openPopup<?php print $series->Get("id"); ?>()">Cerca</button>
  </div>
</td>
	<input type="hidden" name="seriesid" value="<?php print $series->Get("id"); ?>">
	<input type="hidden" name="position" value="<?php print $series->Get("position"); ?>">
	</form>
	
   	<form action="<?php print $selfurl; ?>" method="post">
   	<td>
   	<input type="hidden" name="action" value="position">
   	<input type="hidden" name="seriesid" value="<?php print $series->Get('id'); ?>">
   	<button type="submit" name="setposition" value=-1>
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-up.png'; ?>" width="24" title="Su">
    </button>
   	<button type="submit" name="setposition" value=1 >
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-down.png'; ?>" width="24" title="Giù">
    </button>
   	</td>
   	</form>

	<td>
	<button form="<?php print 'form'.$series->Get('id'); ?>" type="submit" name="action" value="store">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-save.png'; ?>" width="24" title="Salva">
</button>
	<button form="<?php print 'form'.$series->Get('id'); ?>" type="submit" name="action" value="delete"
	   onclick="return confirm('La serie verr\u00E0 cancellata definitivamente. Confermi l\'eliminazione?')">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Elimina">
   	</button>
	</td>
</tr>
<?php
	}
?>
<tr>
	<form action="<?php print $selfurl; ?>" method="post">
	<td><input name="slabel"    value="" required></td>	
	<td><input name="sspecial"  value=""></td>
	<td><input name="simgurl"   value=""></td>
	<td></td>
	<td>
   	<input type="hidden" name="position" value="<?php print $maxposition+1; ?>">
	<button type="submit" name="action" value="store">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-save.png'; ?>" width="24" title="Salva">
</button>
	</td>
	</form>
</tr>
</table>
</div>
