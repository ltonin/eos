<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();

// Define urls
$urlarea   = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlmanage = $urlarea.'?tool=11';
$urlview   = $urlarea.'?tool=12';
$urlself   = $urlarea.'?tool=14';

// Retrieve information about requested type
$type = new ArticleType();

if(isset($_GET['typeid'])) {
	$typeid = $_GET['typeid'];
	$type->Retrieve($typeid);
} else if(isset($_POST['typeid'])) {
	$typeid = $_POST['typeid'];
	$type->Retrieve($typeid);
}

// Management of actions
$action = "";
if(isset($_GET['action'])) {
	$action = $_GET['action'];
} else if(isset($_POST['action'])) {
	$action = $_POST['action'];
}

if(empty($action) == false)
	$articleid = $_POST['articleid'];

switch($action) {
	case "setstatus":
		$article = new Article();
		$article->Retrieve($articleid);
		$articlestatus = new ArticleStatus();
		$articlestatus->Retrieve($_POST['statusid']);
		$refarticle = &$article;	
		$refstatus  = &$articlestatus;	
		
		if(!eos_article_setstatus($refarticle, $refstatus))
			$message->AddMessage("Status not set", MessageType::AsWarning);
		else
			$message->AddMessage("Stato articolo modificato ('"
				     .$article->Get('title')."'): '"
			             .$article->Get('status::label')."'", MessageType::AsInfo);
		break;
	case "setposition":
		$article = new Article();
		$article->Retrieve($articleid);
		$refarticle = &$article;
		if(!eos_article_setposition($refarticle, $_POST['step']))
			$message->AddMessage("Position not set", MessageType::AsWarning);
		else
			$message->AddMessage("Posizione articolo modificata ('"
				     .$article->Get('title')."'): '"
				     .$article->Get('position')."'", MessageType::AsInfo);
		break;
	default:
		break;
}

// Retrieve id of trash status
$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve id of article type
$table = new Table('tb_tipo');
$table->SetCondition('nome', 'Articolo', ElementType::AsString);
$table->Select('id');
$table->Get('id', $articletypeid, 0);

// Retrieve all the possible status for articles (except 'trash')
$table->SetTable('article_status');
$table->SetCondition('id', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->Select('id');
$table->Get('id', $liststatus);

// Retrieve all articles associated to this type (except Article type)
$table->SetTable('tb_articolo');
$table->SetCondition('tipo', $type->Get('id'), ElementType::AsInteger);
$table->SetCondition('tipo', $articletypeid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->SetOrder('categoria');
$table->SetOrder('stato');
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listarticles);
?>
<div id="backend_show">
<div class="title"><?php print $type->Get('label'); ?></div>
<?php $message->Show(); ?>

<table>
<tr>
 <th>Numero</th>
 <th width=50%>Titolo</th>
 <th>Autore</th>
 <th>Ultima modifica</th>
 <th>Categoria</th>
 <th>Stato</th>
 <th>Posizione</th>
 <th>Strumenti</th>
</tr>

<!-- Iterate along articles belonging to this type -->
<?php
foreach($listarticles as $carticleid) {
	$article = new Article();
	$article->Retrieve($carticleid);
?>
<tr>

 <!-- General information -->
 <td>
 <?php print $article->Get("issue::number"); ?>
 </td>
 <td>
 <a href="<?php print $urlview."&articleid=".$article->Get("id"); ?>">
 <?php print $article->Get("title"); ?>
 </a>
 </td>
 <td><?php print $article->Get("user::nickname"); ?></td>
 <td><?php print $article->Get("modifiedby::nickname")."(".$article->Get("modified").")"; ?></td>
 <td><?php print $article->Get("section::label"); ?></td>

 <!-- Status selection -->
 <td>
 <form action="<?php print $urlself; ?>" method="post">
  <select name="statusid" onchange="this.form.submit()">
  <?php
  reset($liststatus);
  foreach($liststatus as $cstatusid) {
  	$status = new ArticleStatus();
  	$status->Retrieve($cstatusid);
  	$selected = "";
  	if($status->Get("id") == $article->Get("status::id"))
  		$selected = "selected";
?>
   <option value="<?php print $status->Get("id"); ?>" <?php print $selected; ?>>
	<?php print $status->Get("label"); ?>
   </option>
<?php
  }
?>
  </select>
  <input type="hidden" name="typeid" value="<?php print $type->Get("id"); ?>">
  <input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
  <input type="hidden" name="action" value="setstatus">
</form>

 <!-- Position selection -->
 <td>
<?php //print $article->Get("position"); ?>
 <form action="<?php print $urlself; ?>" method="post">
  <input type="hidden" name="action" value="setposition">
  <input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
  <input type="hidden" name="typeid" value="<?php print $type->Get("id"); ?>">
  <button type="submit" name="step" value=-1>Su</button>
  <button type="submit" name="step" value=1 >Giù</button>
</form>
</td>

 <!-- Modify/Trash buttons -->
 <td>
  <div class="toolbar">
  <form id="fmodify" action="<?php print $urlmanage; ?>" method="post">
    <input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
    <div><button form="fmodify" type="submit" name="action" value="modify" >Modifica</button></div>
  </form>
  <form id="ftrash" action="<?php print $urlself; ?>" method="post">
    <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
    <input  type="hidden" name="typeid" value="<?php print $type->Get("id"); ?>">
    <input  type="hidden" name="statusid"  value="<?php print $trashid; ?>">
    <div><button form="ftrash" type="submit" name="action"    value="setstatus" >Cestina</button></div>
  </form>
  </div>
</td>
</tr>
<?php
}
?>
</table>
</div>
<?php
/*** INCLUDE MENU ARTICLES ***/
include(EOS_BASEPATH.'core/backend/eos_show_articles_menu.php'); 
/*****************************/
?>


