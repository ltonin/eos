<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);


$message = new Message();
$selfurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=51';

/** Initialize variables **/
$targetid = "";
$action   = "";
$store    = "";

/** Get action required **/
if(isset($_GET['action'])) 
	$action = $_GET['action'];

/** Get store request **/
if(isset($_POST['store'])) 
	$store = $_POST['store'];

/** Get target user id **/
if(isset($_GET['userid'])) 
	$targetid = $_GET['userid'];
elseif(isset($_POST['userid'])) 
	$targetid = $_POST['userid'];
elseif($action === "profile")
	$targetid = $user->Get("id");


/** Create a new User object and retrieve information by id**/
$target = new User();
if(empty($targetid) == false)
	$target->Retrieve($targetid);

switch($action) {
	/** New user visualization **/
	case "new":
		$message->AddMessage("Nuovo utente", MessageType::AsInfo);
		break;
	/** Modify user visualization **/
	case "modify":
		$message->AddMessage("Modifica utente: ".$target->Get("nickname"), MessageType::AsInfo);
		break;
	case "profile":
		$message->AddMessage("Modifica profilo: ".$target->Get("nickname"), MessageType::AsInfo);
		break;
	case "":
		break;
	default:
		$message->AddMessage("Unknown action required (action=".$action.")", MessageType::AsError);
		break;
}

switch($store) {
	
	/** Store new user into db **/
	case "new":
		// Set common fields
		$target->Set('nickname',  $_POST['nickname']);
		$target->Set('firstname', $_POST['firstname']);
		$target->Set('lastname',  $_POST['lastname']);
		$target->Set('email',     $_POST['email']);

		// Set user type
		$type = new UserType();
		$type->Retrieve($_POST['usertype']);
		$target->Set('type', $type);

		// Check if the user nickname already exists	
		if($target->Exist()) {
			$message->AddMessage("L'utente esiste già", MessageType::AsError);
			$message->AddMessage("Errore nella compilazione dei campi", MessageType::AsError);
			break;
		}

		// Check if the passwords is correct
		$password  = $_POST['password'];
		$cpassword = $_POST['cpassword'];
		if($password === $cpassword) {
			$target->Set('password', md5($password));
		} else {
			$message->AddMessage("Le password non corrispondono", MessageType::AsError);
			$message->AddMessage("Errore nella compilazione dei campi", MessageType::AsError);
			break;
		}

		// Store user into db
		if($target->Store() == true) {
			$targetid = $target->Get("id");
			$message->AddMessage("Utente inserito (".$targetid.")", MessageType::AsInfo);
		} else {
			$message->AddMessage("Cannot store user in database", MessageType::AsError);
		}

		// Set next action to modify
		$action="modify";
		break;

	/** Modify existing user **/
	case "profile":
	case "modify":

		// Set common fields
		$target->Set('firstname', $_POST['firstname']);
		$target->Set('lastname',  $_POST['lastname']);
		$target->Set('email',     $_POST['email']);
		
		// Set user type
		$type = new UserType();
		$type->Retrieve($_POST['usertype']);
		$target->Set('type', $type);
		
		// Check if the password is correct and not empty
		$password  = "";
		$cpassword = "";
		if(isset($_POST['password']))
			$password  = $_POST['password'];
		if(isset($_POST['cpassword']))
			$cpassword = $_POST['cpassword'];

		if(empty($password) == false && empty($cpassword) == false) {
			if($password === $cpassword) {
				$target->Set('password', md5($password));
			} else {
				$message->AddMessage("Le password non corrispondono", MessageType::AsError);
				$message->AddMessage("Errore nella compilazione dei campi", MessageType::AsError);
				break;
			}
		}
	
		// Store user into db
		if($target->Store() == true) {
			$targetid = $target->Get("id");
			$message->AddMessage("Utente salvato (".$targetid.")", MessageType::AsInfo);
		} else {
			$message->AddMessage("Cannot store user in database", MessageType::AsError);
		}

		// Set next action to modify
		$action=$store;
		break;


	default:
		break;
}

$table = new Table('user_type');
$table->Select('id');
$table->Get('id', $listtypes);
?>
<div id="backend_manage">
<div class="title">Gestione utente</div>
<?php $message->Show(); ?>
<div style="font-size:12px; margin-botton:10px;">* Campo obbligatorio</div>
<form action="<?php print $selfurl; ?>" method="post">
<table>

<?php
	// Show the field password only if a new user is created
	if($action === "new") {
?>
<tr>
	<td width=15%>
	<label for="nickname">Nickname: (*)</label>
	</td>
	<td>
	<input name="nickname" id="nickname" size="12" value="<?php print $target->Get("nickname"); ?>" required>
	</td>
</tr>
<?php
	}
?>
<?php
	// Show the field password only if a new user is created or if the user
	// is modifying his own profile
	if($action === "profile" || $action === "new") {
?>
<tr>
	<td width=15%>
	<label for="password">Password: <?php if($action === "new") { print "(*)"; } ?></label>
	</td>
	<td>
	<input type="password" name="password" id="password" size="12" value="" <?php if($action === "new") { print " required"; } ?>>
	</td>
</tr>
<tr>
	<td>
	<label for="password">Conferma password: <?php if($action === "new") { print "(*)"; } ?></label>
	</td>
	<td>
	<input type="password" name="cpassword" id="cpassword" size="12" value="" <?php if($action === "new") { print " required"; } ?>>
	</td>
</tr>

<?php
	}
?>

<tr>
	<td width=15%>
	<label for="email">E-mail:</label>
	</td>
	<td>
	<input name="email" id="email" size="12" value="<?php print $target->Get("email"); ?>">
	</td>
</tr>
<tr>
	<td>
	<label for="firstname">Nome:</label>
	</td>
	<td>
	<input name="firstname" id="firstname" size="2" value="<?php print $target->Get("firstname"); ?>">
	</td>
</tr>
<tr>
	<td>
	<label for="lastname">Cognome:</label>
	</td>
	<td>
	<input name="lastname" id="lastname" size="37" value="<?php print $target->Get("lastname"); ?>">
	</td>
</tr>
<tr>
	<td>
	<label for="usertype">Tipologia:</label>
	</td>
	<td>
<?php 
	if($user->IsAuthorized("Magister Ludi")) { 
?>
	<select name="usertype" id="usertype">
<?php
	foreach($listtypes as $typeid) {
		$type = new UserType();
		$type->Retrieve($typeid);
?>
		<option value="<?php print $type->Get('id'); ?>" 
		<?php if($target->Get("type::id") == $type->Get('id')) { print " selected"; } ?>>
		<?php print $type->Get('label'); ?></option>
<?php
	}
?>
	</select>
<?php
	} else {
?>
	<input type="hidden" name="usertype" value="<?php print $target->Get("type::id"); ?>">
	<?php print $target->Get("type::label"); ?>
<?php
	}
?>
	</td>
</tr>
<tr>
	<td><label>Creato:</label></td>
	<td><?php print $target->Get("created"); ?></td>
</tr>
<tr>
	<td><label>Ultima modifica:</label></td>
	<td><?php print $target->Get("modified"); ?></td>
</tr>
<tr>
	<td></td>
	<td></td>
</tr>

</table>

<div align="left; margin-top:5px">
<input type="hidden" name="userid" value="<?php print $targetid; ?>">
<button type="submit" name="store" value="<?php print $action; ?>">Salva</button>
</div>
</form>
</div>
