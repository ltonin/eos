<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();

// Define urls
$urlarea   = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlmanage = $urlarea.'?tool=11';
$urlview   = $urlarea.'?tool=12';
$urlself   = $urlarea.'?tool=13';

// By default visualize the published issue OR the one requested
$issue = new Issue();
$issue->RetrieveBy('numero', ElementType::AsInteger, $_SESSION['issuecurrent']);
$issueid = $issue->Get('id'); 

if(isset($_GET['issueid'])) {
	$issueid = $_GET['issueid'];
	$issue->Retrieve($issueid);
} else if(isset($_POST['issueid'])) {
	$issueid = $_POST['issueid'];
	$issue->Retrieve($issueid);
}

// Management of actions
$action = "";
if(isset($_GET['action'])) {
	$action = $_GET['action'];
} else if(isset($_POST['action'])) {
	$action = $_POST['action'];
}
if(empty($action) == false) {
	$articleid = $_POST['articleid'];
}

switch($action) {
	case "setstatus":
		$article = new Article();
		$article->Retrieve($articleid);
		$articlestatus = new ArticleStatus();
		$articlestatus->Retrieve($_POST['statusid']);
		$refarticle = &$article;	
		$refstatus  = &$articlestatus;	
		
		if(!eos_article_setstatus($refarticle, $refstatus))
			$message->AddMessage("Stato non modificato", MessageType::AsWarning);
		else
			$message->AddMessage("Stato articolo modificato ('"
				     .$article->Get('title')."'): '"
			             .$article->Get('status::label')."'", MessageType::AsInfo);
		break;
	case "setposition":
		$article = new Article();
		$article->Retrieve($articleid);
		$refarticle = &$article;
		if(!eos_article_setposition($refarticle, $_POST['step']))
			$message->AddMessage("Posizione non modificata", MessageType::AsWarning);
		else
			$message->AddMessage("Posizione articolo modificata ('"
				     .$article->Get('title')."'): '"
				     .$article->Get('position')."'", MessageType::AsInfo);
		break;
	default:
		break;
}

// Retrieve id of trash status
$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve all the possible status for articles (except 'trash')
$table->SetTable('article_status');
$table->SetCondition('id', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->Select('id');
$table->Get('id', $liststatus);

// Retrieve all articles associated to this issue
$table->SetTable('tb_articolo');
$table->SetCondition('id_numero', $issueid, ElementType::AsInteger);
$table->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listarticles);

// Retrieve all issues of with the same status
$table->SetTable('tb_numero');
$table->SetCondition('stato', $issue->Get('status::id'), ElementType::AsInteger);
$table->SetOrder('numero');
$table->Select('id');
$table->Get('id', $listissues);
?>
<div id="backend_show">
<div class="title"><?php print $issue->Get('status::label'); ?></div>
<?php $message->Show(); ?>
<?php 
if($issue->Get('status::label') != 'Pubblicato') {
?>
<form action="<?php print $urlself; ?>" method="post">
 <label for="issueid">Visualizza numero:</label>
 <select id="issueid" name="issueid" onchange="this.form.submit()">
<?php
 foreach($listissues as $cissueid) {
 	$issue = new Issue();
 	$issue->Retrieve($cissueid);
 	$selected = "";
 	if($issue->Get("id") == $issueid) {
 		$selected = "selected";
 	}
?>
  <option value="<?php print $issue->Get("id"); ?>" <?php print $selected; ?>>
  <?php print $issue->Get("number"); ?>
  </option>
<?php
}
?>
  </select>
</form>

<?php
}
?>
<table>
<tr>
 <th>Numero</th>
 <th width=50%>Titolo</th>
 <th>Autore</th>
 <th>Ultima modifica</th>
 <th>Categoria</th>
 <th>Stato</th>
 <th>Posizione</th>
 <th>Strumenti</th>
</tr>

<!-- Iterate along articles belonging to this issue -->
<?php                                
foreach($listarticles as $carticleid) {
	$article = new Article();    
	$article->Retrieve($carticleid);
?>
<tr>

 <!-- General information -->
 <td>
 <?php print $article->Get("issue::number"); ?>
 </td>
 <td>
 <a href="<?php print $urlview."&articleid=".$article->Get("id"); ?>">
 <?php print $article->Get("title"); ?>
 </a>
 </td>
 <td><?php print $article->Get("user::nickname"); ?></td>
 <td><?php print $article->Get("modifiedby::nickname")."(".$article->Get("modified").")"; ?></td>
 <td><?php print $article->Get("section::label"); ?></td>

 <!-- Status selection -->
 <td>
 <form action="<?php print $urlself; ?>" method="post">
  <select name="statusid" onchange="this.form.submit()">
  <?php
  reset($liststatus);
  foreach($liststatus as $cstatusid) {
  	$status = new ArticleStatus();
  	$status->Retrieve($cstatusid);
  	$selected = "";
  	if($status->Get("id") == $article->Get("status::id"))
  		$selected = "selected";
?>
   <option value="<?php print $status->Get("id"); ?>" <?php print $selected; ?>>
	<?php print $status->Get("label"); ?>
   </option>
<?php
  }
?>
  </select>
  <input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
  <input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
  <input type="hidden" name="action" value="setstatus">
</form>

 <!-- Position selection -->
 <td>
<?php //print $article->Get("position"); ?>
 <form action="<?php print $urlself; ?>" method="post">
  <input type="hidden" name="action" value="setposition">
  <input type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
  <input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
  <button type="submit" name="step" value=-1>Su</button>
  <button type="submit" name="step" value=1 >Giù</button>
</form>
</td>

 <!-- Modify/Trash buttons -->
 <td>
  <div class="toolbar">
  <form  action="<?php print $urlmanage; ?>" method="post">
    <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
    <div><button type="submit" name="action"    value="modify" >Modifica</button></div>
  </form>
  <form  action="<?php print $urlself; ?>" method="post">
    <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
    <input  type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
    <input  type="hidden" name="statusid"  value="<?php print $trashid; ?>">
    <div><button type="submit" name="action"    value="setstatus" >Cestina</button></div>
  </form>
  </div>
</td>
</tr>
<?php
}
?>
</table>
</div>
<?php
/*** INCLUDE MENU ARTICLES ***/
include(EOS_BASEPATH.'core/backend/eos_show_articles_menu.php'); 
/*****************************/
?>

