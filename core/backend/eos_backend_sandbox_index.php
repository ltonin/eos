<?php
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
eos_session_start();

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

?>

<html>
<?php
/*************** HTML HEAD ***************/
include(EOS_BASEPATH."core/frontend/eos_headhtml.php");
/**************************************/

?>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles_minicolors.php'; ?>" media="screen"></link>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_minicolors.js'; ?>" type="text/javascript"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_javascript_support.js'; ?>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php print EOS_CSS_BASEURL.'eos_styles_backend.php'; ?>">
<link rel="stylesheet" type="text/css" href="<?php print EOS_CSS_BASEURL.'eos_styles_form-eos3.css.php'; ?>">

<script type="text/javascript" src="<?php print EOS_EXTRA_BASEURL.'ckfinder/eos_ckfinder_functions.js'; ?>"></script>
<body>
<div id="top">
<?php
/******** PAGE HEADER  *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
</div>
<div id="backend_left">
<?php
/******** PAGE LEFT PANEL *******/
include(EOS_BASEPATH."core/backend/eos_backend_left.php");
/**************************************/
?>
</div>
<div id="backend">

<?php
include(EOS_BASEPATH."core/backend/eos_backend_sandbox_doi.php");
?>

</div>

<?php
/******** PAGE BOTTOM PANEL *******/
include(EOS_BASEPATH."core/backend/eos_backend_bottom.php");
/**************************************/
?>

</div> 
</body>
</html>
