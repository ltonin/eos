<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false)
	header('location:'.EOS_BASEURL);

$message   = new Message();
$selfurl   = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=11';
$parenturl = EOS_BASEURL.'core/backend/eos_backend_index.php';

/** Manage post action **/
if(isset($_GET['action'])) 
	$action = $_GET['action'];
elseif(isset($_POST['action']))
	$action = $_POST['action'];
else
	trigger_error("unknown action");

switch($action) {
	/** New article visualization **/
	case "new":
		$article = new Article();
		$articleid = "";
		$user = new User();
		$user->Retrieve($_SESSION['userid']);
		$issue = new Issue();
		$issue->RetrieveBy('stato', ElementType::AsInteger,  1); // To be change with Issue table class, (1=>Pubblicato);
		
		$article->Set("issue", $issue); 
		$article->Set("user", $user); 
		$article->Set("modifiedby", $user); 
		$nextaction = "store";
		$message->AddMessage("Nuovo articolo", MessageType::AsInfo);
		break;
	/** Modify article visualization **/
	case "modify":
		if(isset($_GET['articleid'])) {
			$articleid = $_GET['articleid'];
		} elseif(isset($_POST['articleid'])) {
			$articleid = $_POST['articleid'];
		} else {
			$message->AddMessage("Undefined article id", MessageType::AsDebug);
		}
		//print $articleid;
		$article = new Article();
		$article->Retrieve($articleid);
		$nextaction = "store";
		$message->AddMessage("Modifica articolo ('".$article->Get('id')."')", MessageType::AsInfo);
		break;
	/** Store article action **/
	case "store":
		if(isset($_POST['articleid'])) {
			$articleid = $_POST['articleid'];
		} else {
			$message->AddMessage("Undefined article id", MessageType::AsDebug);
		}


		$article = new Article();
		if(empty($articleid) == false) {
			if($article->Retrieve($articleid) == false) {
				$message->AddMessage("Cannot retrieve article with id:".$articleid, MessageType::AsDebug);
				die();
			}
		}

		$type = new ArticleType();
		$type->Retrieve($_POST['typeid']);

		$section = new ArticleSection();
		if(isset($_POST['sectionid']))
			$section->Retrieve($_POST['sectionid']);
		
		$issue = new Issue();
		if(isset($_POST['issueid']))
			$issue->Retrieve($_POST['issueid']);
		
		$status = new ArticleStatus();
		$status->Retrieve($_POST['statusid']);
		
		$author = new User();
		$author->Retrieve($_POST['authorid']);

		// Check if the user exists, otherwise use magistermagistorum user
		// (id=10)
		if( empty($author->Get('nickname')) == true) {
			$author->Retrieve(10);
		}
		
		$modifiedby = new User();
		$modifiedby->Retrieve($_SESSION['userid']);

		$title = $_POST['title'];
		$text = $_POST['text'];
		
		$article->Set("type", 		$type); 
		$article->Set("section", 	$section); 
		$article->Set("issue", 		$issue); 
		$article->Set("status", 	$status); 
		$article->Set("user", 		$author); 
		$article->Set("modifiedby", $modifiedby); 
		$article->Set("title", 		$title); 
		$article->Set("text", 		$text); 
		
		if(empty($title) == true) {
			$message->AddMessage("Prima di salvare inserisci il titolo", MessageType::AsWarning);
			$nextaction = "store";
			break;
		}

		if(isset($_POST['position'])) {
			$position = $_POST['position'];
		} else {
			$table = new Table('tb_articolo');
			$table->SetCondition('tipo', $article->Get('type::id'), ElementType::AsInteger);
			$table->SetCondition('stato', $article->Get('status::id'), ElementType::AsInteger);
			$table->SetCondition('id_numero', $article->Get('issue::id'), ElementType::AsInteger);
			$table->SetCondition('categoria', $article->Get('section::id'), ElementType::AsInteger);
			$table->Select('MAX(`pos`) AS maxpos');
			$table->Get('maxpos', $maxposition, 0);
			$position = $maxposition+1;
		}
		$article->Set("position", $position);
			

		if($article->Store() == false) {
			$message->AddMessage("Cannot store article in database", MessageType::AsDebug);
			die();
		}
		
		$articleid = $article->Get("id");	
		$article->Retrieve($articleid);
		$nextaction = "store";
		$message->AddMessage("Articolo salvato (".$article->Get("id").")", MessageType::AsInfo);
		break;
	case "close":
		eos_redirect($parenturl);
		break;
}


// Get list of all attributes needed
$table = new Table('tb_tipo');
$table->Select('id');
$table->Get('id', $listtype);

$table->SetTable('tb_categoria');
$table->Select('id');
$table->Get('id', $listsection);

$table->SetTable('tb_numero');
$table->Select('id');
$table->Get('id', $listissue);

$table->SetTable('article_status');
$table->Select('id');
$table->Get('id', $liststatus);

?>

<div id="backend_manage">
<div class="title">Nuovo articolo</div>
<?php $message->Show(); ?>
<form id="farticle" action="<?php print $selfurl ?>" method="post">
<table>
<tr>
	<td width=20%><label for="typeid">Tipo:</label></td>
	<td>
		<select name="typeid" id="typeid" form="farticle" >
<?php
		foreach($listtype as $typeid) {
			$type = new ArticleType();
			$type->Retrieve($typeid)
?>
			<option value=<?php print $type->Get('id'); ?>
			<?php if($type->Get('id')==$article->Get("type::id")) {?>selected<?php } ?>>
			<?php print $type->Get('label'); ?>
			</option>	
<?php
		}
?>
		</select>
	</td>
</tr>
<tr>
	<td><label for="sectionid">Categoria:</label></td>
	<td>
	<select name="sectionid" id="sectionid" form="farticle">
<?php
		foreach($listsection as $sectionid) {
			$section = new ArticleSection();
			$section->Retrieve($sectionid);
?>
			<option value=<?php print $section->Get('id'); ?>
			<?php if($section->Get('id')==$article->Get("section::id")) {?>selected<?php } ?>>
			<?php print $section->Get('label'); ?>
			</option>	
<?php
		}
?>
		</select>
	</td>
</tr>
<tr>
	<td><label for="typeid">Numero:</label></td>
	<td>
		<select name="issueid" id="issueid" form="farticle">
<?php
		foreach($listissue as $issueid) {
			$issue = new Issue();
			$issue->Retrieve($issueid);
?>
			<option value=<?php print $issue->Get('id'); ?>
			<?php if($issue->Get('id')==$article->Get("issue::id")) {?>selected<?php } ?>>
			<?php print $issue->Get('number'); ?>
			</option>	
<?php
		}
?>
		</select>
	</td>
</tr>
<tr>
	<td><label for="statusid">Stato:</label></td>
	<td>
		<select name="statusid" id="statusid" form="farticle">
<?php
		foreach($liststatus as $statusid) {
			$status = new ArticleStatus();
			$status->Retrieve($statusid);
?>
			<option value=<?php print $status->Get('id'); ?>
			<?php if($status->Get('id')==$article->Get("status::id")) {?>selected<?php } ?>>
			<?php print $status->Get('label'); ?>
			</option>	
<?php
		}
?>
		</select>
	</td>
</tr>
<tr>
	<td><label for="authorid">Creatore:</label></td>
	<td><?php print $article->Get("user::nickname"); ?></td>
	<input type="hidden" name="authorid" id="authorid" form="farticle" value="<?php print $article->Get("user::id"); ?>">
</tr>
<tr>
	<td><label for="modifiedbyid">Ultima modifica:</label></td>
	<td><?php print $article->Get("modifiedby::nickname"); ?> (in data: <?php print $article->Get("modified"); ?>)</td>
	<input type="hidden" name="modifiedbyid" id="modifiedbyid" form="farticle" value="<?php print $article->Get("modifiedby::id"); ?>">
</tr>
<tr>
	<td><label for="title">Titolo:</label></td>
	<td>
	<input name="title" id="test" form="farticle" value="<?php print $article->Get("title"); ?>" required>
	</td>
</tr>
</table>
</form>
		

<script type="text/javascript">

	// Get current article type
	var atype	= <?php echo(json_encode($article->Get("type::id"))); ?>;
	
	// Get menu element for type, section and issue	
	var mtype	 = document.getElementById("typeid");
	var msection = document.getElementById("sectionid");
	var missue	 = document.getElementById("issueid");

	// Disable all menu
	mtype.disabled		= false;
	msection.disabled	= true;
	missue.disabled		= true;

	// Default initialization according to article type
	switch(atype) {
		case 1:		// Type: Articolo
			msection.disabled	= true;
			missue.disabled		= false;
			break;
		case 2:		// Type: Istituzionale
			msection.disabled	= false;
			missue.disabled		= true;
			break;
		case 3:		// Type: Copertina
			msection.disabled	= true;
			missue.disabled		= false;
			break;
		default:
			msection.disabled	= true;
			missue.disabled		= false;
			break;
	}

	// OnChange initialization according to the selection
	mtype.onchange = function() {
		
		// Get current selection
		var selected = mtype.options[mtype.selectedIndex]

		switch (selected.value) {
			case "1":		// Type: Articolo
			   msection.disabled = true;
			   missue.disabled   = false;
			   break;
			case "2":		// Type: Istituzionale
			   msection.disabled = false;
			   missue.disabled   = true;
			   break;
			case "3":		// Type: Copertina
			   msection.disabled = true;
			   missue.disabled   = false;
			   break;
			default:
			   msection.disabled = true;
			   missue.disabled   = false;
		}
	}	
</script>

<textarea form="farticle" id="text" name="text" cols=120 rows=100>
<?php print $article->Get("text"); ?>
</textarea>

<script type="text/javascript">
var path = {
	extra : '<?php print EOS_EXTRA_BASEURL; ?>'
}


var editor = CKEDITOR.replace( 'text', { 
	customConfig: '<?php print EOS_EXTRA_BASEURL."ckeditor/eos_ckeditor_configuration.js"; ?>',
	filebrowserImageBrowseUrl : '<?php print EOS_CKFINDER_BASEURL; ?>ckfinder.html?type=Immagini',
	filebrowserImageUploadUrl : '<?php print EOS_CKFINDER_BASEURL; ?>core/connector/php/connector.php?command=QuickUpload&type=Immagini',
});
CKFinder.setupCKEditor(editor);
</script>

<!-- This hidden field is only to enable the 'Save' button in the ckeditor toolbar -->
<input  form="farticle" type="hidden" name="action" value="<?php print $nextaction ?>">
<input  form="farticle" type="hidden" name="articleid" value="<?php print $articleid; ?>">
<?php if($action == "modify") { ?>
<input  form="farticle" type="hidden" name="position" value="<?php print $article->Get("position"); ?>">
<?php } ?>
<div style="margin-top:15px;"> 
<button form="farticle" type="submit" name="action" value="<?php print $nextaction ?>">Salva</button>
<button form="farticle" type="submit" name="action" value="close">Chiudi</button>
<div>
</div>
