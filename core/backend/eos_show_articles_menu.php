<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) {
	$user->Retrieve($_SESSION['userid'] );
}

if($user->IsAuthorized() == false) {
	header('location:'.EOS_BASEURL);
}


$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

$table = new Table('tb_articolo');
$table->SetCondition('stato', $trashid, ElementType::AsInteger);
$table->Select('id');
$ntrash = $table->Size('id');

$urlarea  = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlissue = $urlarea."?tool=13";
$urltype  = $urlarea."?tool=14";
$urltrash = $urlarea."?tool=18";
?>
<div id="backend_menu">
<table>
<tr>
<td>
  <table>
  <tr>
  <?php
  $table = new Table('issue_status');
  $table->SetOrder('id');
  $table->Select('id');
  $table->Get('id', $statuslist);
  foreach($statuslist as $statusid) {
  	$status = new IssueStatus();
  	$status->Retrieve($statusid);
	$table->SetTable('tb_numero');
	$table->SetCondition('stato', $statusid, ElementType::AsInteger);
	$table->SetOrder('numero');
	$table->Select('id');
	$table->Get('id', $issueid, 0);
  	$clink = "<a href='".$urlissue."&issueid=".$issueid."'>".$status->Get('label')."</a>"
  ?>
  <td width="5%"><?php print $clink; ?></td>
  <?php 
  }
  ?>
  </tr>
  </table>
</td>
</tr>

<tr>
<td>
<table class="">
<tr>
<?php
  $table = new Table('tb_tipo');
  $table->SetCondition('nome', 'Articolo', ElementType::AsString, QueryCondition::IsDifferent);
  $table->Select('id');
  $table->Get('id', $typelist);
  foreach($typelist as $typeid) {
  	$type = new ArticleType();
  	$type->Retrieve($typeid);
  	$ctable = new Table('tb_articolo');
  	$ctable->SetCondition('tipo',  $typeid, ElementType::AsInteger);
  	$ctable->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
  	$ctable->Select('id');
  	$narticles = $ctable->Size('id');
  
  	$clink = "<a href='".$urltype."&typeid=".$type->Get('id')."'>".$type->Get('label')."(".$narticles.")</a>";
?>
<td width="5%"><?php print $clink; ?></td>

<?php 
}
?>
   <td width="5%">
   <a href="<?php print $urltrash; ?>">Cestino(<?php print $ntrash; ?>)</a>
   </td>
   </tr>	
   </table>
</td>
</table>
</td>
</tr>
</table>
</div>
