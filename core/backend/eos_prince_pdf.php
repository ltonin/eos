<?php
require_once(__DIR__."/../eos_configuration_init.php");

require_once(EOS_PRINCE_BASEPATH.'prince.php');

$http = "http://";   
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
	$http = "https://";
}	

$issue="";
if(isset($_POST['issue-number'])) {
	$issue = $_POST['issue-number'];
}

$type="";
if(isset($_POST['publication-type'])) {
	$type = $_POST['publication-type'];
}
header('Content-Type: application/pdf');
header('Content-Disposition: attachment; filename=engramma_'.$issue. '_' . $type . '.pdf');

$prince = new Prince('/usr/bin/prince');
$prince->setOptions("--verbose");
#$url= 'http://127.0.1.1/eOS/core/backend/eos_generate_pdf.php?issue=';
#$url= "https://" . $_SERVER['SERVER_NAME'] . EOS_BASEURL.'core/backend/eos_generate_pdf.php?issue=';
$url= $http . $_SERVER['SERVER_NAME'] . EOS_BASEURL.'core/backend/eos_backend_prince_generate_html.php';
$prince->setHTML(true);
$prince->setJavaScript(true);
$prince->setLog('/var/www/html/eOS/resources/eos_prince.log');

//$prince->convert_file_to_passthru($url.'?issue='.$issue.'&type='.$type);
$prince->convertFileToPassthru($url.'?issue='.$issue.'\&type='.$type);
 ?>
