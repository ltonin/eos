<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();

// Define urls
$urlarea   = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlview   = $urlarea.'?tool=12';
$urlself   = $urlarea.'?tool=18';

// By default visualize status 'cestino' OR (to be general) the requested status
$status = new ArticleStatus();
$status->RetrieveBy('label', ElementType::AsString, 'cestino');
$statusid = $status->Get('id'); 

if(isset($_GET['statusid'])) {
	$statusid = $_GET['statusid'];
	$status->Retrieve($statusid);
} else if(isset($_POST['statusid'])) {
	$statusid = $_POST['statusid'];
	$status->Retrieve($statusid);
}

// Management of actions
$action = "";
if(isset($_GET['action'])) {
	$action = $_GET['action'];
} else if(isset($_POST['action'])) {
	$action = $_POST['action'];
}

if(empty($action) == false)
	$articleid = $_POST['articleid'];

switch($action) {
	case "setstatus":
		$article = new Article();
		$article->Retrieve($articleid);
		$articlestatus = new ArticleStatus();
		$articlestatus->Retrieve($_POST['statusid']);
		$refarticle = &$article;	
		$refstatus  = &$articlestatus;	
		
		if(!eos_article_setstatus($refarticle, $refstatus))
			$message->AddMessage("Articolo non ripristinato", MessageType::AsWarning);
		else
			$message->AddMessage("Articolo ripristinato ('"
				     .$article->Get('title')."') in categoria '"
				     .$article->Get('section::label')."', tipo '"
				     .$article->Get('type::label')."', numero '"
				     .$article->Get('issue::number')."'", MessageType::AsInfo);
		break;
	case "delete":
		$article = new Article();
		$article->Retrieve($articleid);
		$article->Drop();
		$message->AddMessage("Articolo ('".$article->Get('title').
				     "') eliminato", MessageType::AsInfo);
		break;
	default:
		break;
}

// Retrieve id of trash status
$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve id of draft status
$table->SetTable('article_status');
$table->SetCondition('label', 'bozze', ElementType::AsString);
$table->Select('id');
$table->Get('id', $draftid, 0);

// Retrieve all articles associated to this status
$table->SetTable('tb_articolo');
$table->SetCondition('stato', $trashid, ElementType::AsInteger);
$table->SetOrder('categoria');
$table->SetOrder('id_numero');
$table->SetOrder('autore');
$table->Select('id');
$table->Get('id', $listarticles);

 
?>
<div id="backend_show">
<div class="title"><?php print ucwords($status->Get('label')); ?></div>
<?php $message->Show(); ?>

<table>
<tr>
 <th>Numero</th>
 <th>Titolo</th>
 <th>Autore</th>
 <th>Ultima modifica</th>
 <th>Categoria</th>
 <th>Strumenti</th>
</tr>

<!-- Iterate along articles belonging to this status -->
<?php
foreach($listarticles as $carticleid) {
	$article = new Article();
	$article->Retrieve($carticleid);
?>
<tr>

 <!-- General information -->
 <td><?php print $article->Get("issue::number"); ?></td>
 <td style="max-width:300px; word-wrap:break-word;">
 <a href="<?php print $urlview."&articleid=".$article->Get("id"); ?>">
 <?php print $article->Get("title"); ?>
 </a>
 </td>
 <td><?php print $article->Get("user::nickname"); ?></td>
 <td><?php print $article->Get("modified")." (".$article->Get("modifiedby::nickname").")"; ?></td>
 <td><?php print $article->Get("section::label"); ?></td>

 <!-- Restore/Delete buttons -->
 <td>
  <div class="toolbar">
    <form action="<?php print $urlself; ?>" method="post">
    <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
    <input  type="hidden" name="statusid" value="<?php print $draftid; ?>">
    <button type="submit" name="action" value="setstatus" >
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-restore.png'; ?>" width="24" title="Ripristina">
    </button>
    </form>
<?php

if($user->IsAuthorized("Magister Ludi")) { 
?>
    <form action="<?php print $urlself; ?>" method="post" 
       onsubmit="return confirm('L\'articolo verr\u00E0 cancellato definitivamente. Confermi l\'eliminazione?')">
    <input  type="hidden" name="articleid" value="<?php print $article->Get("id"); ?>">
    <button type="submit" name="action"    value="delete" >
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Elimina">
    </button>
  </form>
<?php
}
?>
  </div>
</td>
</tr>
<?php
}
?>
</table>
</div>
<?php
/*** INCLUDE MENU ARTICLES ***/
include(EOS_BASEPATH.'core/backend/eos_show_articles_menu.php'); 
/*****************************/
?>


