<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_book.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized("Magister Ludi") == false)
	header('location:'.EOS_BASEURL);

$message = new Message();
$selfurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=21';

/** Manage post action **/
$action      = "";
$storeaction = "";
$issueid     = "";

if(isset($_GET['action'])) 
	$action = $_GET['action'];
elseif(isset($_POST['action'])) 
	$action = $_POST['action'];

if(isset($_POST['issueid']))
	$issueid = $_POST['issueid'];

$issue = new Issue();
if(empty($issueid) == false)
	$issue->Retrieve($issueid);

$book = new Book();
if(empty($issueid) == false)
	$book->RetrieveByIssue($issueid);

switch($action) {
	/** New issue visualization **/
	case "new":
		$message->AddMessage("Nuovo numero", MessageType::AsInfo);
		break;
	/** Modify issue visualization **/
	case "modify":
		$message->AddMessage("Modifica numero ".$issue->Get('number'), MessageType::AsInfo);
		break;
	/** Store issue visualization **/
	case "store":

		$storeaction = "insert";
		if(empty($issueid) == false) 
			$storeaction = "update";
	
		$issue->Set('number', 	   $_POST['number']);
		$issue->Set('special', 	   $_POST['special']);
		$issue->Set('description', $_POST['description']);
		$issue->Set('authors',     $_POST['authors']);
		$issue->Set('extra',       $_POST['extra']);
		$issue->Set('isbn',        $_POST['isbn']);
		$issue->Set('img',         $_POST['cover']);
		$issue->Set('color',       "#".$_POST['color']);
		$issue->Set('altcolor',    "#".$_POST['altcolor']);
		$issue->Set('shade',       $_POST['shade']);
		
		$status = new IssueStatus();
		$status->Retrieve($_POST['status']);
		$issue->Set('status',      $status);

		$series = new IssueSeries();
		if(empty($_POST['series']) == false)
			$series->Retrieve($_POST['series']);
		$issue->Set('series', $series);

		$isbnbase = new IssueIsbn();
		if($_POST['isbn_base'] != "null") {
			if(empty($_POST['isbn_base']) == false)
				$isbnbase->Retrieve($_POST['isbn_base']);
			$issue->Set('isbn_base', $isbnbase);
		} 

		// Check if the number already exists
		if($storeaction === "insert" && $issue->Exist()) {
			$message->AddMessage("Il numero esiste già", MessageType::AsError);
			$message->AddMessage("Errore nella compilazione dei campi", MessageType::AsError);
			break;
		}

		
		if($issue->Store() == false) 
			$message->AddMessage("Cannot store issue in database", MessageType::AsDebug);
		
		$issueid = $issue->Get("id");
		$message->AddMessage("Numero salvato (".$issue->Get("number").")", MessageType::AsInfo);
		// If the number is new, create the image folder
		if($storeaction === "insert") {
			$issuefolder = EOS_IMAGES_BASEPATH . $_POST['number']; 
			if(!mkdir($issuefolder)) {
				$message->AddMessage("Il numero è stato creato, ma c'è stato un errore nella creazione della cartella immagine. Procedere manualmente.", MessageType::AsWarning);
				$message->AddMessage("Requested folder path: ".$issuefolder, MessageType::AsDebug);
			}
		}

		// Store issue bookshop joint table
		$book->Set('issue_id', $issueid);
		$book->Set('pdf_path', $_POST['pdfpath']);	
		$book->Set('ebook_path', $_POST['ebookpath']);	
		$book->Set('thumbnail', $_POST['thumbnail']);	
		
		if($book->Store() == false)
			$message->AddMessage("Cannot store book in database", MessageType::AsDebug);


		break;
}

// Get list of all attributes needed
$table = new Table('issue_status');
$table->Select('id');
$table->Get('id', $liststatus);

$table->SetTable('issue_series');
$table->SetOrder('position', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $listseries);

$table->SetTable('isbn_series');
$table->Select('id');
$table->Get('id', $listisbn);

/*
$pdfpath = '';
$ebookpath = '';
if (empty($issueid) == false) {
	$table2 = new Table('issue_bookshop');
	$table2->SetCondition('issue_id', $issueid, ElementType::AsInteger);
	$table2->Select('pdf_path');
	$nrows_pdf = $table2->Get('pdf_path', $pdfpath_array);
	$table2->Select('ebook_path');
        $nrows_ebook = $table2->Get('ebook_path', $ebookpath_array);
	
	if ($nrows_pdf == 1) {
		$pdfpath = $pdfpath_array[0];
	}
	
	if ($nrows_ebook == 1) {
		$ebookpath = $ebookpath_array[0];
	}
}
echo "pdf path: $pdfpath";
echo "ebook path: $ebookpath";
 */
print "Elements in isbn table: ".$table->Size("id")."(".count($listisbn).")";
?>
<div id="backend_manage">
<div class="title">Gestione numero</div>
<?php $message->Show(); ?>
<form action="<?php print $selfurl; ?>" method="post">
<table>
<tr>
	<td width=20%>
	<label for="number">Numero:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="number" id="number" size="37" value="<?php print $issue->Get("number"); ?>" 
	<?php if($action === "new" || $storeaction==="insert") { print " required"; } ?>>
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="special">Mese:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="special" id="special" size="37" value="<?php print $issue->Get("special"); ?>" >
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="description">Descrizione:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="description" id="description" size="37" value="<?php print $issue->Get("description"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="authors">Curatori:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="authors" id="authors" size="37" value="<?php print $issue->Get("authors"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="extra">Extra:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="extra" id="extra" size="37" value="<?php print $issue->Get("extra"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="isbn">ISBN:</label>
	</td>
	<td>
  <div style="float:left">
	<select name="isbn_base">
	<option value="null"></option>
	<?php 

	foreach($listisbn as $isbn_base_id) {
		$isbn_base = new IssueIsbn();
		$isbn_base->Retrieve($isbn_base_id);
		$selected = "";
		if(($action == "new") && ($isbn_base->Get("current"))) {
			$selected = "selected";
		} elseif(($action != "new") && ($issue->Get("isbn_base::id") == $isbn_base->Get("id"))) {
			$selected = "selected";
		}
	?>

	<option value="<?php print $isbn_base->Get("id"); ?>" <?php print $selected; ?>><?php print $isbn_base->Get("base"); ?></option>
	<?php
	}
	?>
	</select>
</div>
  <div style="float:left; margin-left:5px;">
	<input name="isbn" id="isbn" size="3" value="<?php print $issue->Get("isbn"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="cover">Immagine cover:</label>
	</td>
	<td>
  <script>
      function openPopup() {
        CKFinder.popup( { resourceType: "Immagini", chooseFiles: true, 
            onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
            var file = evt.data.files.first();
            document.getElementById( 'cover' ).value = file.getUrl();
            } );
        finder.on( 'file:choose:resizedImage', function( evt ) {
            document.getElementById( 'cover' ).value = evt.data.resizedUrl;
            } );
        }
      } );
    }
     </script>
  <div style="float:left">
	   <input name="cover" id="cover" size="37" value="<?php print $issue->Get("img"); ?>">
  </div>
  <div style="float:left; margin-left:5px;">
     <button type="button" onclick="openPopup()">Cerca</button>
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="color">Colore sfondo:</label>
	</td>
	<td>
  <div style="float:left">
	<input class="jscolor" name="color" id="color" value="<?php print $issue->Get("color");?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="altcolor">Colore titoli:</label>
	</td>
	<td>
  <div style="float:left">
	<input class="jscolor" name="altcolor" id="altcolor" value="<?php print $issue->Get("altcolor"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="shade">Schema colore: </label>
	</td>
	<td>
  <select name="shade" id="shade">
	<option value="0" <?php if ($issue->Get("shade")==0) {?>selected<?php } ?> >
      Schema scuro
    </option>
    <option value="1" <?php if ($issue->Get("shade")==1) {?>selected<?php } ?> style="color:#fff;background-color:#000;" >
      Schema chiaro
    </option>
    <option value="2" <?php if ($issue->Get("shade")==2) {?>selected<?php } ?> >
      Schema scuro, nero su foto cover
    </option>
    <option value="3" <?php if ($issue->Get("shade")==3) {?>selected<?php } ?> style="color:#fff;background-color:#000;" >
      Schema chiaro, nero su foto cover
    </option>
    <option value="4" <?php if ($issue->Get("shade")==4) {?>selected<?php } ?> style="color:#fff;background-color:#000;" >
      Invertito - Beta test
    </option>
  </select>
  </td>
</tr>
<tr>
	<td>
	<label for="status">Stato:</label>
	</td>
	<td>
	<?php
		if($action === "new") {
			$status = new IssueStatus();
			$status->RetrieveBy('label', ElementType::AsString, 'Infieri');
		} else {
			$status = $issue->Get("status");
		}
		print $status->Get('label'); ?>
	<input type="hidden" name="status" id="status" value=<?php print $status->Get("id"); ?>>
</tr>
<tr>
	<td>
	<label for="series">Serie:</label>
	</td>
	<td>
	<select name="series" id="series">
<?php
	foreach($listseries as $seriesid) {
		$series = new IssueSeries();
		$series->Retrieve($seriesid);
		$selected = "";
		if(($action != "new") && ($series->Get("id") == $issue->Get("series::id"))) {
			$selected = "selected";
		} 
?>
	<option value="<?php print $series->Get("id"); ?>" <?php print $selected; ?>><?php print $series->Get("label"); ?></option>
<?php
		}
?>	</select>
	</td>
</tr>
<tr>
	<td>
	<label for="thumbnail">icona libreria:</label>
	</td>
	<td>
	<script>
      function openPopupBook() {
        CKFinder.popup( { resourceType: "Libreria", chooseFiles: true, 
            onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
            var file = evt.data.files.first();
            document.getElementById( 'thumbnail' ).value = file.getUrl();
            } );
        finder.on( 'file:choose:resizedImage', function( evt ) {
            document.getElementById( 'thumbnail' ).value = evt.data.resizedUrl;
            } );
        }
      } );
    }
     </script>
  <div style="float:left">
	<input name="thumbnail" id="thumbnail" size="37" value="<?php print $book->Get("thumbnail"); ?>">
  </div>
  <div style="float:left; margin-left:5px;">
     <button type="button" onclick="openPopupBook()">Cerca</button>
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="pdfpath">cartaceo path:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="pdfpath" id="pdfpath" size="37" value="<?php print $book->Get("pdf_path"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td>
	<label for="ebookpath">ebook path:</label>
	</td>
	<td>
  <div style="float:left">
	<input name="ebookpath" id="ebookpath" size="37" value="<?php print $book->Get("ebook_path"); ?>">
  </div>
	</td>
</tr>
<tr>
	<td><label>Creato:<label></td>
	<td><?php print $issue->Get("created"); ?></td>
</tr>
</table>
<div align="left">
<input type="hidden" name="issueid" value="<?php print $issueid; ?>">
<button type="submit" name="action" value="store">Salva</button>
</div>
</form>

<div>
<img alt="" src="<?php print EOS_BASEURL.'core/frontend/eos_colorgrid.php?num='.$issue->Get('number'); ?>"  />
</div>
</div>

