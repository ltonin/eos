<?php
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
eos_session_start();

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

?>

<html>
<?php
/*************** HTML HEAD ***************/
include(EOS_BASEPATH."core/frontend/eos_headhtml.php");
/**************************************/

?>
<link rel="stylesheet" type="text/css" title="currentStyle" href="<?php print EOS_CSS_BASEURL.'/eos_styles_minicolors.php'; ?>" media="screen"></link>
<script src="<?php print EOS_BASEURL.'core/functions/eos_jquery_minicolors.js'; ?>" type="text/javascript"></script>
<script src="<?php print EOS_BASEURL.'core/functions/eos_javascript_support.js'; ?>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php print EOS_CSS_BASEURL.'eos_styles_backend.php'; ?>">
<link rel="stylesheet" type="text/css" href="<?php print EOS_CSS_BASEURL.'eos_styles_form-eos3.css.php'; ?>">

<script type="text/javascript" src="<?php print EOS_EXTRA_BASEURL.'ckfinder/eos_ckfinder_functions.js'; ?>"></script>
<body>
<div id="top">
<?php
/******** PAGE HEADER  *******/
include(EOS_BASEPATH."core/frontend/eos_header.php");
/**************************************/
?>
</div>
<div id="backend_left">
<?php
/******** PAGE LEFT PANEL *******/
include(EOS_BASEPATH."core/backend/eos_backend_left.php");
/**************************************/
?>
</div>
<div id="backend">
<?php 
$tool = 0;
if(isset($_GET['tool'])) {
	$tool = $_GET['tool'];
}

if($user->IsAuthorized("Magister Ludi")) { 
	switch($tool) {
		case 11:
			include(EOS_BASEPATH.'core/backend/eos_manage_article.php');
			break;
		case 12:
			include(EOS_BASEPATH.'core/backend/eos_view_article.php');
			break;
		case 13:
			include(EOS_BASEPATH.'core/backend/eos_show_articles.php');
			break;
		case 14:
			include(EOS_BASEPATH.'core/backend/eos_show_articles_type.php');
			break;
		case 15:
			//include(EOS_BASEPATH.'core/backend/eos_show_articles_trash.php');
			break;
		case 16:
      			// Alternative article visualization with select menu (not used)
			include(EOS_BASEPATH.'core/backend/eos_show_articles_alternative.php');
			break;
		case 17:
			include(EOS_BASEPATH.'core/backend/eos_show_institutionals.php');
			break;
		case 18:
			include(EOS_BASEPATH.'core/backend/eos_show_trash.php');
			break;
		case 19:
			include(EOS_BASEPATH.'core/backend/eos_show_atlas.php');
			break;
		case 21:
			include(EOS_BASEPATH.'core/backend/eos_manage_issue.php');
			break;
		case 22:
			include(EOS_BASEPATH.'core/backend/eos_show_issues.php');
			break;
		case 31:
			include(EOS_BASEPATH.'core/backend/eos_manage_series.php');
			break;
		case 41:
			include(EOS_BASEPATH.'core/backend/eos_manage_section.php');
			break;
		case 51:
			include(EOS_BASEPATH.'core/backend/eos_manage_user.php');
			break;
		case 52:
			include(EOS_BASEPATH.'core/backend/eos_show_users.php');
			break;
		case 61:
			include(EOS_BASEPATH.'core/backend/eos_manage_doi.php');
			break;
		case 71:
			include(EOS_BASEPATH.'core/backend/eos_manage_prince.php');
			break;
		default:
			include(EOS_BASEPATH.'core/backend/eos_backend_tools.php');
			break;
	}
} else {

	switch($tool) {
		case 11:
			include(EOS_BASEPATH.'core/backend/eos_manage_article.php');
			break;
		case 12:
			include(EOS_BASEPATH.'core/backend/eos_view_article.php');
			break;
		case 13:
			include(EOS_BASEPATH.'core/backend/eos_show_articles.php');
			break;
		case 14:
			include(EOS_BASEPATH.'core/backend/eos_show_articles_type.php');
			break;
		case 17:
			include(EOS_BASEPATH.'core/backend/eos_show_institutionals.php');
			break;
		case 19:
			include(EOS_BASEPATH.'core/backend/eos_show_atlas.php');
			break;
		case 15:
			include(EOS_BASEPATH.'core/backend/eos_show_articles_trash.php');
			break;
		case 51:
			include(EOS_BASEPATH.'core/backend/eos_manage_user.php');
			break;
		case 61:
			include(EOS_BASEPATH.'core/backend/eos_manage_doi.php');
			break;
		case 71:
			include(EOS_BASEPATH.'core/backend/eos_manage_prince.php');
			break;
		default:
			include(EOS_BASEPATH.'core/backend/eos_show_articles.php');
			break;
	}
}



?>
</div>

<?php
/******** PAGE BOTTOM PANEL *******/
include(EOS_BASEPATH."core/backend/eos_backend_bottom.php");
/**************************************/
?>

</div> 
</body>
</html>

