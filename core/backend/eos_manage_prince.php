<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

$message = new Message();
$selfurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=71';
$pdfurl  = EOS_BASEURL.'core/backend/eos_backend_prince.php';

?>

<div class="title">Generazione PDF</div>
<?php $message->Show(); ?>

<div class="eos-admin-form">
<form action="<?php print $pdfurl; ?>" method="post" id="form-find">

	<!-- Issue Id -->	
	<div class="row">
		<div class="col-left"><label for="issue-number">Numero Rivista:</label></div>
		<div class="col-right">
	    	<input type="text" id="issue-number" name="issue-number" placeholder="Numero rivista" value="" pattern="\d*" required>
		</div>
	</div>
		
	<div class="row">
		<div class="col-left"><label for="tipologia">Tipologia:</label></div>
		<div class="col-right" id="tipologia">
			<input type="radio" id="publication-pdf" name="publication-type" value="pdf" checked>
        	<label for="publication-pdf">cartaceo</label>
			
			<input type="radio" id="publication-ebook" name="publication-type" value="ebook">
        	<label for="publication-ebook">e-book</label>
		</div>
	</div>
		
	<!-- Button find -->
	<div class="row">
		<div class="col-left">
			<input type="submit" form="form-find" value="Genera">		
		</div>
	</div>
</form>
</div>




