<?php

//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__."/../eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
eos_session_start();
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$issue="";
if(isset($_GET['issue'])) {
	$issue = $_GET['issue'];
}


?>
<html>
<head>
	<style >
h2.titolo {font:normal 18pt "Linux Libertine O";padding:20px 150px 0 0}
h3.sottotitolo{font:normal 16pt "Linux Libertine O";padding:0 150px 0 0;}
h4.autore{font:normal 14pt "Linux Libertine O"; color: #444;padding:0 0 30px;}
.autore+.autore {padding-top:-40px}
h6.paragrafo{font:bold 12pt "Linux Libertine O"; padding:20px 20px 5px;}

.biblio {clear:both;}
ul.biblio {font:normal 10pt "Linux Libertine O";text-align:left;letter-spacing:0.04em}
ul.biblio, ul.biblio li{list-style:none inside ;padding: 5px 10px; color: #444;}
ul.biblio li a {display: inline !important;}
h5.biblio {font:normal 14pt "Linux Libertine O";text-align:left; padding:20px 0px 10px; margin-top:20px; border-top:1px dotted #000;}
h5.biblio a{text-decoration:none; color:#333;} 
h5.abstract {font:normal 14pt "Linux Libertine O"; padding:10px 20px 10px;}
.abstract {clear:both;}
h5.abstract:before, h5.abstract:after {content: &#8659;}
p.abstract{font:normal 10pt "Linux Libertine O";text-align:left;letter-spacing:0.04em;padding:20px 20px 0}
ol.note {font:normal 10pt "Linux Libertine O";text-align:left;letter-spacing:0.04em; color: #444;}
ol.note, ol.note li{padding: 0 10px}
ol li{list-style-type:decimal;}
p.caption{font:9pt "Linux Libertine O";padding:5px;}
p, ol{font:12pt "Linux Libertine O";padding-top:20px; color:#444;}
.caption {padding:5pt;}
sup {font-size: 50%;vertical-align: top;}
.citazione sup {font-size: 80%;vertical-align: top;}
p.citazione{font:10pt "Linux Libertine O";text-align: left;	}
p.citadx{font:10pt "Linux Libertine O";padding:20px 20px 0;text-align: right;}
.maiuscoletto {font-variant: small-caps;}</style>
</head>
<body>


<?php

if(empty($issue)) {
	echo "No issue number provided. Provide the issue number in page
		address. <br><br>Example: eos_backend_sandbox.php<b>?issue=150</b>";
} else {

// Retrieve issueid for number 150
$table = new Table('tb_numero');
$table->SetCondition('numero', $issue, ElementType::AsInteger, QueryCondition::IsEqual);
$table->Select('id');
$table->Get('id', $issueid, 0);

// Retrieve id of trash status
$table->SetTable('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve all article in issueid (no in trash)
$table->SetTable('tb_articolo');
$table->SetCondition('id_numero', $issueid, ElementType::AsInteger);
$table->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->SetOrder('titolo', QueryOrder::AsAscending);
$table->Select('id');
$table->Get('id', $listarticles);

foreach($listarticles as $carticleid) {
	$carticle = new Article();
	$carticle->Retrieve($carticleid);

	print $carticle->Get('text')."<br>";

}
}
?>


</body>
</html>
