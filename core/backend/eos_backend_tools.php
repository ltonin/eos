<?php

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false)
	header('location:'.EOS_BASEURL);

$urlarea    = EOS_BASEURL.'core/backend/eos_backend_index.php';

$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

$table = new Table('tb_articolo');
$table->SetCondition('stato', $trashid, ElementType::AsInteger);
$table->Select('id');
$ntrash = $table->Size('id');
?>
<div id="backend_tools" style="max-width=30px;">

<div style="float:left">
<div class="title">Rivista - Articoli</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=11&action=new">
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-new.png'; ?>" width="48" title="Nuovo articolo">
  <figcaption>Nuovo articolo</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=13">
  <figure>  
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-articles.png'; ?>" width="48" title="Gestione articoli">
  <figcaption>Gestione articoli</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=19">
  <figure>  
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-atlas.png'; ?>" width="48" title="Gestione tavole">
  <figcaption>Gestione tavole</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=17">
  <figure>  
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-institutionals.png'; ?>" width="48" title="Gestione istituzionali">
  <figcaption>Gestione istituzionali</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=41">
  <figure>  
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-categories.png'; ?>" width="48" title="Cestino">
  <figcaption>Gestione categorie</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=18">
  <figure>  
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="48" title="Cestino">
  <figcaption>Cestino (<?php print $ntrash; ?>)</figcaption>
  </figure>
  </a>
</div>

</div>





<br>

<div style="float:left">
<div class="title" >Rivista - Numeri</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=21&action=new">
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-new-issue.png'; ?>" width="48" title="Nuovo numero">
  <figcaption>Nuovo numero</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=22">
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-issue.png'; ?>" width="48" title="Gestione numeri">
  <figcaption>Gestione numeri</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=31">
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-series.png'; ?>" width="48" title="Gestione serie">
  <figcaption>Gestione serie</figcaption>
  </figure>
  </a>
</div>

</div>

<br>
<div style="float:left">
<div class="title">Sito</div>
<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=51&action=new">
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-new-user.png'; ?>" width="48" title="Nuovo utente">
  <figcaption>Nuovo utente</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="<?php print $urlarea; ?>?tool=52">
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-users.png'; ?>" width="48" title="Gestione utenti">
  <figcaption>Gestione utenti</figcaption>
  </figure>
  </a>
</div>

<div style="float:left; font-size:14px;">
  <a href="#" onclick="eos_ckfinder_launch('ckfinder1')" >
  <figure>
  <img src="<?php print EOS_SYSTEM_IMAGES.'icon-view-files.png'; ?>" width="48" title="Gestione files">
  <figcaption>Gestione risorse</figcaption>
  </figure>
  </a>
</div>

</div>

<br>
<div style="float:left">
<div class="title">Esportazione</div>

<div style="float:left; font-size:14px;">
<a href="<?php print $urlarea; ?>?tool=61">
<figure>
<img src="<?php print EOS_SYSTEM_IMAGES.'icona-xml-doi.png'; ?>" width="48" title="Gestione DOI">
<figcaption>Gestione DOI</figcaption>
</figure>
</a>
</div>

<div style="float:left; font-size:14px;">
<a href="<?php print $urlarea; ?>?tool=71">
<figure>
<img src="<?php print EOS_SYSTEM_IMAGES.'icona-pdf.png'; ?>" width="48" title="Crea PDF">
<figcaption>Crea PDF</figcaption>
</figure>
</a>
</div>

</div>

</div>
