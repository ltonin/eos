<?php 
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized() == false)
	header('location:'.EOS_BASEURL);


$urlarea    = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlmanage  = $urlarea.'?tool=11';
$urlpanel   = $urlarea.'?tool=16';
$urlissue   = $urlarea.'?tool=13';
$urlinstitutionals  = $urlarea.'?tool=17';
$urlatlas   = $urlarea.'?tool=19';
$urlprofile = $urlarea.'?tool=51';
$exportdoi  = $urlarea.'?tool=61';
$generatepdf= $urlarea.'?tool=71';
?>
<div class="title">
<?php print $user->Get("type::label"); ?> | <?php print $user->Get("nickname"); ?>
</div>
<?php 
if($user->IsAuthorized("Magister Ludi") == true) {
?>
	<a href="<?php print $urlarea; ?>" target="_self">Pannello di controllo</a><br>
<?php
}
?>
<div class="subtitle">Articoli</div>
<a href="<?php print $urlmanage.'&action=new'; ?>" target="_self">Nuovo Articolo</a><br>
<a href="<?php print $urlissue; ?>" target="_self">Gestione Articoli</a><br>
<a href="<?php print $urlatlas; ?>" target="_self">Gestione Tavole</a><br>
<a href="<?php print $urlinstitutionals; ?>" target="_self">Gestione Istituzionali</a><br>

<div class="subtitle">Risorse</div>
<a id="manage-images"    href="#" onclick="eos_ckfinder_launch('ckfinder1', 'Immagini')">Gestione immagini</a><br>
<a id="manage-bookstore" href="#" onclick="eos_ckfinder_launch('ckfinder2', 'Libreria')">Gestione libreria</a><br>
<a id="manage-video"     href="#" onclick="eos_ckfinder_launch('ckfinder3', 'Video')">Gestione video</a><br>

<div class="subtitle">Esportazione</div>
<a href="<?php print $exportdoi; ?>" target="_self">Gestione DOI</a><br>
<a href="<?php print $generatepdf; ?>" target="_self">Genera PDF</a><br>

<div class="subtitle">Utente</div>
<a href="<?php print $urlprofile.'&action=profile'; ?>" target="_self">Modifica profilo</a><br>
<a href="<?php print EOS_BASEURL.'core/frontend/eos_logout.php'; ?>" target="_self">Logout</a>


<div class="title">In fieri</div>
<?php
$table = new Table('issue_status');	
$table->SetCondition('label', 'Infieri', ElementType::AsString); 
$table->Select('id');
$table->Get('id', $statusid, 0);

$table->SetTable('tb_numero');
$table->SetCondition('stato', $statusid, ElementType::AsInteger);
$table->SetCondition('numero', 1000, ElementType::AsInteger, QueryCondition::IsLesser);
$table->SetOrder('numero');
$table->Select('id');
$table->Get('id', $issuelist);

$table->SetTable('tb_numero');
$table->SetCondition('stato', $statusid, ElementType::AsInteger);
$table->SetCondition('numero', 999, ElementType::AsInteger, QueryCondition::IsGreater);
$table->SetOrder('numero');
$table->Select('id');
$table->Get('id', $panellist);

foreach($issuelist as $issueid) {
	$issue = new Issue();
	$issue->Retrieve($issueid);
	$clink = "<a href='".$urlissue."&showissueid=".$issue->Get('id')."'>Numero ".$issue->Get('number')."</a>";
?>
	<?php print $clink; ?><br>
<?php
}
?>


<div class="title">Archivio</div>
<?php
$table = new Table('issue_status');
$table->SetCondition('label', 'Archiviato', ElementType::AsString);
$table->Select('id');
$table->Get('id', $statusid, 0);

$table->SetTable('tb_numero');
$table->SetCondition('stato', $statusid, ElementType::AsInteger);
$table->SetOrder('numero', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $issuelist);
?>

<form  action="<?php print $urlissue; ?>" method="post">

  <select id="showissueid" name="showissueid" onchange="this.form.submit();">

<?php

foreach($issuelist as $issueid) {
	$issue = new Issue();
	$issue->Retrieve($issueid);
?>
	<option value="<?php print $issue->Get('id'); ?>"><?php print $issue->Get('number'); ?></option>
<?php 
}
?>
  </select>
</form>


<?php /*
<input style="float:left; font-size:12px" type="text" name="articleid" size="6" maxlength="6" placeholder="ID articolo"></td>
<button type="submit" name="action"    value="modify" >
	<img src="<?php print EOS_SYSTEM_IMAGES.'icon-edit.png'; ?>" width="18" title="Modifica">
</button>
</form>
 */?>

<?php 

/* Tavole not showed in left menu

<!-- Temporary hack for Tavole -->
<div class="title">Tavole</div>
<?php
$table->SetTable('tb_numero');
$table->SetCondition('stato', $statusid, ElementType::AsInteger);
$table->SetCondition('numero', 999, ElementType::AsInteger, QueryCondition::IsGreater);
$table->SetOrder('numero');
$table->Select('id');
$table->Get('id', $panellist);

foreach($panellist as $panelid) {
	$panel = new Issue();
	$panel->Retrieve($panelid);
	$clink = "<a href='".$urlissue."&showissueid=".$panel->Get('id')."'>Numero ".$panel->Get('number')."</a>";
?>
	<?php print $clink; ?><br>
<?php
}
?>
 
*/?>
