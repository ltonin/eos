<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized("Magister Ludi") == false)
	header('location:'.EOS_BASEURL);

$message   = new Message();
$selfurl   = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=22';
$parenturl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=21';

/** Manage post action **/
$action   = "";
$issueid  = "";
$statusid = "";
if(isset($_POST['action'])) 
	$action=$_POST['action'];
if(isset($_POST['issueid'])) 
	$issueid = $_POST['issueid'];
if(isset($_POST['statusid'])) 
	$statusid = $_POST['statusid'];

switch($action) {
	/** Store issue action/visualization **/
	case "modify":
		$issue = new Issue();
		$issue->Retrieve($issueid);
		$status = new IssueStatus();
		$status->Retrieve($statusid);

		// Looking for cover with 'ok' status
		if($status->Get('label') === 'Pubblicato') {
			$covertype = new ArticleType();
			$covertype->RetrieveBy('nome', ElementType::AsString, 'Copertina');
			$covertypeid = $covertype->Get('id');
			
			$coverstatus = new ArticleStatus();
			$coverstatus->RetrieveBy('label', ElementType::AsString, 'ok');
			$coverstatusid = $coverstatus->Get('id');

			$table = new Table('tb_articolo');
			$table->SetCondition('id_numero', $issue->Get('id'), ElementType::AsInteger);
			$table->SetCondition('tipo',  $covertypeid, ElementType::AsInteger);
			$table->SetCondition('stato', $coverstatusid, ElementType::AsInteger);
			$table->Select('id');
			$nfound = $table->Get('id', $coverlist);

			if($nfound == 0) {
				$message->AddMessage("Numero non pubblicato. 
						      Non c'e' una cover con stato '"
						      .$coverstatus->Get('label')."'", 
						      MessageType::AsWarning);
				break;
			}

			// Looking for the current published issue
			$cissuestatus = new IssueStatus();
			$cissuestatus->RetrieveBy('label', ElementType::AsString, 'Pubblicato');
			$cissue = new Issue();
			$cissue->RetrieveBy('stato', ElementType::AsInteger, $cissuestatus->Get('id'));

			$cissuestatus->RetrieveBy('label', ElementType::AsString, 'Archiviato');
			$cissue->Set("status", $cissuestatus);
			if($cissue->Store() == false) {
				$message->AddMessage("Cannot change status of the previous published issue in database", MessageType::AsDebug);
			}
		}


		$issue->Set("status", $status);

		$message->AddMessage("Numero '".$issue->Get('number')."': stato modificato in '".$issue->Get('status::label')."'", MessageType::AsInfo);
		if($issue->Store() == false) {
			$message->AddMessage("Cannot store issue in database", MessageType::AsDebug);
		}
		break;
	/** Delete issue action **/
	case "delete":
		$issue = new Issue();
		$issue->Retrieve($issueid);
		$issue->Drop();
		$message->AddMessage("Numero '".$issue->Get('number')."' eliminato", MessageType::AsInfo);
		break;
}


// Get list of all attributes needed
$table = new Table('tb_numero');
// Temporary hack for Tavole (until I create new table for them)
$table->SetCondition('numero', 1000, ElementType::AsInteger, QueryCondition::IsLesser);
$table->SetOrder('stato');
$table->SetOrder('numero', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $listissue);

$table->SetTable('issue_status');
$table->Select('id');
$table->Get('id', $liststatus);
?>

<div id="backend_show">
<div class="title">Gestione numeri</div>
<?php $message->Show(); ?>
<table>
<tr>
	<th>Numero</th>
	<th>Mese</th>
	<th>Descrizione</th>
	<th>Curatori</th>
	<th>Extra</th>
	<th>Serie</th>
	<th>ISBN</th>
	<th>Creato</th>
	<th>Stato</th>
	<th></th>
	<th></th>
</tr>
<?php 
	foreach($listissue as $cissueid) {
		$issue = new Issue();
		$issue->Retrieve($cissueid);
?>
<tr>
	<td><?php print $issue->Get("number"); ?></td>	
	<td><?php print $issue->Get("special"); ?></td>
	<td><?php print $issue->Get("description"); ?></td>
	<td><?php print $issue->Get("authors"); ?></td>
	<td><?php print $issue->Get("extra"); ?></td>
	<td><?php print $issue->Get("series::label"); ?></td>
	<td><?php print $issue->Get("isbn"); ?></td>
	<td><?php print $issue->Get("created"); ?></td>
	<td>
	<form action="<?php print $selfurl; ?>" method="post">
	<select name="statusid" onchange="this.form.submit()">
<?php
		reset($liststatus);
		foreach($liststatus as $cstatusid) {
			$status = new IssueStatus();
			$status->Retrieve($cstatusid);
			$selected = "";
			if($status->Get("id") == $issue->Get("status::id")) {
				$selected = "selected";
			}
?>
	<option value="<?php print $status->Get("id"); ?>" <?php print $selected; ?>><?php print $status->Get("label"); ?></option>
<?php
		}
?>
	</select>
	<input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
	<input type="hidden" name="action" value="modify">
	</form>
	</td>
	<td>
	<form action="<?php print $parenturl."&action=modify"; ?>" method="post" >
		<input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
		<button type="submit">
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-edit.png'; ?>" width="24" title="Modifica">
    </button>
	</form>
	</td>
	<td>
	<form action="<?php print $selfurl; ?>" method="post" onsubmit="return confirm('Vuoi veramente cancellare questo numero (<?php print $issue->Get("number"); ?>)?');">
		<input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
		<button type="submit" name="action" value="delete">
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Elimina">
</button>
	</form>
	</td>
</tr>
<?php
	}
?>
</table>
</div>

