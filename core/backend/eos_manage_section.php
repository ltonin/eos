<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized("Magister Ludi") == false) 
	header('location:'.EOS_BASEURL);

$message = new Message();
$selfurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=41';

/** Manage post action **/
$action = "";
if(isset($_POST['action'])) {
	$action = $_POST['action'];
	$sectionid = $_POST['sectionid'];
} elseif(isset($_GET['action'])) {
	$action = $_GET['action'];
	$sectionid = $_GET['sectionid'];
}

switch($action) {
	/** Position section action **/
	case "position":
		$step = $_POST['setposition'];
		$section = new ArticleSection();
		$section->Retrieve($sectionid);
		$refsection = &$section;
		
		if(!eos_section_setposition($refsection, $step))
			$message->AddMessage("Posizione non modificata", MessageType::AsWarning);
		else
			$message->AddMessage("Posizione modificata ('"
					      .$section->Get('label')."'): "
					      .$section->Get('position'), MessageType::AsInfo);
		break;
	/** Store section action **/
	case "store":
		$section = new ArticleSection();
		if(isset($_POST['sectionid']) && empty($_POST['sectionid']) == false)
			$section->Retrieve($_POST['sectionid']);

		$section->Set('label',    $_POST['slabel']);
		
		if(isset($_POST['position']))
			$section->Set('position', $_POST['position']);

		if($section->Store() == false) {
			$message->AddMessage("Cannot store section in database", MessageType::AsDebug);
			die();
		}
		$message->AddMessage("Categoria salvata ('".$section->Get('label')."')", MessageType::AsInfo);
		break;
	/** Delete section action **/
	case "delete":
		if(isset($_POST['sectionid'])) {
			$seriesid = $_POST['sectionid'];
		}
		
		$section = new ArticleSection();
		$section->Retrieve($sectionid);
		$section->Drop();

		$table = new Table('tb_categoria');
		$table->SetOrder('pos');
		$table->Select('id');
		$table->Get('id', $listsection);

		$pid = 1;
		foreach($listsection as $csectionid) {
			$csection = new ArticleSection();
			$csection->Retrieve($csectionid);
			$csection->Set('position', $pid++);
			$csection->Store();
		}
		$message->AddMessage("Categoria eliminata ('".$section->Get('label')."')", MessageType::AsInfo);
		break;
}

// Get list of all attributes needed
$table = new Table('tb_categoria');
$table->SetOrder('pos');
$table->Select('id');
$table->Get('id', $listsection);
$table->Select('MAX(`pos`) AS maxpos');
$table->Get('maxpos', $maxposition, 0);
?>

<div id="backend_show">
<div class="title">Gestione categorie</div>
<?php $message->Show(); ?>
<table>
<tr>
   <th>Nome categoria</th>
   <th>Posizione</th>
   <th>Strumenti</th>
</tr>
<?php 
   $fid = 0;
   foreach($listsection as $sectionid) {
   	$section = new ArticleSection();
   	$section->Retrieve($sectionid);
   	$label    = $section->Get("label");
   	$position = $section->Get("position");
?>
<tr>
   <td>
   <form action="<?php print $selfurl; ?>" method="post" id="<?php print 'form'.$section->Get('id'); ?>">
   <input name="slabel"    value="<?php print $label; ?>" required>
   </form>
   </td>
   
   <td>
   <form action="<?php print $selfurl; ?>" method="post">
   <input type="hidden" name="action" value="position">
   <input type="hidden" name="sectionid" value="<?php print $section->Get('id'); ?>">
   <button type="submit" name="setposition" value=-1>
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-up.png'; ?>" width="24" title="Su">
   </button>
   <button type="submit" name="setposition" value=1 >
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-down.png'; ?>" width="24" title="Giù">
   </button>
   </form>
   </td>

   <td>
   <input  form="<?php print 'form'.$section->Get('id'); ?>" type="hidden" name="sectionid" value="<?php print $section->Get("id"); ?>">
   <button form="<?php print 'form'.$section->Get('id'); ?>" type="submit" name="action" value="store">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-save.png'; ?>" width="24" title="Salva">
</button>
   <button form="<?php print 'form'.$section->Get('id'); ?>" type="submit" name="action" value="delete" 
	   onclick="return confirm('La categoria verr\u00E0 cancellata definitivamente. Confermi l\'eliminazione?')">
  
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Elimina">
   </button>
   </td>
</tr>
<?php
	$fid++;
   }
?>
<tr>
   <td>
   <form action="<?php print $selfurl; ?>" method="post">
   <input name="slabel" required>
   </td>
   <td></td>
   <td>
   <input type="hidden" name="sectionid" value="">
   <input type="hidden" name="action" value="store">
   <input type="hidden" name="position" value="<?php print $maxposition+1; ?>">
   <button type="submit" name="action" value="store">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-save.png'; ?>" width="24" title="Salva">
   </button>
   </form>
   </td>
</tr>

</table>
</div>
