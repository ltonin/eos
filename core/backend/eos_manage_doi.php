<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_doi.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_xml_doi.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

$message = new Message();
$selfurl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=61';
$action  = "find";

/** Form data **/
if(isset($_GET['article_id']) || isset($_POST['action'])) {
	$action = $_POST['action'];
}


// Processing different actions
$article = new Article();

switch($action) {
	case "edit":

		// Retrieve article id
		if(isset($_GET['article_id']))
			$article_id = $_GET['article_id'];
		else
			$article_id = $_POST['article_id'];

		$article->Retrieve($article_id);
		if (empty($article->Get('id')) ) {
			$message->AddMessage("There is no article with id: ".$article_id, MessageType::AsError);
			$action = "find";
			break;
			
		}


		// Retrieve information from DOI table
		$doi = new DOI();
		$doi->RetrieveByArticle($article_id);

		if ( empty($doi->Get('id')) == true ) {
			$message->AddMessage("There is no DOI for article with id: ".$article_id.". Please proceed to create it", MessageType::AsInfo);
		} else {
			$message->AddMessage("Found DOI for article with id: ".$article_id.". Please proceed to modify it or download the associated XML", MessageType::AsInfo);
		}
		break;

	case "store":
	case "download":
			$article_id = $_POST['article_id'];
			$article->Retrieve($article_id);

			$doi = new DOI();
			
			$doi->Set('id',				$_POST['id']);
			$doi->Set('article_id', 	$_POST['article_id']);
			$doi->Set('doi',			$_POST['doi']);
			$doi->Set('creator',		$_POST['creator']);
			$doi->Set('status',			$_POST['status']);
			$doi->Set('url',			$_POST['url']);
			$doi->Set('title',			$_POST['title']);
			$doi->Set('language',		$_POST['language']);
			$doi->Set('publisher',		$_POST['publisher']);
			$doi->Set('year',			$_POST['year']);
			$doi->Set('resource',		$_POST['resource']);
			$doi->Set('abstract',		$_POST['abstract']);
			$doi->Set('keywords',		$_POST['keywords']);
			$doi->Set('modified_by',	$user->Get('id'));

			if($doi->Store() == false) {
				$message->AddMessage("Cannot store DOI in database", MessageType::AsDebug);
				die();
			}

			$doi->RetrieveByArticle($article_id);
			$message->AddMessage("DOI stored for article with id: ".$doi->Get("id"), MessageType::AsInfo);
			break;


}

$issue = $article->Get('issue::number');

if(isset($_POST["download"])) {
	$xml_template_datacite = EOS_DOI_BASEPATH."eos_doi_template_datacite.xml";
	
	$xml = new XmlDOI($xml_template_datacite);

	$xml->SetDoi($doi->Get("doi"));
	$xml->SetAuthor($doi->Get("creator"));
	$xml->SetTitle($doi->Get("title"));
	$xml->SetPublisher($doi->Get("publisher"));
	$xml->SetYear($doi->Get("year"));
	$xml->SetLanguage($doi->Get("language"));
	$xml->SetResourceType($doi->Get("resource"));
	$xml->SetAbstract($doi->Get("abstract"));
	$xml->SetKeywords($doi->Get("keywords"));
	$xml->SetAlternateIdentifier();
	$xml->SetRelatedIdentifier();
	$xml->SetRights();

	$filename = date('Ymd.His') . '.engramma.doi.' . $doi->Get("article_id") . '.xml';
	
	$content = $xml->GetXML();
?>
<script>
	function download(filename, text) {
	  var element = document.createElement('a');
	  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	  element.setAttribute('download', filename);
	
	  element.style.display = 'none';
	  document.body.appendChild(element);
	
	  element.click();
	
	  document.body.removeChild(element);
	}
	
	// Start file download.
	download("<?php print $filename; ?>", <?php print json_encode($content); ?>);
</script>
<?php

	$message->AddMessage("DOI downloaded in: ".$filename);
}
?>


<div class="title">Gestione DOI</div>
<?php $message->Show(); ?>

<?php //if ($action == "find") { ?>

<div class="eos-admin-form">
<form action="<?php print $selfurl; ?>" method="post" id="form-find">

	<!-- Id articolo -->	
	<div class="row">
		<div class="col-left"><label for="article_id">Id Articolo:</label></div>
		<div class="col-right">
		<input type="text" size="10" id="article_id" name="article_id" placeholder="Id Articolo" value="" pattern="\d*" title="Id Articolo" required>
		
		<!-- Action field -->
		<input type="hidden" name="action" id="action" value="edit">		
		
		<!-- Button find -->
		<input type="submit" form="form-find" value="Trova">		
		</div>
	</div>
</form>
</div>

<?php // } else { ?>
<?php if ($action != "find") { ?>

<?php 
$article_doi_index = $doi->ArticleDoiIndex($article->Get("issue::id"), $article_id);
?>

<script type="text/javascript">
function autogendoi(index) {
	var year = document.getElementById("year").value;
	var issue = document.getElementById("number").value;
	var type = document.getElementById("resource").value;
	var result = "https://doi.org/10.25432/1826-901X/";

	if(type == "articolo") {
		result = result	+ year + "." + issue + "." + index;
	} else {
		result = result + year + "." + issue;
	}
	document.getElementById("doi").value=result;
}
</script>

<div class="eos-admin-form">
<form action="<?php print $selfurl; ?>" method="post" id="form-edit">

	<!-- DOI id -->
	<input type="hidden" name="id" id="id" value="<?php print $doi->Get('id'); ?>">		
	
	<!-- Id articolo -->
	<div class="row">
		<div class="col-left"><label  for="article_id">Id Articolo:</label></div>
		<div class="col-right">
		<input type="text" size="10" id="article_id" name="article_id" placeholder="Id Articolo" value="<?php print $article_id; ?>" readonly>
		</div>
	</div>
	
	<!-- Numero -->
	<div class="row">
		<div class="col-left"><label  for="number">Numero:</label></div>
		<div class="col-right">
		<input type="text" size="10" id="number" name="number" placeholder="Numero" value="<?php print $issue; ?>" readonly>
		</div>
	</div>
	
	<!-- Autore/i -->
	<div class="row">
		<div class="col-left"><label  for="creator">Autore/i:</label></div>
		<div class="col-right">
		<input type="text" size="100" id="creator" name="creator" placeholder="Cognome1, Nome1;Cognome2, Nome2;Congome3, Nome3 ..." value="<?php print $doi->Get('creator'); ?>" required>
		</div>
	</div>



	<!-- Stato -->
	<div class="row">
		<div class="col-left"><label  for="status">Stato:</label></div>
		<div class="col-right">
		<select id="status" name="status">
		
			<option value="bozza" <?php if($doi->Get('status') == "bozza" || empty($doi->Get('status')) == true) print "selected"; ?>>bozza</option>
			<option value="registrato" <?php if($doi->Get('status') == "registrato") print "selected"; ?>>registrato</option>
			<option value="ricercabile" <?php if($doi->Get('status') == "ricercabile") print "selected"; ?>>ricercabile</option>
		</select>
		</div>
	</div>
	
	<!-- URL -->
	<div class="row">
		<div class="col-left"><label  for="url">URL:</label></div>
		<div class="col-right">
		<input type="url" size="100" id="url" name="url" placeholder="URL" value="<?php print EOS_BASEURL."index.php?id_articolo=".$article_id; ?>" readonly>
		</div>
	</div>

	<!-- Titolo -->
	<div class="row">
		<div class="col-left"><label  for="title">Titolo:</label></div>
		<div class="col-right">
		<input type="text" size="100" id="title" name="title" placeholder="Titolo" value="<?php print $doi->Get('title'); ?>" required>
		</div>
	</div>
	
	<!-- Editore -->
	<div class="row">
		<div class="col-left"><label  for="publisher">Editore:</label></div>
		<div class="col-right">
		<input type="text" size="50" id="publisher" name="publisher" placeholder="Editore" value="<?php if(empty($doi->Get('publisher'))) { print "La Rivista di Engramma"; } else { print $doi->Get('publisher'); } ?>" required>
		</div>
	</div>
	
	<!-- Anno -->
	<div class="row">
		<div class="col-left"><label  for="year">Anno:</label></div>
		<div class="col-right">
		<input type="text" size="10" id="year" name="year" placeholder="Anno" value="<?php print $doi->Get('year'); ?>" required>
		</div>
	</div>
	
	<!-- Risorsa -->
	<div class="row">
		<div class="col-left"><label  for="resource">Tipo di risorsa:</label></div>
		<div class="col-right">
		<select id="resource" name="resource">
			<option value="fascicolo" <?php if($doi->Get('resource') == "fascicolo") print "selected"; ?>>Fascicolo</option>
			<option value="articolo" <?php if($doi->Get('resource') == "articolo" || empty($doi->Get('resource')) == true) print "selected";  ?>>Articolo</option>
		</select>
		</div>
	</div>

	<!-- DOI -->
	<div class="row">
		<div class="col-left"><label  for="doi">DOI:</label></div>
		<div class="col-right">
		<input type="text" size="60" id="doi" name="doi" placeholder="DOI" value="<?php print $doi->Get('doi'); ?>" required>
		<input type="button" value="Genera DOI" name="Auto" onclick="autogendoi('<?php print $article_doi_index; ?>')">  
		</div>
	</div>

	<!-- Abstract -->
	<div class="row">
		<div class="col-left"><label  for="abstract">Abstract:</label></div>
		<div class="col-right">
		<textarea rows="20" cols="100" id="abstract" name="abstract"><?php print $doi->Get('abstract'); ?></textarea>
		</div>
	</div>
	
	<!-- Keywords -->
	<div class="row">
		<div class="col-left"><label  for="keywords">Keywords:</label></div>
		<div class="col-right">
		<input type="text" size="100" id="keywords" name="keywords" placeholder="keyword1;keyword2;keyword3 ..." pattern="^((\w\s?)+;\s?)*(\w\s?)+$" value="<?php print $doi->Get('keywords'); ?>" title="keyword separate da punto e virgola: KEY1; KEY2">
		</div>
	</div>
	
	<!-- Lingua -->
	<div class="row">
		<div class="col-left"><label  for="language">Lingua:</label></div>
		<div class="col-right">
			<select id="language" name="language">
			  <option value="af" <?php if($doi->Get('language') == "af") print "selected"; ?>>Afrikaans</option>
			  <option value="sq" <?php if($doi->Get('language') == "sq") print "selected"; ?>>Albanian</option>
			  <option value="ar" <?php if($doi->Get('language') == "ar") print "selected"; ?>>Arabic</option>
			  <option value="hy" <?php if($doi->Get('language') == "hy") print "selected"; ?>>Armenian</option>
			  <option value="eu" <?php if($doi->Get('language') == "eu") print "selected"; ?>>Basque</option>
			  <option value="bn" <?php if($doi->Get('language') == "bn") print "selected"; ?>>Bengali</option>
			  <option value="bg" <?php if($doi->Get('language') == "bg") print "selected"; ?>>Bulgarian</option>
			  <option value="ca" <?php if($doi->Get('language') == "ca") print "selected"; ?>>Catalan</option>
			  <option value="km" <?php if($doi->Get('language') == "km") print "selected"; ?>>Cambodian</option>
			  <option value="zh" <?php if($doi->Get('language') == "zh") print "selected"; ?>>Chinese (Mandarin)</option>
			  <option value="hr" <?php if($doi->Get('language') == "hr") print "selected"; ?>>Croatian</option>
			  <option value="cs" <?php if($doi->Get('language') == "cs") print "selected"; ?>>Czech</option>
			  <option value="da" <?php if($doi->Get('language') == "da") print "selected"; ?>>Danish</option>
			  <option value="nl" <?php if($doi->Get('language') == "nl") print "selected"; ?>>Dutch</option>
			  <option value="en" <?php if($doi->Get('language') == "en") print "selected"; ?>>English</option>
			  <option value="et" <?php if($doi->Get('language') == "et") print "selected"; ?>>Estonian</option>
			  <option value="fj" <?php if($doi->Get('language') == "fj") print "selected"; ?>>Fiji</option>
			  <option value="fi" <?php if($doi->Get('language') == "fi") print "selected"; ?>>Finnish</option>
			  <option value="fr" <?php if($doi->Get('language') == "fr") print "selected"; ?>>French</option>
			  <option value="ka" <?php if($doi->Get('language') == "ka") print "selected"; ?>>Georgian</option>
			  <option value="de" <?php if($doi->Get('language') == "de") print "selected"; ?>>German</option>
			  <option value="el" <?php if($doi->Get('language') == "el") print "selected"; ?>>Greek</option>
			  <option value="gu" <?php if($doi->Get('language') == "gu") print "selected"; ?>>Gujarati</option>
			  <option value="he" <?php if($doi->Get('language') == "he") print "selected"; ?>>Hebrew</option>
			  <option value="hi" <?php if($doi->Get('language') == "hi") print "selected"; ?>>Hindi</option>
			  <option value="hu" <?php if($doi->Get('language') == "hu") print "selected"; ?>>Hungarian</option>
			  <option value="is" <?php if($doi->Get('language') == "is") print "selected"; ?>>Icelandic</option>
			  <option value="id" <?php if($doi->Get('language') == "id") print "selected"; ?>>Indonesian</option>
			  <option value="ga" <?php if($doi->Get('language') == "ga") print "selected"; ?>>Irish</option>
			  <option value="it" <?php if($doi->Get('language') == "it" || empty($doi->Get('language'))) print "selected"; ?>>Italian</option>
			  <option value="ja" <?php if($doi->Get('language') == "ja") print "selected"; ?>>Japanese</option>
			  <option value="jw" <?php if($doi->Get('language') == "jw") print "selected"; ?>>Javanese</option>
			  <option value="ko" <?php if($doi->Get('language') == "ko") print "selected"; ?>>Korean</option>
			  <option value="la" <?php if($doi->Get('language') == "la") print "selected"; ?>>Latin</option>
			  <option value="lv" <?php if($doi->Get('language') == "lv") print "selected"; ?>>Latvian</option>
			  <option value="lt" <?php if($doi->Get('language') == "lt") print "selected"; ?>>Lithuanian</option>
			  <option value="mk" <?php if($doi->Get('language') == "mk") print "selected"; ?>>Macedonian</option>
			  <option value="ms" <?php if($doi->Get('language') == "ms") print "selected"; ?>>Malay</option>
			  <option value="ml" <?php if($doi->Get('language') == "ml") print "selected"; ?>>Malayalam</option>
			  <option value="mt" <?php if($doi->Get('language') == "mt") print "selected"; ?>>Maltese</option>
			  <option value="mi" <?php if($doi->Get('language') == "mi") print "selected"; ?>>Maori</option>
			  <option value="mr" <?php if($doi->Get('language') == "mr") print "selected"; ?>>Marathi</option>
			  <option value="mn" <?php if($doi->Get('language') == "mn") print "selected"; ?>>Mongolian</option>
			  <option value="ne" <?php if($doi->Get('language') == "ne") print "selected"; ?>>Nepali</option>
			  <option value="no" <?php if($doi->Get('language') == "no") print "selected"; ?>>Norwegian</option>
			  <option value="fa" <?php if($doi->Get('language') == "fa") print "selected"; ?>>Persian</option>
			  <option value="pl" <?php if($doi->Get('language') == "pl") print "selected"; ?>>Polish</option>
			  <option value="pt" <?php if($doi->Get('language') == "pt") print "selected"; ?>>Portuguese</option>
			  <option value="pa" <?php if($doi->Get('language') == "pa") print "selected"; ?>>Punjabi</option>
			  <option value="qu" <?php if($doi->Get('language') == "qu") print "selected"; ?>>Quechua</option>
			  <option value="ro" <?php if($doi->Get('language') == "ro") print "selected"; ?>>Romanian</option>
			  <option value="ru" <?php if($doi->Get('language') == "ru") print "selected"; ?>>Russian</option>
			  <option value="sm" <?php if($doi->Get('language') == "sm") print "selected"; ?>>Samoan</option>
			  <option value="sr" <?php if($doi->Get('language') == "sr") print "selected"; ?>>Serbian</option>
			  <option value="sk" <?php if($doi->Get('language') == "sk") print "selected"; ?>>Slovak</option>
			  <option value="sl" <?php if($doi->Get('language') == "sl") print "selected"; ?>>Slovenian</option>
			  <option value="es" <?php if($doi->Get('language') == "es") print "selected"; ?>>Spanish</option>
			  <option value="sw" <?php if($doi->Get('language') == "sw") print "selected"; ?>>Swahili</option>
			  <option value="sv" <?php if($doi->Get('language') == "sv") print "selected"; ?>>Swedish </option>
			  <option value="ta" <?php if($doi->Get('language') == "ta") print "selected"; ?>>Tamil</option>
			  <option value="tt" <?php if($doi->Get('language') == "tt") print "selected"; ?>>Tatar</option>
			  <option value="te" <?php if($doi->Get('language') == "te") print "selected"; ?>>Telugu</option>
			  <option value="th" <?php if($doi->Get('language') == "th") print "selected"; ?>>Thai</option>
			  <option value="bo" <?php if($doi->Get('language') == "bo") print "selected"; ?>>Tibetan</option>
			  <option value="to" <?php if($doi->Get('language') == "to") print "selected"; ?>>Tonga</option>
			  <option value="tr" <?php if($doi->Get('language') == "tr") print "selected"; ?>>Turkish</option>
			  <option value="uk" <?php if($doi->Get('language') == "uk") print "selected"; ?>>Ukrainian</option>
			  <option value="ur" <?php if($doi->Get('language') == "ur") print "selected"; ?>>Urdu</option>
			  <option value="uz" <?php if($doi->Get('language') == "uz") print "selected"; ?>>Uzbek</option>
			  <option value="vi" <?php if($doi->Get('language') == "vi") print "selected"; ?>>Vietnamese</option>
			  <option value="cy" <?php if($doi->Get('language') == "cy") print "selected"; ?>>Welsh</option>
			  <option value="xh" <?php if($doi->Get('language') == "xh") print "selected"; ?>>Xhosa</option>
			</select>
		</div>
	</div>

	<!-- Modified by -->
	<div class="row">
		<div class="col-left"><label  for="modified_by">Modificato da:</label></div>
		<div class="col-right">
		<?php 
		$moduser = new User();
		$moduser->Retrieve($doi->Get('modified_by'));
		$modified_by = $moduser->Get('nickname');
		?>
		<input type="text" size="100" id="modified_by" name="modified_by" value="<?php print $modified_by ?>" readonly>
		</div>
	</div>

	<!-- Modified on -->
	<div class="row">
		<div class="col-left"><label  for="modified_on">Ultima modifica:</label></div>
		<div class="col-right">
		<input type="text" size="100" id="modified_on" name="modified_on" value="<?php print $doi->Get('modified_on'); ?>" readonly>
		</div>
	</div>

	<!-- Action field -->
	<input type="hidden" name="action" id="action" value="store">		

	<!-- Button find -->	
	<div class="row">
		<div class="col-left"></div>
		<div class="col-right">
		<input type="submit" form="form-edit" value="Salva">		
		<input type="submit" form="form-edit" name="download" value="Salva e Download">		
		</div>
	</div>
</form>
</div>

<?php } ?>

