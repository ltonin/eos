<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_doi.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_xml_doi.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0)
	$user->Retrieve($_SESSION['userid'] );

$db = Database::Connect();
$stmt = $db->prepare("SELECT * FROM `article_doi`");
if($stmt->execute() == false) {
	trigger_error('[mysql::sandbox] '.$merror);
	$dberror = true;
}

$result = $stmt->get_result();

print_r($result);

while($row = $result->fetch_array()) {

	$doi = new DOI();	
	$doi->Set('id',				$row['id']);
	$doi->Set('article_id', 	$row['article_id']);
	$doi->Set('doi',			$row['doi']);
	$doi->Set('creator',		$row['creator']);
	$doi->Set('status',			$row['status']);
	$doi->Set('url',			$row['url']);
	$doi->Set('title',			$row['title']);
	$doi->Set('language',		$row['language']);
	$doi->Set('publisher',		$row['publisher']);
	$doi->Set('year',			$row['year']);
	$doi->Set('resource',		$row['resource']);
	$doi->Set('abstract',		$row['abstract']);
	$doi->Set('keywords',		$row['keywords']);
	$doi->Set('modified_by',	$row['modified_by']);

	$doiarray[] = $doi;
}

echo "Number of entries: " . sizeof($doiarray);

?>

<script>
	function download(filename, text) {
	  var element = document.createElement('a');
	  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	  element.setAttribute('download', filename);
	
	  element.style.display = 'none';
	  document.body.appendChild(element);
	
	  element.click();
	
	  document.body.removeChild(element);
	}
	</script>

<?php


$i = 0;
foreach($doiarray as $doi) {
	
	$xml_template_datacite = EOS_DOI_BASEPATH."eos_doi_template_datacite.xml";
	
	$xml = new XmlDOI($xml_template_datacite);

	$xml->SetDoi($doi->Get("doi"));
	$xml->SetAuthor($doi->Get("creator"));
	$xml->SetTitle($doi->Get("title"));
	$xml->SetPublisher($doi->Get("publisher"));
	$xml->SetYear($doi->Get("year"));
	$xml->SetLanguage($doi->Get("language"));
	$xml->SetResourceType($doi->Get("resource"));
	$xml->SetAbstract($doi->Get("abstract"));
	$xml->SetKeywords($doi->Get("keywords"));
	$xml->SetAlternateIdentifier();
	$xml->SetRelatedIdentifier();
	$xml->SetRights();

	$filename = date('Ymd.His') . '.engramma.doi.' . $doi->Get("article_id") . '.xml';
	
	$content = $xml->GetXML();
	$i = $i +1;
?>
<script>
	// Start file download.
	download("<?php print $filename; ?>", <?php print json_encode($content); ?>);
</script>

<?php

}

echo "<br><br>Numeber of iterations: ". $i;

?>

