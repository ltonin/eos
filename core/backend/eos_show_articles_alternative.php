<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_article.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_issue.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );


if($user->IsAuthorized() == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();

// Define urls
$urlarea   = EOS_BASEURL.'core/backend/eos_backend_index.php';
$urlmanage = $urlarea.'?tool=11';
$urlview   = $urlarea.'?tool=12';
$urlself   = $urlarea.'?tool=16';

// Issue to be showed
$issue   = new Issue();

if(isset($_GET['issueid'])) {
	$issueid = $_GET['issueid'];
  $issue->Retrieve($issueid);
} else if(isset($_POST['issueid'])) {
	$issueid = $_POST['issueid'];
  $issue->Retrieve($issueid);
} 

// Default the current published issue
if ($issue->IsEmpty()) {
	$issuenumber = $_SESSION['issuecurrent'];
  $issue->RetrieveBy('numero', ElementType::AsInteger, $issuenumber);
  $issueid = $issue->Get('id');
}

// Management of actions
$action = "";
if(isset($_GET['action'])) {
	$action = $_GET['action'];
} else if(isset($_POST['action'])) {
	$action = $_POST['action'];
}

$listarticles = array();
switch($action) {
	case "setstatus":
	  $articleid = $_POST['articleid'];
		$article = new Article();
		$article->Retrieve($articleid);
		$articlestatus = new ArticleStatus();
		$articlestatus->Retrieve($_POST['statusid']);
		$refarticle = &$article;	
		$refstatus  = &$articlestatus;	
		
		if(!eos_article_setstatus($refarticle, $refstatus))
			$message->AddMessage("Stato non modificato", MessageType::AsWarning);
		else
			$message->AddMessage("Stato articolo modificato ('"
				     .$article->Get('title')."'): '"
			             .$article->Get('status::label')."'", MessageType::AsInfo);
		break;
	default:
		break;
}

// Retrieve id of trash status
$table = new Table('article_status');
$table->SetCondition('label', 'cestino', ElementType::AsString);
$table->Select('id');
$table->Get('id', $trashid, 0);

// Retrieve all the possible status for articles (except 'trash')
$table->SetTable('article_status');
$table->SetCondition('id', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
$table->Select('id');
$table->Get('id', $liststatus);

// Retrieve all issues (ordered by status and number)
$table->SetTable('tb_numero');
// Hack before moving table (>1000) to a new category
$table->SetCondition('numero', 1000, ElementType::AsInteger, QueryCondition::IsLesser);
$table->SetOrder('stato');
$table->SetOrder('numero', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $listissues);
?>
<div id="backend_show">
<div class="title">Gestione articoli</div>

<!-- Operation messages -->
<?php $message->Show(); ?>

<!-- Select item with all issues -->
<form action="<?php print $urlself; ?>" method="post">
<label for="issueid">Seleziona numero:</label>
<select id="issueid" name="issueid" onchange="this.form.submit()">
<?php
$pstatus = "";
foreach($listissues as $cissueid) {
	$cissue = new Issue();
	$cissue->Retrieve($cissueid);
	$selected = "";
	if($cissue->Get("id") == $issueid) {
		$selected = "selected";
	}
  
  $cstatus = $cissue->Get("status::label");
  if(strcmp($cstatus, $pstatus) != 0) {
    if(empty($pstatus) == 0) {
      print "</optgroup>";
    }
    $pstatus = $cstatus;
?>
  <optgroup label="<?php print $cstatus; ?>">
<?php } ?>
  <option value="<?php print $cissue->Get("id"); ?>" <?php print $selected; ?>>
  <?php print $cissue->Get("number"); ?>
  </option>
<?php
}
?>
  </select>
</form>

<!-- Articles table -->
<?php
if($issue->IsEmpty() == false) {
?>

<table>
<tr>
<th>Titolo</th>
<th>Autore</th>
<th>Ultima modifica</th>
<th>Tipo</th>
<th>Stato</th>
<th>Strumenti</th>
</tr>
<?php
  // Retrieve all articles belonging to the given issue
  $table = new Table('tb_articolo');
  $table->SetCondition('id_numero', $issue->Get('id'), ElementType::AsInteger);
  $table->SetCondition('stato', $trashid, ElementType::AsInteger, QueryCondition::IsDifferent);
  $table->Select('id');
  $table->Get('id', $articles_l);

  // Iterate along articles belonging to this issue -->
  foreach($articles_l as $carticleid) {
    $carticle = new Article();    
    $carticle->Retrieve($carticleid);
?>
    
    <tr>
    <!-- General information -->
    <td style="max-width:300px; word-wrap:break-word;" >
    <a href="<?php print $urlview."&articleid=".$carticle->Get("id"); ?>">
    <?php print $carticle->Get("title"); ?>
    </a>
    </td>
    <td><?php print $carticle->Get("user::nickname"); ?></td>
    <td><?php print $carticle->Get("modified")." (".$carticle->Get("modifiedby::nickname").")"; ?></td>
    <td><?php print $carticle->Get("type::label"); ?></td>
    
    <!-- Status selection -->
    <td>
    <form action="<?php print $urlself."&showissueid=".$cissue->Get('id'); ?>" method="post">
    <select name="statusid" onchange="this.form.submit()">
    <?php
    reset($liststatus);
    foreach($liststatus as $cstatusid) {
      $status = new ArticleStatus();
      $status->Retrieve($cstatusid);
      $selected = "";
      if($status->Get("id") == $carticle->Get("status::id"))
      	$selected = "selected";
    ?>
      <option value="<?php print $status->Get("id"); ?>" <?php print $selected; ?>>
      <?php print $status->Get("label"); ?>
      </option>
    <?php
    }
    ?>
    </select>
    <input type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
    <input type="hidden" name="articleid" value="<?php print $carticle->Get("id"); ?>">
    <input type="hidden" name="action" value="setstatus">
    </form>
    </td>	
    <!-- Modify/Trash buttons -->
    <td>
    <div class="toolbar">
    <form  action="<?php print $urlmanage; ?>" method="post">
    <input  type="hidden" name="articleid" value="<?php print $carticle->Get("id"); ?>">
    <button type="submit" name="action"    value="modify" >
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-edit.png'; ?>" width="24" title="Modifica">
    </button>
    </form>
    <form  action="<?php print $urlself."&showissueid=".$cissue->Get('id'); ?>" method="post">
    <input  type="hidden" name="articleid" value="<?php print $carticle->Get("id"); ?>">
    <input  type="hidden" name="issueid" value="<?php print $issue->Get("id"); ?>">
    <input  type="hidden" name="statusid"  value="<?php print $trashid; ?>">
    <button type="submit" name="action"    value="setstatus" >
    <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Cestina">
    </button>
    </form>
    </div>
    </td>	
    </tr>
  <?php
  } // list articles
  ?>
</table>
<?php
}
?>
</div>
