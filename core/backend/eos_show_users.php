<?php
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_message.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_table.php");

/** Authorization **/
$user = new User();
if(isset($_SESSION['userid']) && $_SESSION['userid'] != 0) 
	$user->Retrieve($_SESSION['userid'] );

if($user->IsAuthorized("Magister Ludi") == false) 
	header('location:'.EOS_BASEURL);

$message   = new Message();
$selfurl   = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=52';
$parenturl = EOS_BASEURL.'core/backend/eos_backend_index.php?tool=51';

/** Manage post action **/
$action = "";
if(isset($_POST['action'])) 
	$action=$_POST['action'];

switch($action) {
	/** Delete user action **/
	case "delete":
		$userid = $_POST['userid'];
		$user = new User();
		$user->Retrieve($userid);
		$user->Drop();
		$message->AddMessage("Utente '".$user->Get('nickname')."' eliminato", MessageType::AsInfo);
		break;
}

// Get list of all attributes needed
$table = new Table('tb_utente');
$table->SetOrder('tipo', QueryOrder::AsAscending);
$table->SetOrder('created', QueryOrder::AsDescending);
$table->Select('id');
$table->Get('id', $listusers);
?>

<div id="backend_show">
<div class="title">Gestione utenti</div>
<?php $message->Show(); ?>
<table>
<tr>
	<th>Nickname</th>
	<th>E-mail</th>
	<th>Tipo</th>
	<th>Nome</th>
	<th>Cognome</th>
	<th>Creato</th>
	<th>Ultima modifica</th>
	<th>Strumenti</th>
</tr>
<?php 
	foreach($listusers as $cuserid) {
		$user = new User();
		$user->Retrieve($cuserid);
?>
<tr>
	<td><?php print $user->Get("nickname"); ?></td>	
	<td><?php print $user->Get("email"); ?></td>
	<td><?php print $user->Get("type::label"); ?></td>
	<td><?php print $user->Get("firstname"); ?></td>
	<td><?php print $user->Get("lastname"); ?></td>
	<td><?php print $user->Get("created"); ?></td>
	<td><?php print $user->Get("modified"); ?></td>
	<td>
  <div style="float:left">
	<form action="<?php print $parenturl."&action=modify"; ?>" method="post" >
		<input type="hidden" name="userid" value="<?php print $user->Get("id"); ?>">
		<button type="submit">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-edit.png'; ?>" width="24" title="Modifica">
</button>
	</form>
</div>
  <div style="float:left">
	<form action="<?php print $selfurl; ?>" method="post" 
	      onsubmit="return confirm('L\'utente \'<?php print $user->Get("nickname"); ?>\' verr\u00E0 cancellato definitivamente. Confermi l\'eliminazione?');">
		<input type="hidden" name="userid" value="<?php print $user->Get("id"); ?>">
		<button type="submit" name="action" value="delete">
   <img src="<?php print EOS_SYSTEM_IMAGES.'icon-trash.png'; ?>" width="24" title="Elimina">
</button>
	</form>
</div>
	</td>
</tr>
<?php
	}
?>
</table>
</div>
<br>
<br>
