<?php
//** DEBUG **//
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once(__DIR__."/core/eos_configuration_init.php");
require_once(EOS_BASEPATH."core/pod/eos_pod_user.php");
require_once(EOS_BASEPATH."core/functions/eos_core_support.php");
eos_session_start();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html prefix="og: http://ogp.me/ns#">

<?php
/*************** HTML HEAD ***************/
include(EOS_BASEPATH."core/frontend/eos_headhtml.php");
/**************************************/
?>

<?php //<body> ?>


<?php  
if(isset($_GET['id_articolo'])) {
	include(EOS_BASEPATH."core/frontend/eos_article.php");
} elseif (isset($_GET['archivio'])) {
	include(EOS_BASEPATH."core/frontend/eos_archive.php");
} elseif (isset($_GET['cerca'])) {
	include(EOS_BASEPATH."core/frontend/eos_google.php");
} elseif (isset($_GET['libreria'])) {
	include(EOS_BASEPATH."core/frontend/eos_bookstore.php");
} elseif(isset($_GET['login']))  {
     include(EOS_BASEPATH."core/frontend/eos_login.php");
} elseif(isset($_GET['notfound']))  {
	include(EOS_BASEPATH."core/frontend/eos_notfound.php");
} elseif(isset($_GET['sandbox']))  {
	include(EOS_BASEPATH."core/frontend/eos_sandbox.php");
} elseif(isset($_GET['meta-sandbox']))  {
	include(EOS_BASEPATH."core/frontend/eos_sandbox_meta.php");
} elseif(isset($_GET['covertest']))  {
	include(EOS_BASEPATH."core/frontend/eos_cover_test.php");
} else  {
	if (isset($_GET['issue'])) {
		$_SESSION['issueshown'] = $_GET['issue'];
	}
	include(EOS_BASEPATH.'core/frontend/eos_cover.php');
}
?>
</div>
<?php
/*************** FOOTER ***************/
include(EOS_BASEPATH."core/frontend/eos_footer.php");
/**************************************/
?>
</body>
</html>
