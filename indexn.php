<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once(__DIR__."/core/eos_configuration_init.php");
require_once(EOS_BASEPATH."core/functions/new/eos_session_support.php");
require_once(EOS_BASEPATH."core/functions/new/eos_frontend_support.php");

eos_session_start();

//print_r($_SESSION);

//session_unset();
?>
<!DOCTYPE html>
<html lang="it">
<head>

<title>engramma - la tradizione classica nella memoria occidentale</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS Style sheets -->
<link rel="stylesheet" type="text/css" title="eos_styles" href="<?php print EOS_CSS_BASEURL.'/new/eos_styles_common.css.php'; ?>" media="screen"></link>
<link rel="stylesheet" type="text/css" title="eos_styles" href="<?php print EOS_CSS_BASEURL.'/new/eos_styles_navigation.css.php'; ?>" media="screen"></link>
<link rel="stylesheet" type="text/css" title="eos_styles" href="<?php print EOS_CSS_BASEURL.'/new/eos_styles_footer.css.php'; ?>" media="screen"></link>

<!-- Script for JQuery hosted by Google -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script>
$(document).ready( function() {
	$('#eos-navigation-menu a').click(function() {


		$("#eos-navigation-container > div").css("display", "none");

		var tab = $(this).attr("href");
		var is_visible = $(tab).css("display");

		if(is_visible === "none") {
			$("#eos-navigation-container").css("display", "block");
			$(tab).css("display", "flex");
		} else if(is_visible === "flex") {
			$("#eos-navigation-container").css("display", "none");
			$(tab).css("display", "none");
		}
			
	});


});

$(document).ready( function() {

	$(document).mousemove(function(event) {
		
		var offset = $("#eos-navigation-menu").offset();
		var height = $("#eos-navigation-menu").height();
		var limit = (parseInt(offset.top) + parseInt(height));

		if(event.pageY > limit) {
			$("#eos-navigation-container").css("display", "none");
		}
	});

});
</script>

<?php

/*** Meta-tags ***/

/*** Scripts ***/
?>
</head>

<body>

<header>
<img class="title" src="<?php print(eos_frontend_current_title_image()); ?>" alt="engramma">
</header>

<nav>
<?php include(EOS_BASEPATH . "core/frontend/new/eos_navigation.php"); ?>
</nav>





<section>

<article>
</article>

</section>

<footer class="eos-footer">
<?php include(EOS_BASEPATH."core/frontend/new/eos_footer.php"); ?>
</footer>

</body>
</html>
