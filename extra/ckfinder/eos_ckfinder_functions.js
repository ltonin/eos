function eos_ckfinder_launch( elementId, resourceId ) {
	CKFinder.modal({
		chooseFiles: true,
		language: 'it',
		resourceType: resourceId,	

		width: 800,
		height: 600,	
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById(elementId);
				output.value = file.getUrl();
			} );
		finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
	
	} );
};

