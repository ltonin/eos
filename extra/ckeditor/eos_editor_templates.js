/*Aggiunge templates personalizzati al menu
*/
CKEDITOR.addTemplates('default', {
	imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates')+'templates/images/'),
	templates: [ 
	{
		title:'base',
		image:'template1.gif',
		description:'impostazioni standard per scrivere articoli.',
		html:'<h2 class="titolo">Scrivi il titolo qui</h2><h3 class="sottotitolo">Scrivi il sottotitolo</h3><h4 class="autore">Autore</h4><p>Testo da modificare</p><div class="biblio"><h5>Bibliografia</h5><ul><li>titolo</li><li>altro titolo2</li></ul></div>'
	},
	{
		title:'tutta pagina',
		image:'template1.gif',
		description:'figura e testo a tutta pagina',
		html:'<div class="grid_10"><img src="nofile.png"><p class="caption">didascalia</p><p>Testo da modificare</p></div>'
	},
	{
		title:'due-tre',
		image:'template1.gif',
		description:'figura 2/5 e testo 3/5',
		html:'<div class="grid_4"><img src="nofile.png"><p class="caption">didascalia</p></div><div class="grid_6"><p>Testo da modificare</p></div>'
	},
	{
		title:'cinque-cinque',
		image:'template1.gif',
		description:'figura 1/2 e testo 1/2',
		html:'<div class="grid_5"><img src="nofile.png"><p class="caption">didascalia</p></div><div class="grid_5"><p>Testo da modificare</p></div>'
	},
	{
		title:'tre-sette',
		image:'template1.gif',
		description:'figura 3 e testo 7',
		html:'<div class="grid_3"><img src="nofile.png"><p class="caption">didascalia</p></div><div class="grid_7"><p>Testo da modificare</p></div>'
	} ]
} );
