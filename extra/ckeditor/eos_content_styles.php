<?php 
header("Content-type: text/css"); 
require_once(__DIR__."/../../core/eos_configuration_init.php");
?>

@font-face {
	font-family: 'Museo300-Regular';
	src: url(<?php print EOS_FONTS_BASEURL."Museo300-Regular.eot"; ?>);
	src: local('Museo300-Regular'), 
	     local('Museo300-Regular'), 
	     url(<?php print EOS_FONTS_BASEURL."Museo300-Regular.otf"; ?>) 
	     format('opentype');
}

@font-face {
	font-family: 'Museo500-Regular';
	src: url(<?php print EOS_FONTS_BASEURL."Museo500-Regular.eot"; ?>);
	src: local('Museo500-Regular'), 
	     local('Museo500-Regular'), 
	     url(<?php print EOS_FONTS_BASEURL."Museo500-Regular.otf"; ?>) 
             format('opentype');
}

@font-face {
	font-family: 'Museo700-Regular';
	src: url(<?php print EOS_FONTS_BASEURL."Museo700-Regular.eot"; ?>);
	src: local('Museo700-Regular'), 
	     local('Museo700-Regular'), 
	     url(<?php print EOS_FONTS_BASEURL."Museo700-Regular.otf"; ?>) 
             format('opentype');
}

@font-face {
	font-family: 'newathenaunicoderegular';
	src: url(<?php print EOS_FONTS_BASEURL."new_athena_unicode-webfont.eot"; ?>);
	src: url(<?php print EOS_FONTS_BASEURL."new_athena_unicode-webfont.eot?#iefix"; ?>) 
	     format('embedded-opentype'),
	     url(<?php print EOS_FONTS_BASEURL."new_athena_unicode-webfont.woff"; ?>) 
	     format('woff'),
	     url(<?php print EOS_FONTS_BASEURL."new_athena_unicode-webfont.ttf"; ?>) 
	     format('truetype'),
	     url(<?php print EOS_FONTS_BASEURL."new_athena_unicode-webfont.svg#newathenaunicoderegular"; ?>) 
             format('svg');
}

body {
	background-color:#E5E5C5;
	color:black;
	font-size:13px;
}
body, p, a, img, ul, ul li, div, h1, h2, h3, h4, h5, h6 {
	padding:0;
	margin:0;
	border:0;
	text-decoration:none;
	list-style:none
}

h2.titolo {
	font:normal 3.2em/0.9em "Museo500-Regular",verdana, sans-serif;
	padding:20px 150px 0 0;
}

p {
	font:1.3em/1.4em "Lucida Sans","Lucida Sans Unicode","Trebuchet MS",arial,sans-serif;
	padding:20px 20px 0
}

.caption{
	background-color:#ccc;
	padding:5px;
	color:#eee
}

h3.sottotitolo {
	font:normal 2.6em/1em "Museo300-Regular",verdana, sans-serif;
	padding:0 150px 0 0;
}

h4.autore{
	font:normal 1.3em/2em "Museo300-Regular",verdana, sans-serif; 
	color: #444;
	padding:0 20px 50px;
}

h4+h4 {
	padding-bottom:0
}

h5.abstract {
	font:normal 1.1em/1.2em "Museo300-Regular",verdana, sans-serif;
	text-align:left; 
	padding:10px 20px 10px; 
	float: right;
}

h6.paragrafo{
	font:bold 1.3em/2em "Museo300-Regular",verdana, sans-serif; 
	color: #444;
	padding:20px 20px 5px;
}

p.abstract{
	font:normal .9em/1.4em Arial, sans-serif;
	text-align:left;
	letter-spacing:0.04em;
	padding:20px 20px 0
}

p.citadx{
	font:1.1em/1.2em "Lucida Sans","Lucida Sans Unicode","Trebuchet MS",arial,sans-serif;
	padding:20px 20px 0;
	text-align: right;	
}

p.citazione{
	font:1.1em/1.2em "Lucida Sans","Lucida Sans Unicode","Trebuchet MS",arial,sans-serif;
	padding:20px 40px 0;
	text-align: left;	
}

img {
	width:100%;
}

sup {
	font-size: 50%;
	vertical-align: top;
}

.grid_1,.grid_2,.grid_3,.grid_4,.grid_5,.grid_6,.grid_7,.grid_8,.grid_9,.grid_10 {
	display:inline;
	float:left;
	position:relative;
}
.grid_1 { width:9.9%; }
.grid_2 { width:19.9%; clear: left; }
.grid_3 { width:29.8%; clear: left; }
.grid_4 { width:39.8%; clear: left; }
.grid_5 { width:49.6%; }
.grid_6 { width:59.8%; }
.grid_7 { width:69.4%; }
.grid_8 { width:79.8%; }
.grid_9 { width:89.2%; }
.grid_10{ width:99.8%; clear: both; }

.push_1 { left:9.9%;  }
.push_2 { left:19.9%; }
.push_3 { left:29.8%; }
.push_4 { left:39.8%; }
.push_5 { left:49.6%; }
.push_6 { left:59.8%; }
.push_7 { left:69.4%; }
.push_8 { left:79.8%; }
.push_9 { left:89.2%; }

ul.biblio li{
list-style: disc inside url(<?php print EOS_BASEURL."core/img/li.png"; ?>);
	padding: 5px 10px
}

h5.biblio {
	font:normal 2em/0.8em "Museo500-Regular",verdana, sans-serif;
	text-align:left
}

ul.biblio {
	font:normal .9em/1.4em Arial, sans-serif;
	text-align:left;
	letter-spacing:0.04em; 
	color: #444;
}

.biblio {
	clear:both;
}

ol.note {
	font:normal .9em/1.4em Arial, sans-serif;
	text-align:left;
	letter-spacing:0.04em; 
	color: #444;
}

ol.note, ol.note li{
	padding: 0 10px
}

html>body ol { 
	list-style-type: none;
   	counter-reset: level1;
}

ol.note li:before {
	content: counter(level1) ". ";
	counter-increment: level1;
}

ol.note li ol {
	list-style-type: none;
	counter-reset: level2;
}

ol.note li ol li:before {
	content: counter(level1) "" counter(level2,lower-alpha) ". ";
	counter-increment: level2;
}

.abstract {
	clear:both;
}

.greco {
	font-family:'newathenaunicoderegular','New Athena Unicode','Palatino Linotype','Lucida Grande','Cardo','Gentium','Vusillus Old Face','Arial Unicode MS','Georgia Greek','Athena','Code2000','TITUS Cyberbit Basic','Lucida Sans Unicode', verdana;
}

#index {
	display:block;
	width:57.1%;
	float:left;
}

.tMenu {
	font:normal 1.6em/0.9em "Museo500-Regular",verdana, sans-serif;
}

.aMenu {
	font:normal 0.9em/0.8em verdana, sans-serif;
}

.vMenu a:hover, .vMenu a:active {
	background-color:#efefef;
	text-decoration:underline
}

.tMenu {
	padding:0 0 0.15em 0
}

.vMenu {
	padding:0.5em 0.5em;
	display:block;
	color:#222222;
}

#issueN {
	font:normal 24em/0.8em "Museo700-Regular",verdana, sans-serif;
	text-align:right
}

#issueD {
	font:normal 6em/0.8em "Museo300-Regular",verdana, sans-serif;
	text-align:right
}

#issue {
	display:block;
	width:40.7%;
	float:left;
	padding:0px;
	text-align:right;
	position:relative;
	z-index:1000;
	font-size:0.8em;
}

#issueN, #issueD, #issueT {
	padding-right:30px;
}

#imgNumero{
	position:absolute;
	left:0px;
	display:block;
	width:40.7%;
	float:left;
	text-align:right;
	overflow:hidden;
	margin-top:-25px;
	z-index:0;
}
.maiuscoletto {
  font-variant: small-caps;
}
