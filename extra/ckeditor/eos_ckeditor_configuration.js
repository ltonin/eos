// eOS ckeditor configuration

CKEDITOR.editorConfig = function( config ) {

	// General configuration
	config.language	= 'it';
	config.skin 	= 'kama';
	
	config.extraPlugins 	= 'codemirror,autocorrect';
	config.autoParagraph 	= false;
	config.fillEmptyBlocks 	= false;
	config.entities_greek 	= false;
	
	// Contents style configuration
	config.contentsCss 	= path.extra + 'ckeditor/eos_content_styles.php';
	config.allowedContent 	= true;
	
	config.font_names = 'greco/newathenaunicoderegular;' + config.font_names;	
	
	// Editor styles and templates configuration
        config.stylesSet 	= 'stili:' + path.extra + 'ckeditor/eos_editor_styles.js';
        config.templates_files 	= [ path.extra + 'ckeditor/eos_editor_templates.js'];

	// Toolbar configuration
        config.toolbar = 'eos';
        config.toolbar_eos = [
        { 
		name: 'document', 
		items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] 
	},
        { 
		name: 'clipboard', 
		items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] 
	},
        { 
		name: 'editing', 
		items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] 
	},
	{ 
		name: 'links', 
		items : [ 'Link','Unlink','Anchor' ] 
	},
        { 
		name: 'insert', 
		items : [ 'Image','ImageMaps', 'Table','SpecialChar' ] 
	},
        { 
		name: 'tools', 
		items : [ 'Maximize', 'ShowBlocks','-','About' ] 
	},
        '/',
        { 
		name: 'styles', 
		items : [ 'Styles','Format','Font','FontSize' ] 
	},
        { 	
		name: 'colors', 
		items : [ 'TextColor','BGColor' ] 
	},
        { 
		name: 'basicstyles', 
		items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] 
	},
        { 
		name: 'paragraph', 
		items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv', '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] 
	} ];
};
